
import React, {Component} from 'react';
import {Text, View,Dimensions, ScrollView, AsyncStorage, Alert, TouchableOpacity} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux'

import Axios from 'axios';
// Axios.defaults.baseURL = url;
import SidebarItem from '../components/sidebarItem'
import Loader from '../components/loader'
import AlertView from '../components/modalMassage'
import LinearGradient from 'react-native-linear-gradient'
import {connect} from 'react-redux';
import {store} from '../config/store';

class Sidebar extends React.Component {
    constructor(){
        super();
        this.state = {
            loading: false,
            user: {},
            show: false,
            modalVisible: false,
            request: false,
            showMore : false,
            login: false
        };
    }
    closeModal() {
        this.setState({modalVisible: false});
    }
    componentWillMount() {
        AsyncStorage.getItem('token').then((info) => {
            if(info !== null) {

                const newInfo = JSON.parse(info);
                const expiresTime = newInfo.expires_at;
                const currentTime = new Date().getTime()/1000;
                console.log('expiresTime', expiresTime);
                console.log('currentTime', currentTime);
                if(expiresTime> currentTime){
                    store.dispatch({type: 'USER_LOGED', payload: true});
                    Axios.post('/request/market/get-status', {
                        user_id: newInfo.user_id
                    }).then(response => {
                        this.setState({loading: false, statuses: response.data.data, showMore: true});
                        console.log('statuses',response.data.data)
                        if(response.data.data === "") {
                            this.setState({request: false, showMore: true});
                        }
                        else if(response.data.data !== "" && response.data.data.status === "request") {
                            this.setState({request: false, showMore: true});
                        }
                        else if(response.data.data !== "" && response.data.data.status === "approve") {
                            this.setState({request: true, showMore: true});
                        }
                        else if(response.data.data !== "" && response.data.data.status === "reject") {
                            this.setState({request: false, showMore: true});
                        }
                        const info = {'showMore': true};
                        const newwwwAsync = AsyncStorage.setItem('showMore', JSON.stringify(info));
                        console.log('newwwwAsync', newwwwAsync);
                        AsyncStorage.getItem('showMore').then((info) => {
                            if(info !== null) {
                                const newInfo = JSON.parse(info);
                                console.log('showMore', newInfo)
                                newInfo.showMore === true ? this.setState({showMore: true}) :  this.setState({showMore: false});
                            }
                        })
                    })
                        .catch((error) =>{
                            // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                            // this.setState({loading: false});
                            this.setState({modalVisible: true, loading: false});

                        });
                }
                else {
                    this.setState({showMore : false, loading: false});
                }
            }
            else{
                this.setState({showMore : false, loading: false});
            }
        });
    }
    // componentDidUpdate() {
    //     let loged2 = null;
    //     AsyncStorage.getItem('loged').then((info) => {
    //         if(info !== null) {
    //             const newInfo = JSON.parse(info);
    //             console.log('new info sidebar', newInfo)
    //             newInfo.loged === true ? this.setState({login: true}) : null;
    //         }
    //     })
    //     console.log('loged side bar var', this.state.login)
    //     console.log('showMore', this.state.showMore)
    // }

    componentWillReceiveProps() {
        console.log('drawer is open kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk');
        this.forceUpdate();

        let loged2 = null;
        AsyncStorage.getItem('loged').then((info) => {
            if(info !== null) {
                const newInfo = JSON.parse(info);
                console.log('new info sidebar', newInfo)
                newInfo.loged === true ? this.setState({login: true}) : null;
            }
        })
        console.log('loged side bar var', this.state.login)
        AsyncStorage.getItem('showMore').then((info) => {
            if(info !== null) {
                const newInfo = JSON.parse(info);
                console.log('ssshow more async', newInfo)
                newInfo.showMore === true ? this.setState({showMore: true}) : this.setState({showMore: false});
            }
        })
        console.log('showMore', this.state.showMore)
    }
    userLogOut() {
        this.setState({login: false})
        AsyncStorage.removeItem('loged');
    }
    render(){
        const {loged} = this.props;
        console.log('loged redux',this.props.loged)

        if(this.state.loading){
            return (<Loader />)
        }
        else return (
            <LinearGradient
                style={{height: Dimensions.get('window').height*.96}}
                start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['#21b34f', '#95cc3a']}>
                <ScrollView style={styles.scroll}>
                    <View style={styles.container}>
                        <View style={styles.profile}>
                            {/*<Text style={styles.name}>{this.state.user.fname} {this.state.user.lname}</Text>*/}
                            <View style={styles.iconContainer}>
                                {/*{this.state.user.attachments !== undefined ?*/}
                                {/*<Image source={{uri: 'http://https://bimehsho.com/api/v1/files?uid='+this.state.user.attachments.uid+'&width=350&height=350'}} style={styles.image} />*/}
                                {/*: null*/}
                                {/*}*/}
                            </View>
                        </View>
                        {/*<SidebarItem label="خانه" icon="home" openDrawer={this.props.openDrawer} closeDrawer={this.props.closeDrawer}/>*/}
                        <SidebarItem label="خرید بیمه" icon="shield" openDrawer={this.props.openDrawer} closeDrawer={this.props.closeDrawer}/>
                        {loged ?
                            <View>
                                <SidebarItem label="کیف پول من" icon="md-wallet" openDrawer={this.props.openDrawer} closeDrawer={this.props.closeDrawer}/>
                                <SidebarItem label="سفارش های من" icon="receipt" openDrawer={this.props.openDrawer} closeDrawer={this.props.closeDrawer}/>
                                <SidebarItem label="یادآوری ها" icon="event-note" openDrawer={this.props.openDrawer} closeDrawer={this.props.closeDrawer}/>
                                {/*<SidebarItem label="معرفی به دوستان" icon="users" openDrawer={this.props.openDrawer} closeDrawer={this.props.closeDrawer}/>*/}
                                {
                                    this.state.request ?
                                        <View>
                                            <SidebarItem label="لینک دعوت" icon="wallet" openDrawer={this.props.openDrawer} closeDrawer={this.props.closeDrawer}/>
                                            <SidebarItem label="دعوت شده ها" icon="users" openDrawer={this.props.openDrawer} closeDrawer={this.props.closeDrawer}/>
                                        </View>

                                        :
                                        <SidebarItem label="درخواست بازاریابی" icon="wallet" openDrawer={this.props.openDrawer} closeDrawer={this.props.closeDrawer}/>
                                }
                            </View> : null
                        }
                        <SidebarItem label="بیمه شو چیست؟" icon="question-circle" openDrawer={this.props.openDrawer} closeDrawer={this.props.closeDrawer}/>
                        <SidebarItem label="قوانین و مقررات" icon="balance-scale" openDrawer={this.props.openDrawer} closeDrawer={this.props.closeDrawer}/>
                        {/*<SidebarItem label="تنظیمات" icon="md-settings" openDrawer={this.props.openDrawer} closeDrawer={this.props.closeDrawer}/>*/}
                        {/*<SidebarItem label="خروج" icon="logout" openDrawer={this.props.openDrawer} closeDrawer={this.props.closeDrawer} userLogOut={() => this.userLogOut()} />*/}
                        <AlertView
                            closeModal={(title) => this.closeModal(title)}
                            onChange={(visible) => this.setState({modalVisible: false})}
                            modalVisible={this.state.modalVisible}
                            title='مشکلی در برقراری ارتباط با سرور به وجود آمده لطفا دوباره سعی کنید '
                        />
                    </View>
                </ScrollView>
            </LinearGradient>
        );
    }
}

function mapStateToProps(state) {
    return {
        loged: state.auth.loged,
    }
}
export default connect(mapStateToProps)(Sidebar);

