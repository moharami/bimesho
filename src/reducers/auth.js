
const INITIAL_STATE = {
    user: {},
    prices: [],
    loged: false
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'USER_INFO_FETCHED':
            return { ...state, user: action.payload };
        case 'USER_LOGED':
            return { ...state, loged: action.payload };
        default:
            return state;
    }
};
