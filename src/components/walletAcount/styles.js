import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    topContainer: {
        width: '100%',
        // height: '100%',
        borderRadius: 10,
        marginBottom: 30,
        elevation: 4
    },
    container: {
        flex: 1,
        backgroundColor: 'white',
        // borderColor: 'lightgray',
        // borderWidth: 1,
        borderRadius: 10,
        // elevation: 4
    },
    header: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        // borderBottomColor: 'rgb(237, 237, 237)',
        // borderBottomWidth: 1,
        padding: 20
    },
    headerLabel: {
        fontSize: 18,
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'center',
        paddingBottom: 10
    },
    subLabel: {
        fontSize: 10,
        color: 'rgb(150, 150, 150)',
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'center',
        paddingBottom: 5
    },
    info: {
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
        padding: 10
    },
    price: {
        flexDirection: 'row'
    },
    priceLabel: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 12,
        paddingRight: 5,
        color: 'green'
    },
    amount: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 18,
        color: 'green'
    },
    Image: {
        width: 50,
        resizeMode: 'contain',
        height: 50,
        marginTop: 5
    },
    regimContainer: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 8,
    },
    left: {
        flexDirection: 'row',
    },
    right: {
        // flexDirection: 'row',
    },
    rightContainer: {
        // flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingRight: 3,
        paddingLeft: 3,
        // borderRadius: 20,
        backgroundColor: 'rgba(122, 130, 153, 1)',
        // overflow: 'hidden'
        borderBottomRightRadius: 10,
        borderBottomLeftRadius: 10,
        borderTopRightRadius: 10,
        // overflow: 'hidden',

    },
    label: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 14,
        color: 'white'
    },
    value: {
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'black',
        fontSize: 14
    },
    redValue: {
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'red',
        fontSize: 12
    },
    body: {
        paddingRight: 10,
        paddingTop: 15,
        paddingBottom: 15,
        borderBottomColor: 'rgb(237, 237, 237)',
        borderBottomWidth: 1,
    },
    contentLabel: {
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'rgb(50, 50, 50)',
        fontSize: 11,
        textAlign: 'right',
        paddingLeft: 5,
        lineHeight: 20,
        paddingBottom: 15
    },
    bodyLeft: {
        flexDirection: 'row',
        paddingLeft: 30
    },
    bodyRight: {
        flexDirection: 'row'
    },
    bodyLabel: {
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'rgb(150, 150, 150)',
        fontSize: 12
    },
    bodyValue: {
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'black',
        fontSize: 12
    },
    iconLeftContainer: {
        backgroundColor: 'rgba(255, 45, 85, 1)',
        width: 32,
        height: 32,
        borderRadius: 32,
        alignItems: 'center',
        justifyContent: 'center',
    },
    iconRightContainer: {
        backgroundColor: '#21b34f',
        // width: '40%',

        height: 32,
        borderRadius: 30,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        paddingLeft: 5,
        paddingRight: 10
    },
    footer: {
        padding: 15
    },
    advertise: {
        width: '85%',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 30,
        backgroundColor: 'rgba(255, 193, 39, 1)',
        padding: 7,
    },
    buttonTitle: {
        fontSize: 13,
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    btnContainer: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    shareContainer: {
        width: '85%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: 'rgba(245, 246, 250, 1)',
        height: 45,
        borderTopRightRadius: 10,
        borderBottomRightRadius: 10,
    },
    shareIcon: {
        backgroundColor: 'rgba(122, 130, 153, 1)',
        borderTopLeftRadius: 10,
        borderBottomLeftRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
        height: 45,
        width: 45

    },
    shareCode: {
        flexDirection: 'row',
        // alignItems: 'center',
        // justifyContent: '',
        paddingRight: 10
    },
    codeLabel: {
        fontSize: 13,
        color: 'rgb(150, 150, 150)',
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    codeValue: {
        fontSize: 13,
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)',
        paddingRight: 15
    },
    searchButton: {
        width: '90%',
        height: 35,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#21b34f',
        borderRadius: 5
    },
    searchText:{
        color: 'white'
    },

});
