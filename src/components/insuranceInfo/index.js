import React, {Component} from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import FIcon from 'react-native-vector-icons/dist/Feather';
import FFIcon from 'react-native-vector-icons/dist/FontAwesome';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import pic2 from '../../assets/insurances/2.png'
import pic3 from '../../assets/insurances/3.png'
import pic4 from '../../assets/insurances/4.png'
import pic5 from '../../assets/insurances/5.png'
import pic6 from '../../assets/insurances/6.png'
import pic7 from '../../assets/insurances/7.png'
import pic8 from '../../assets/insurances/8.png'
import pic9 from '../../assets/insurances/9.png'
import pic10 from '../../assets/insurances/10.png'
import pic11 from '../../assets/insurances/11.png'
import pic12 from '../../assets/insurances/12.png'
import pic13 from '../../assets/insurances/13.png'
import pic14 from '../../assets/insurances/14.png'
import pic15 from '../../assets/insurances/15.png'
import pic18 from '../../assets/insurances/18.png'
import pic19 from '../../assets/insurances/19.png'
import pic20 from '../../assets/insurances/20.png'
import pic21 from '../../assets/insurances/21.png'
import pic22 from '../../assets/insurances/22.png'
import pic23 from '../../assets/insurances/23.png'
import payment from '../../assets/payment.png'
import instalment1 from '../../assets/instalment1.png'
import instalment2 from '../../assets/instalment2.png'

class InsuranceInfo extends Component {
    constructor(props){
        super(props);
        this.state = {
            open: false,
            buy: false,
            activeBuy: null,
            instalment: false,
            instalmentBuy: false,
            covers: false
        };
    }
    render() {
        // console.log('this.state.active buy', this.state.activeBuy)
        // console.log('this.props.nnnn.active buy', this.props.buyId)
        const newValue =  Math.floor(this.props.item.price/10).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        const thirdBasePrice = this.props.insurType === 'Third' ? Math.floor(this.props.item.base_insurance.base_third_party/10).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : null;
        const id = this.props.item.insurance.id;
        let imgSrc = null;
        switch (id) {
            case 2:
                imgSrc = pic2;
                break;
            case 3:
                imgSrc = pic3;
                break;
            case 4:
                imgSrc = pic4;
                break;
            case 5:
                imgSrc = pic5;
                break;
            case 6:
                imgSrc = pic6;
                break;
            case 7:
                imgSrc = pic7;
                break;
            case 8:
                imgSrc = pic8;
                break;
            case 9:
                imgSrc = pic9;
                break;
            case 10:
                imgSrc = pic10;
                break;
            case 11:
                imgSrc = pic11;
                break;
            case 12:
                imgSrc = pic12;
                break;
            case 13:
                imgSrc = pic13;
                break;
            case 14:
                imgSrc = pic14;
                break;
            case 15:
                imgSrc = pic5;
                break;
            case 16:
                imgSrc = pic22;
                break;
            case 18:
                imgSrc = pic18;
                break;
            case 19:
                imgSrc = pic19;
                break;
            case 20:
                imgSrc = pic20;
                break;
            case 21:
                imgSrc = pic21;
                break;
            case 22:
                imgSrc = pic22;
                break;
            case 23:
                imgSrc = pic23;
                break;
        }
        return (
            <View style={styles.topContainer}  onPress={() => null}>
                <View style={styles.container}>
                    <View style={styles.header}>
                        <View>
                            {
                                this.state.activeBuy ===  this.props.buyId && this.props.buyId !== null ?
                                    // this.state.buy?
                                    <View style={styles.iconRightEditContainer}>
                                        <FIcon name="check" size={18} color="white" />
                                    </View> :
                                    <TouchableOpacity onPress={() => {this.props.handleNext(Math.floor(this.props.item.price/10), this.props.item.insurance.id, false, this.props.item.insurance.id);  this.setState({buy: true, activeBuy: this.props.item.insurance.id})}} style={styles.iconRightContainer}>
                                        <FIcon name="shopping-cart" size={16} color="white" style={{borderColor: 'white', borderWidth: 1, borderRadius: 20, padding: 5, marginRight: 5, alignItems: 'center', justifyContent: 'center'}} />
                                        <Text style={styles.label}>خرید </Text>
                                    </TouchableOpacity>
                            }
                            {
                                (this.props.item.installment &&  this.props.item.installment === 1) ?
                                    <TouchableOpacity onPress={() => {this.setState({instalment: !this.state.instalment})}} style={[styles.iconRightContainer, {backgroundColor: 'rgb(255, 192, 51)'}]}>
                                        <FFIcon name="money" size={18} color="white" style={{borderColor: 'white', borderWidth: 1, borderRadius: 20, padding: 3, marginRight: 5}} />
                                        <Text style={styles.label}>اقساط </Text>
                                    </TouchableOpacity>
                                    : null
                            }
                        </View>
                        {/*<View style={styles.right}>*/}
                        {/*</View>*/}
                        <View style={styles.left}>
                            <View style={styles.info}>
                                <Text style={styles.labelInfo}>{this.props.item.insurance.title}</Text>
                                <View style={styles.price}>
                                    <Text style={styles.priceLabel}>تومان</Text>
                                    <Text style={styles.amount}>{newValue}</Text>
                                </View>
                            </View>
                            <Image source={imgSrc} style={styles.Image} />
                        </View>
                    </View>
                    {
                        this.state.instalment?
                            <View style={styles.body}>
                                <Text style={styles.instalmentLabel}> شرایط قسط : </Text>
                                <Text style={styles.instalmentValue}> در حال حاضر خرید قسطی تنها در شهر تهران امکان‌پذیر است.
                                    فرایند صدور پس از واریز پیش پرداخت از درگاه بیمه شو، آغاز خواهد شد.
                                    برای سایر اقساط، چک‌ها با "آرم صیاد"، با فاصله یک ماه در وجه شرکت بیمه شو ، هنگام ارسال
                                    بیمه‌نامه از شما دریافت می‌شود.
                                </Text>
                                <View style={styles.row}>
                                    <Text style={styles.instalmentLabel}>پیش پرداخت: {Math.floor((this.props.item.price/20)/100)*100} تومان</Text>
                                    <Image source={payment} style={styles.image} />
                                </View>
                                <View style={styles.row}>
                                    <Text style={styles.instalmentLabel}>قسط اول: {Math.floor((this.props.item.price/40)/1000)*1000} تومان</Text>
                                    <Image source={instalment1} style={styles.image} />
                                </View>
                                <View style={styles.row}>
                                    <Text style={styles.instalmentLabel}>قسط دوم: {Math.floor((this.props.item.price/40)/1000)*1000} تومان</Text>
                                    <Image source={instalment2} style={styles.image} />
                                </View>
                                <View style={{alignItems: 'center', justifyContent: 'center'}}>
                                    <TouchableOpacity onPress={() => {this.setState({instalmentBuy: true}, () => {this.props.handleNext(Math.floor(this.props.item.price/10), this.props.item.insurance.id, this.state.instalmentBuy)})}} style={styles.btnContainer}>
                                        <Text style={styles.label}>خرید اقساط</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            : null
                    }
                    {
                        this.state.open ?
                            (
                                this.props.insurType === 'complete' ?
                                    <View style={styles.body}>
                                        <View style={styles.regimContainer}>
                                            <View style={styles.bodyLeft}>
                                                <Text style={styles.bodyValue}>{this.props.item.content.name}</Text>
                                                <Text style={styles.bodyLabel}>نام طرح: </Text>
                                            </View>
                                            <View style={styles.bodyLeft}>
                                                <Text style={styles.bodyValue}>{this.props.item.form_complete === 1 ? 'بله' : 'خیر'}</Text>
                                                <Text style={styles.bodyLabel}>تکمیل فرم پزشکی: </Text>
                                            </View>
                                        </View>
                                        <View style={styles.regimContainer}>
                                            <View style={styles.bodyLeft}>
                                                <Text style={styles.bodyValue}>{this.props.item.content.Franchise === 1 ? 'بله' : 'خیر'}</Text>
                                                <Text style={styles.bodyLabel}>فرانشیز: </Text>
                                            </View>
                                            <View style={styles.bodyLeft}>
                                                <Text style={styles.bodyValue}>{this.props.item.content.period_other === 1 ? 'بله' : 'خیر'}</Text>
                                                <Text style={styles.bodyLabel}>مدت انتظار سایر تعهدات: </Text>
                                            </View>
                                        </View>
                                        <View style={[styles.regimContainer, {justifyContent: 'flex-end'}]}>
                                            <View style={styles.bodyLeft}>
                                                <Text style={styles.bodyValue}>{this.props.item.content.period_childbirth}</Text>
                                                <Text style={styles.bodyLabel}>مدت انتظار زایمان و بیماری‌های مزمن: </Text>
                                            </View>
                                        </View>
                                        <View style={{ alignItems: 'flex-end', justifyContent: 'flex-end'}}>
                                            {/*<Image source={imgSrc} style={styles.Image} />*/}
                                            <TouchableOpacity onPress={() => this.setState({covers: !this.state.covers})} style={[styles.describeContainer, {backgroundColor: 'rgb(255, 193, 39)', marginBottom: 20}]}>
                                                <Text style={[styles.label, {fontSize: 10}]}>پوشش ها</Text>
                                            </TouchableOpacity>
                                        </View>
                                        {
                                            this.state.covers ?
                                                <View>
                                                    <View style={styles.regimContainer2}>
                                                        <View style={styles.bodyLeft3}>
                                                            <Text style={styles.bodyValue2}>{this.props.item.content.cover_surgery/10} تومان</Text>
                                                            <Text style={styles.bodyLabel2}>جراحی تخصصی: </Text>
                                                        </View>
                                                        <View style={styles.bodyLeft2}>
                                                            <Text style={styles.bodyValue2}>{this.props.item.content.cover_hospitalization/10} تومان</Text>
                                                            <Text style={styles.bodyLabel2}>هزینه بستری: </Text>
                                                        </View>
                                                    </View>
                                                    <View style={styles.regimContainer2}>
                                                        <View style={styles.bodyLeft3}>
                                                            <Text style={styles.bodyValue2}>{this.props.item.content.cover_sonography/10} تومان</Text>
                                                            <Text style={styles.bodyLabel2}>سونوگرافی و انواع اسکن: </Text>
                                                        </View>
                                                        <View style={styles.bodyLeft2}>
                                                            <Text style={styles.bodyValue2}>{this.props.item.content.cover_brain_bar/10} تومان</Text>
                                                            <Text style={styles.bodyLabel2}>نوار مغز و تست ورزش: </Text>
                                                        </View>
                                                    </View>
                                                    <View style={styles.regimContainer2}>
                                                        <View style={styles.bodyLeft3}>
                                                            <Text style={styles.bodyValue2}>{this.props.item.content.cover_decrepitude/10} تومان</Text>
                                                            <Text style={styles.bodyLabel2}>شکستگی و دررفتگی: </Text>
                                                        </View>
                                                        <View style={styles.bodyLeft3}>
                                                            <Text style={styles.bodyValue2}>{this.props.item.content.cover_sterile/10} تومان</Text>
                                                            <Text style={styles.bodyLabel2}>نازایی: </Text>
                                                        </View>
                                                    </View>
                                                    <View style={styles.regimContainer2}>
                                                        <View style={styles.bodyLeft3}>
                                                            <Text style={styles.bodyValue2}>{this.props.item.content.cover_ambulance/10} تومان</Text>
                                                            <Text style={styles.bodyLabel2}>هزینه آمبولانس: </Text>
                                                        </View>
                                                        <View style={styles.bodyLeft2}>
                                                            <Text style={styles.bodyValue2}>{this.props.item.content.cover_childbirth/10} تومان</Text>
                                                            <Text style={styles.bodyLabel2}>هزینه زایمان: </Text>
                                                        </View>
                                                    </View>
                                                    <View style={styles.regimContainer2}>
                                                        <View style={styles.bodyLeft3}>
                                                            <Text style={styles.bodyValue2}>{this.props.item.content.cover_visit/10} تومان</Text>
                                                            <Text style={styles.bodyLabel2}>ویزیت و دارو: </Text>
                                                        </View>
                                                        <View style={styles.bodyLeft2}>
                                                            <Text style={styles.bodyValue2}>{this.props.item.content.cover_dentistry/10} تومان</Text>
                                                            <Text style={styles.bodyLabel2}>هزینه های دندانپزشکی: </Text>
                                                        </View>
                                                    </View>
                                                    <View style={styles.regimContainer2}>

                                                        <View style={[styles.bodyLeft2, {flexDirection: this.props.item.content.cover_laboratory.toString().length > 4 ? 'column' : undefined, paddingTop: 15, alignItems:'flex-end', justifyContent: 'flex-end'}]}>
                                                            <Text style={styles.bodyLabel2}>هزینه های خدمات آزمایشگاهی: </Text>
                                                            <Text style={styles.bodyValue2}>{this.props.item.content.cover_laboratory/10} تومان</Text>
                                                        </View>
                                                        <View style={styles.bodyLeft2}>
                                                            <Text style={styles.bodyValue2}>{this.props.item.content.cover_eyes/10} تومان</Text>
                                                            <Text style={styles.bodyLabel2}>عیوب انکساری دو چشم: </Text>
                                                        </View>
                                                    </View>
                                                </View>
                                                :  null

                                        }
                                    </View>

                                    :
                                    <View style={styles.body}>
                                        <View style={[styles.regimContainer,{justifyContent: this.props.insurType === 'Third' ? 'space-between' : 'flex-end'} ]}>
                                            <View style={styles.bodyLeft}>
                                                <Text style={styles.bodyValue}>{this.props.item.insurance.shop_share} درصد</Text>
                                                <Text style={styles.bodyLabel}> سهم از بازار : </Text>
                                            </View>
                                            {
                                                this.props.insurType === 'Third'?
                                                    <View style={[styles.bodyLeft, {alignSelf: 'flex-end'}]}>
                                                        <Text style={styles.bodyValue}>{thirdBasePrice} تومان</Text>
                                                        <Text style={styles.bodyLabel}>مبلغ پایه بیمه:  </Text>
                                                    </View>
                                                    : null
                                            }
                                        </View>
                                        <View style={styles.regimContainer}>
                                            <View style={styles.bodyLeft}>
                                                <Text style={styles.bodyValue}>ندارد</Text>
                                                <Text style={styles.bodyLabel}>پرداخت خسارت سیار: </Text>
                                            </View>
                                            <View style={styles.bodyLeft}>
                                                <Text style={styles.bodyValue}>{this.props.item.insurance.off/10} تومان</Text>
                                                <Text style={styles.bodyLabel}>مبلغ تخفیف بیمه: </Text>
                                            </View>
                                        </View>
                                        <View style={styles.regimContainer}>
                                            <View style={styles.bodyLeft}>
                                                <Text style={styles.bodyValue}>{this.props.item.insurance.branches}</Text>
                                                <Text style={styles.bodyLabel}>تعداد مراکز پرداخت خسارت: </Text>
                                            </View>
                                            <View style={styles.bodyLeft}>
                                                <Text style={styles.bodyValue}>{this.props.item.insurance.customer_rate}</Text>
                                                <Text style={styles.bodyLabel}>رضایت مشتری(رتبه): </Text>
                                            </View>
                                        </View>
                                        <View style={styles.regimContainer}>
                                            <View style={styles.bodyLeft}>
                                                <View style={styles.startContainer}>
                                                    {
                                                        [...Array(parseInt( this.props.item.insurance.summary_rank)).keys()].map((item, index)=> <Icon key={index} name="star" size={10} color="rgba(255, 193, 39, 1)" />
                                                        )
                                                    }
                                                </View>
                                                <Text style={styles.bodyLabel}>سطوح توانگر مالی: </Text>
                                            </View>
                                            <View style={styles.bodyLeft}>
                                                <Text style={styles.bodyValue}>{this.props.item.insurance.delay}</Text>
                                                <Text style={styles.bodyLabel}>مدت پاسخگویی: </Text>
                                            </View>
                                        </View>
                                    </View>
                            )
                            : null
                    }
                    <View style={styles.footer}>
                        {
                            this.state.open ?
                                <TouchableOpacity onPress={() => this.setState({open: false})}>
                                    <Text style={styles.bodyLabel}>بستن اطلاعات بیشتر</Text>
                                </TouchableOpacity>
                                :
                                <TouchableOpacity onPress={() => this.setState({open: true})}>
                                    <Text style={styles.bodyLabel}>اطلاعات بیشتر</Text>
                                </TouchableOpacity>
                        }
                    </View>
                </View>
            </View>
        );
    }
}
export default InsuranceInfo;