

import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        // flex: .5,
        // width: '48%',
        alignItems: "center",
        justifyContent: 'center',
        backgroundColor: 'white',
        // paddingRight: 10,
        // paddingLeft: 30,
        // paddingBottom: 20,
        // paddingTop: 20,
        // borderRadius: 10,
        // elevation: 5,
        // marginRight: 5,
        margin: 10
    },
    left: {
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'flex-start',
    },
    label: {
        color: 'rgba(122, 130, 153, 1)',
        fontSize: 14,
        fontFamily: 'IRANSansMobile(FaNum)',

    },
    image: {
        width: 50,
        height: 50,
        resizeMode: 'contain'
    }
});

