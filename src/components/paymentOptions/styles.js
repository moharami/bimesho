
import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        // flex: 1,
        width: '100%',
        backgroundColor: 'white',
        position: 'relative',
        bottom: 25,
        zIndex: -1,
        elevation: 5,
        borderRadius: 10,
        paddingTop: 20
    },
    navContainer: {
        alignItems: "center",
        justifyContent: 'center',
        // paddingTop: 6,
        // paddingBottom: 3,
        position: 'relative'
    },
    searchButton: {
        width: '90%',
        height: 35,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgb(20, 122, 170)',
        marginBottom: 20
    },
    searchText:{
        color: 'white'
    },
    label: {
        fontFamily: 'IRANSansMobile(FaNum)',
        // position: 'absolute',
        // bottom: 0,
        paddingBottom: 20,
        textAlign: 'center'
    },
    image: {
        width: 50,
        height: 50,
        resizeMode: 'contain'
    },
    paymentButtonContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingBottom: 30
    },
    paymentButton: {
        flex: .3,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 4,
        // backgroundColor: 'red',
        borderColor: '#21b34f',
        borderWidth: 1
    },
    paymentText: {
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)'

    },
    portContainer: {
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'space-around',
    }
});