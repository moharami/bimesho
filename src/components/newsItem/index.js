import React, {Component} from 'react';
import {View, Text, TouchableOpacity, Image, ImageBackground} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import styles from './styles'
import {Actions} from 'react-native-router-flux'

class NewsItem extends Component {
    constructor(props){
        super(props);
    }
    render() {
        return (
            <TouchableOpacity style={styles.container} onPress={() =>Actions.blogDetail({item: this.props.item, hasCat: this.props.hasCat})}>
                {
                    this.props.item.attachments[0] !== undefined ?
                        <ImageBackground source={{uri: 'https://bimesho.azarinpro.info/files?uid='+this.props.item.attachments[0].uid+'&width=350&height=350'}}
                                         style={styles.subImage}>
                            <View style={{
                                backgroundColor:'rgba(0,0,0,.6)',
                                height: 140,
                                width: "100%",
                                paddingRight: 10,
                                paddingBottom: 10,
                                alignItems: "flex-end",
                                justifyContent: "flex-end",
                            }}>
                                <Text style={styles.imageText}>{this.props.item.title}</Text>
                            </View>
                        </ImageBackground> :
                        <ImageBackground source={{uri: 'http://jewel1067.com/wp-content/uploads/news.jpg'}}
                                         style={styles.subImage}>
                            <View style={{
                                backgroundColor:'rgba(0,0,0,.6)',
                                height: 140,
                                width: "100%",
                                paddingRight: 10,
                                paddingBottom: 10,
                                alignItems: "flex-end",
                                justifyContent: "flex-end",
                            }}>
                                <Text style={styles.imageText}>{this.props.item.title}</Text>
                            </View>
                        </ImageBackground>
                }

            </TouchableOpacity>

        );
    }
}
export default NewsItem;