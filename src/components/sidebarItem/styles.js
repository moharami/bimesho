
import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'flex-end',
        // paddingRight: 30,
        // paddingLeft: 30,
        // paddingBottom: 20,
        // paddingTop: 20,
        marginRight: 10,
        marginBottom: 20
    },
    label: {
        color: 'white',
        fontSize: 18,
        paddingRight: 20,
        fontFamily: 'IRANSansMobile(FaNum)',
    },

});


