import React, { Component } from 'react';
import {
    Modal,
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    ScrollView,
    TextInput,
    Alert,
    ActivityIndicator
}
    from 'react-native'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import Axios from 'axios'
    ;
export const url = 'http://banibime.com/api/v1';
Axios.defaults.baseURL = url;
// import PersianCalendarPicker from 'react-native-persian-calendar-picker';
import PersianCalendarPicker from './persian_calender/src/index';

class AlertModal extends Component {
    state = {
        modalVisible: false,
        value: '',
        lineAmount: 1,
        text: ''
    };
    render() {
        return (
            <View style = {styles.container}>
                <Modal animationType = {"fade"} transparent = {true}
                       visible = {this.props.modalVisible}
                       onRequestClose = {() => this.props.onChange(false)}
                       onBackdropPress= {() => this.props.onChange(!this.state.modalVisible)}
                >
                    <TouchableOpacity onPress={()=> this.props.closeModal()} style={styles.modal}>
                        <View style={{
                            // position: 'absolute',
                            // bottom: '65%',
                            // zIndex: 9999,
                            backgroundColor: '#21b34f',
                            borderRadius: 10
                        }}>
                            <PersianCalendarPicker
                                initialDate={this.props.input === 1 ? new Date().setFullYear(new Date().getFullYear() - 1) : undefined}
                                todayBackgroundColor="rgb(250, 189, 58)"
                                onDateChange={(date) => this.props.onDateChange(date, this.props.input)}
                            />
                        </View>
                    </TouchableOpacity>
                </Modal>
            </View>
        )
    }
}
export default AlertModal

const styles = StyleSheet.create ({
    container: {
        alignItems: 'center',
        backgroundColor: 'white',
        padding: 100,
        position: 'absolute',
        bottom: -200
    },
    modal: {
        flexGrow: 1,
        justifyContent: 'center',
        // flex: 1,
        alignItems: 'center',
        backgroundColor: 'rgba(0,0,0,.5)',
        // paddingRight: 55,
        // paddingLeft: 55
    },
    text: {
        color: '#3f2949',
        marginTop: 10
    },
    box: {
        width: '70%',
        height: 60,
        flexDirection: 'row',
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20,
        borderRadius: 10

    },
    buttonText: {
        fontSize: 15,
        color: 'white',
        fontWeight: 'bold',
        fontFamily: 'IRANSansMobile(FaNum)',
    },
    vacleContainer: {
        flexDirection: 'row',
        flexWrap: 'wrap'
    }
});