import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingBottom: 20
    },
    imageContainer: {
        backgroundColor: 'white',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 20,
        borderRadius: 100,
        elevation: 8
    },
    amount: {
        fontSize: 11,
        fontFamily: 'IRANSansMobile(FaNum)',
        paddingRight: 15
    },
    subamount: {
        fontSize: 8,
        fontFamily: 'IRANSansMobile(FaNum)',
        paddingRight: 15
    },
    iconRightContainer: {
        backgroundColor: 'rgba(51, 197, 117, 1)',
        // width: '40%',
        height: 38,
        borderRadius: 30,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        paddingLeft: 5,

    },
    topLabel: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 14,
        color: 'white',
        paddingRight: 10,
        paddingLeft: 7
    },
    right: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    iconRightEditContainer: {
        backgroundColor: 'rgba(51, 197, 117, 1)',
        width: 32,
        height: 32,
        borderRadius: 32,
        alignItems: 'center',
        justifyContent: 'center'
    },
});
