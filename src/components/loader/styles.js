import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        // padding: 30,
        width: '100%',

    },
    text: {
        textAlign: 'center',
        fontSize:16,
        // paddingTop: 20,
        fontFamily: 'IRANSansMobile(FaNum)'
    }
});