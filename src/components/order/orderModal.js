import React, { Component } from 'react';
import {
    Modal,
    Text,
    View,
    StyleSheet,
    Alert,
    TouchableOpacity,
    Image,
    ScrollView
}
    from 'react-native'
import Axios from 'axios'
;
export const url = 'https://bimehsho.com/api/v1';
Axios.defaults.baseURL = url;
import FIcon from 'react-native-vector-icons/Feather'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import moment_jalaali from 'moment-jalaali'
import moment from 'moment'

class OrderModal extends Component {
    state = {
        modalVisible: false,
        value: '',
        lineAmount: 1,
        text: ''
    };
    handleUpdate() {
        let midterm = {"weight": 0, "description": ""};
        midterm.weight = this.state.value;
        midterm.description = this.state.text;
        console.log('midterm', midterm);

        try {
            Axios.post('/training/details/midterm', {
                midterm: JSON.stringify(midterm),
            }).then(response=> {
                Alert.alert('','پروفایل شما بروزرسانی شد');

            })
            .catch((error) => {
                    Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                    this.setState({loading: false});
                }
            );
        }
        catch (error) {
            console.log(error)
            Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
            this.setState({loading: false});
        }
    }
    render() {
        const newValue =  Math.floor(this.props.item.data.factorInfo.dataSelect.fianl_price).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

        return (
            <View style = {styles.container}>
                <Modal animationType = {"fade"} transparent = {true}
                       visible = {this.props.modalVisible}
                       onRequestClose = {() => this.props.onChange(false)}
                       onBackdropPress= {() => this.props.onChange(!this.state.modalVisible)}
                >
                    <View style = {styles.modal} >
                        <ScrollView style={styles.scroll}>
                            <View style={styles.allContainer}>
                                <View style={styles.header}>
                                    <View style={styles.headerleft}>
                                        <View style={styles.info}>
                                            <Text style={styles.labelInfo}>{this.props.title}</Text>
                                            <View style={styles.price}>
                                                <Text style={styles.priceLabel}>تومان</Text>
                                                <Text style={styles.amount}>{newValue}</Text>
                                            </View>
                                        </View>
                                        <Image source={this.props.imgSrc} style={styles.Image} />
                                    </View>
                                    <View style={styles.headerright}>
                                        <View style={styles.rightContainer}>
                                            <Text style={styles.label}>{this.props.typeLabel}</Text>
                                        </View>
                                    </View>
                                </View>
                                <View style={styles.body}>
                                    <View style={styles.left}>
                                        <View style={styles.bodyLeft}>
                                            <Text style={styles.redValue}>{this.props.status}</Text>
                                            <Text style={styles.bodyLabel}>وضعیت سفارش : </Text>
                                        </View>
                                        <View style={styles.bodyLeft}>
                                            <Text style={styles.bodyValue}>نقدی</Text>
                                            <Text style={styles.bodyLabel}> نوع خرید : </Text>
                                        </View>
                                    </View>
                                    <View style={styles.right}>
                                        <View style={styles.bodyLeft}>
                                            <Text style={styles.bodyValue}>{this.props.item.id}</Text>
                                            <Text style={styles.bodyLabel}>کد رهگیری : </Text>
                                        </View>
                                        <View style={styles.bodyLeft}>
                                            <Text style={styles.bodyValue}>{this.props.time}</Text>
                                            <Text style={styles.bodyLabel}>تاریخ ایجاد : </Text>
                                        </View>
                                    </View>
                                </View>
                                <View style={styles.body}>
                                    <View style={styles.left}>
                                        <View style={styles.bodyLeft}>
                                            <Text style={styles.bodyValue}>{this.props.title}</Text>
                                            <Text style={styles.bodyLabel}> شرکت بیمه: </Text>
                                        </View>
                                        <View style={styles.bodyLeft}>
                                            <Text style={styles.bodyValue}>{newValue} ریال</Text>
                                            <Text style={styles.bodyLabel}> مبلغ کل بیمه: </Text>
                                        </View>
                                        {
                                            this.props.item.type === 'Third' || this.props.item.type === 'motor' ?
                                                <View style={styles.innerLeft}>
                                                    <View style={styles.bodyLeft}>
                                                        <Text style={styles.bodyValue}>{this.props.item.data.factorInfo.commitments}</Text>
                                                        <Text style={styles.bodyLabel}>حداکثر مقدار پوشش: </Text>
                                                    </View>
                                                    {/*<View style={styles.bodyLeft}>*/}
                                                        {/*<Text style={styles.bodyValue}>{this.props.item.data.factorInfo.}</Text>*/}
                                                        {/*<Text style={styles.bodyLabel}>مبلغ پایه بیمه: </Text>*/}
                                                    {/*</View>*/}
                                                    <View style={styles.bodyLeft}>
                                                        <Text style={styles.bodyValue}>{this.props.item.data.factorInfo.third_car_years} سال</Text>
                                                        <Text style={styles.bodyLabel}> تخفیف بیمه: </Text>
                                                    </View>
                                                </View>
                                                : null
                                        }

                                    </View>

                                    <View style={styles.right}>
                                        {
                                            this.props.item.type ===  'life'?
                                                <View style={styles.innerRight}>
                                                    <View style={styles.bodyLeft}>
                                                        <Text style={styles.bodyValue}>{this.props.item.data.factorInfo.dataSelect.life_pay}</Text>
                                                        <Text style={styles.bodyLabel}>نحوه پرداخت: </Text>
                                                    </View>
                                                    <View style={styles.bodyLeft}>
                                                        <Text style={styles.bodyValue}>{this.props.item.data.factorInfo.dataSelect.life_years}</Text>
                                                        <Text style={styles.bodyLabel}> مدت قرارداد: </Text>
                                                    </View>
                                                    <View style={styles.bodyLeft}>
                                                        <Text style={styles.bodyValue}>{this.props.item.data.factorInfo.dataSelect.life_cost}</Text>
                                                        <Text style={styles.bodyLabel}> حق بیمه: </Text>
                                                    </View>
                                                </View>
                                                : null
                                        }
                                        {
                                            this.props.item.type ===  'travel'?
                                                <View style={styles.innerRight}>
                                                    <View style={styles.bodyLeft}>
                                                        <Text style={styles.bodyValue}>{this.props.item.data.factorInfo.dataSelect.travel_country}</Text>
                                                        <Text style={styles.bodyLabel}>کشور: </Text>
                                                    </View>
                                                    <View style={styles.bodyLeft}>
                                                        <Text style={styles.bodyValue}>{this.props.item.data.factorInfo.dataSelect.travel_type}</Text>
                                                        <Text style={styles.bodyLabel}>نوع بیمه: </Text>
                                                    </View>
                                                    <View style={styles.bodyLeft}>
                                                        <Text style={styles.bodyValue}>{this.props.item.data.factorInfo.dataSelect.travel_time}</Text>
                                                        <Text style={styles.bodyLabel}>مدت اقامت: </Text>
                                                    </View>
                                                    <View style={styles.bodyLeft}>
                                                        <Text style={styles.bodyValue}>{this.props.item.data.factorInfo.dataSelect.travel_age}</Text>
                                                        <Text style={styles.bodyLabel}>سن: </Text>
                                                    </View>
                                                </View>
                                                : null
                                        }
                                        {
                                            this.props.item.type ===  'fire'?
                                                <View style={styles.innerRight}>
                                                    <View style={styles.bodyLeft}>
                                                        <Text style={styles.bodyValue}>{this.props.item.data.factorInfo.dataSelect.fire_home_type}</Text>
                                                        <Text style={styles.bodyLabel}>نوع ملک: </Text>
                                                    </View>
                                                    <View style={styles.bodyLeft}>
                                                        <Text style={styles.bodyValue}>{this.props.item.data.factorInfo.dataSelect.fire_structure}</Text>
                                                        <Text style={styles.bodyLabel}>نوع سازه: </Text>
                                                    </View>
                                                    <View style={styles.bodyLeft}>
                                                        <Text style={styles.bodyValue}>{Math.floor(this.props.item.data.factorInfo.dataSelect.fire_home_price).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</Text>
                                                        <Text style={styles.bodyLabel}>ارزش لوازم خانگی: </Text>
                                                    </View>
                                                    <View style={styles.bodyLeft}>
                                                        <Text style={styles.bodyValue}>{this.props.item.data.factorInfo.dataSelect.fire_home_count}</Text>
                                                        <Text style={styles.bodyLabel}>تعداد واحد: </Text>
                                                    </View>
                                                    <View style={styles.bodyLeft}>
                                                        <Text style={styles.bodyValue}>{this.props.item.data.factorInfo.dataSelect.fire_meters}</Text>
                                                        <Text style={styles.bodyLabel}>متراژ مورد بیمه: </Text>
                                                    </View>
                                                </View>
                                                : null
                                        }
                                        {
                                            this.props.item.type ===  'complete'?
                                                <View style={styles.innerRight}>
                                                    <View style={styles.bodyLeft}>
                                                        <Text style={styles.bodyValue}>{this.props.item.data.factorInfo.dataSelect.resp_type}</Text>
                                                        <Text style={styles.bodyLabel}>نوع بیمه: </Text>
                                                    </View>
                                                    <View style={styles.bodyLeft}>
                                                        <Text style={styles.bodyValue}>{this.props.item.data.factorInfo.dataSelect.insurancer}</Text>
                                                        <Text style={styles.bodyLabel}>بیمه گر پایه: </Text>
                                                    </View>
                                                </View>
                                                : null
                                        }
                                        {
                                            this.props.item.type ===  'responsible'?
                                                <View style={styles.innerRight}>
                                                    <View style={styles.bodyLeft}>
                                                        <Text style={styles.bodyValue}>{this.props.item.data.factorInfo.dataSelect.resp_type_text}</Text>
                                                        <Text style={styles.bodyLabel}>نوع بیمه: </Text>
                                                    </View>
                                                    <View style={[styles.bodyLeft, {flexDirection: 'column'} ]}>
                                                        <Text style={styles.bodyLabel}>نوع تخصص: </Text>
                                                        <Text style={styles.bodyValue}>{this.props.item.data.factorInfo.dataSelect.res_cost}</Text>
                                                    </View>
                                                    <View style={styles.bodyLeft}>
                                                        <Text style={styles.bodyValue}>{this.props.item.data.factorInfo.dataSelect.res_job}</Text>
                                                        <Text style={styles.bodyLabel}>تعداد دیه: </Text>
                                                    </View>
                                                    <View style={styles.bodyLeft}>
                                                        <Text style={styles.bodyValue}>{this.props.item.data.factorInfo.dataSelect.res_time}</Text>
                                                        <Text style={styles.bodyLabel}>مدت بیمه نامه: </Text>
                                                    </View>
                                                </View>
                                                : null
                                        }
                                        {
                                            this.props.item.type === 'Third' || this.props.item.type === 'motor' ?
                                                <View style={styles.innerRight}>
                                                    <View style={styles.bodyLeft}>
                                                        <Text style={styles.bodyValue}>{this.props.item.data.factorInfo.dataSelect.third_car_cate}</Text>
                                                        <Text style={styles.bodyLabel}> نوع وسیله نقلیه: </Text>
                                                    </View>
                                                    <View style={styles.bodyLeft}>
                                                        <Text style={styles.bodyValue}>{this.props.item.data.factorInfo.dataSelect.third_car_name}</Text>
                                                        <Text style={styles.bodyLabel}>نام وسیله نقلیه:  </Text>
                                                    </View>
                                                    <View style={[styles.bodyLeft, {flexDirection: 'column'} ]}>
                                                        <Text style={styles.bodyLabel}>مدل وسیله نقلیه:  </Text>
                                                        <Text style={styles.bodyValue}>{this.props.item.data.factorInfo.dataSelect.third_car_model}</Text>
                                                    </View>
                                                    <View style={styles.bodyLeft}>
                                                        <Text style={styles.bodyValue}>{this.props.item.data.factorInfo.dataSelect.third_car_years}</Text>
                                                        <Text style={styles.bodyLabel}>سال ساخت: </Text>
                                                    </View>
                                                    <View style={styles.bodyLeft}>
                                                        <Text style={styles.bodyValue}>{this.props.item.data.factorInfo.dataSelect.third_dmg_count}</Text>
                                                        <Text style={styles.bodyLabel}>تخفیف عدم خسارت: </Text>
                                                    </View>
                                                    <View style={styles.bodyLeft}>
                                                        <Text style={styles.bodyValue}>{this.props.item.data.factorInfo.dataSelect.third_dmg_years}</Text>
                                                        <Text style={styles.bodyLabel}>سالهای عدم خسارت: </Text>
                                                    </View>
                                                </View>
                                                : null
                                        }
                                        {
                                            this.props.item.type === 'body' ?
                                                <View style={styles.innerRight}>
                                                    <View style={styles.bodyLeft}>
                                                        <Text style={styles.bodyValue}>{this.props.item.data.factorInfo.dataSelect.body_car_cate}</Text>
                                                        <Text style={styles.bodyLabel}> نوع وسیله نقلیه: </Text>
                                                    </View>
                                                    <View style={styles.bodyLeft}>
                                                        <Text style={styles.bodyValue}>{this.props.item.data.factorInfo.dataSelect.body_car_name}</Text>
                                                        <Text style={styles.bodyLabel}>نام وسیله نقلیه:  </Text>
                                                    </View>
                                                    <View style={[styles.bodyLeft, {flexDirection: 'column'} ]}>
                                                        <Text style={styles.bodyLabel}>مدل وسیله نقلیه:  </Text>
                                                        <Text style={styles.bodyValue}>{this.props.item.data.factorInfo.dataSelect.body_car_model}</Text>
                                                    </View>
                                                    <View style={styles.bodyLeft}>
                                                        <Text style={styles.bodyValue}>{this.props.item.data.factorInfo.dataSelect.body_car_years}</Text>
                                                        <Text style={styles.bodyLabel}>سال ساخت: </Text>
                                                    </View>
                                                </View>
                                                : null
                                        }
                                    </View>
                                </View>
                                <View style={styles.body}>
                                    <View style={styles.address}>
                                        <View style={styles.bodyLeft}>
                                            <Text style={styles.bodyValue}>{this.props.item.user_details.address}</Text>
                                            <Text style={styles.bodyLabel}> نشانی درج: </Text>
                                        </View>
                                        <View style={styles.bodyLeft}>
                                            <Text style={styles.bodyValue}>{this.props.item.user_details.new_address}</Text>
                                            <Text style={styles.bodyLabel}> نشانی ارسال: </Text>
                                        </View>

                                    </View>
                                    <View style={styles.right}>
                                        <View style={styles.bodyLeft}>
                                            <Text style={styles.bodyValue}>{this.props.item.user_details.fname}</Text>
                                            <Text style={styles.bodyLabel}> نام: </Text>
                                        </View>
                                        <View style={styles.bodyLeft}>
                                            <Text style={styles.bodyValue}>{this.props.item.user_details.lname}</Text>
                                            <Text style={styles.bodyLabel}>نام خانوادگی: </Text>
                                        </View>
                                        <View style={styles.bodyLeft}>
                                            <Text style={styles.bodyValue}>{this.props.item.user_details.national_id}</Text>
                                            <Text style={styles.bodyLabel}>کدملی: </Text>
                                        </View>
                                        <View style={styles.bodyLeft}>
                                            <Text style={styles.bodyValue}>{this.props.item.user_details.mobile}</Text>
                                            <Text style={styles.bodyLabel}>تلفن همراه: </Text>
                                        </View>
                                        <View style={styles.bodyLeft}>
                                            <Text style={styles.bodyValue}>{this.props.item.user_details.tel}</Text>
                                            <Text style={styles.bodyLabel}>تلفن: </Text>
                                        </View>
                                        <View style={styles.bodyLeft}>
                                            <Text style={styles.bodyValue}>{moment_jalaali(this.props.item.user_details.birthday).format('jYYYY/jM/jD')}</Text>
                                            <Text style={styles.bodyLabel}>تاریخ تولد: </Text>
                                        </View>
                                    </View>
                                </View>
                                {
                                    this.props.item.payment_status !== 'payment' ?
                                        <TouchableOpacity onPress={() => {this.props.closeModal();Actions.payment({openDrawer: this.props.openDrawer, invoice: this.props.item.invoice, factor:  this.props.item.data.factorInfo, insurType: this.props.item.type,pageTitle: 'پرداخت', invoice_id: this.props.item.invoice_id})}} style={styles.iconLeftContainer}>
                                            <FIcon name="arrow-left" size={18} color="white" style={{borderColor: 'white', borderWidth: 1, borderRadius: 20, padding: 3, marginRight: 5}} />
                                            <Text style={styles.labelBottom}> پرداخت</Text>
                                        </TouchableOpacity>
                                        : null
                                }
                                <TouchableOpacity onPress={() => this.props.closeModal()} style={styles.closeContainer}>
                                    <Icon name="close" size={15} color={"white"} />
                                </TouchableOpacity>
                            </View>
                        </ScrollView>
                    </View>
                </Modal>
            </View>
        )
    }
}
export default OrderModal

const styles = StyleSheet.create ({
    container: {
        alignItems: 'center',
        backgroundColor: 'white',
        padding: 100,
        position: 'absolute',
        bottom: -200
    },
    modal: {
        flexGrow: 1,
        justifyContent: 'center',
        // flex: 1,
        alignItems: 'center',
        backgroundColor: 'rgba(0,0,0,.5)',
        paddingRight: 15,
        paddingLeft: 15,
        paddingTop: 20,
        paddingBottom: 5,
    },
    text: {
        color: '#3f2949',
        marginTop: 10
    },
    body: {
        paddingRight: 10,
        paddingTop: 6,
        paddingBottom: 6,
        borderTopColor: 'rgb(237, 237, 237)',
        borderTopWidth: 1,
        backgroundColor: 'white',
        borderRadius: 10,
        // elevation: 5,
        width: '100%',
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'space-between',
        // marginBottom: 8
    },
    left: {
        width: '50%',
        alignItems: 'flex-end',
        justifyContent: 'flex-start',
        marginBottom: 8,
    },
    innerLeft: {
        width: '100%',
        alignItems: 'flex-end',
        justifyContent: 'flex-start',
    },
    right: {
        width: '50%',
        alignItems: 'flex-end',
        justifyContent: 'flex-start',
        marginBottom: 8,
    },
    innerRight: {
        width: '100%',
        alignItems: 'flex-end',
        justifyContent: 'flex-start',
    },
    bodyLeft: {
        flexDirection: 'row',
        paddingLeft: 30
    },
    bodyRight: {
        flexDirection: 'row'
    },
    bodyLabel: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 10,
        paddingBottom: 7,
        color: 'rgba(122, 130, 153, 1)'
    },
    bodyValue: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 10,
        color: 'rgba(51, 54, 64, 1)',
        paddingBottom: 7

    },




    scroll: {
        // backgroundColor: 'white',
        width: '100%',
        // borderRadius: 15
    },
    allContainer: {
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        borderRadius: 15
    },
    header: {
        flexDirection: 'row',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingRight: 10,
        borderBottomColor: 'rgb(237, 237, 237)',
        borderBottomWidth: 1,

    },
    info: {
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
        padding: 10
    },
    price: {
        flexDirection: 'row'
    },
    priceLabel: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 12,
        paddingRight: 5
    },
    amount: {
        fontSize: 16,
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    Image: {
        width: 50,
        resizeMode: 'contain',
        height: 50,
        marginTop: 5
    },
    headerleft: {
        flexDirection: 'row'
    },
    // headerright: {
    //     // flexDirection: 'row',
    // },
    rightContainer: {
        // flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingRight: 3,
        paddingLeft: 3,
        // borderRadius: 20,
        backgroundColor: 'rgba(122, 130, 153, 1)',
        // overflow: 'hidden'
        borderBottomRightRadius: 10,
        borderBottomLeftRadius: 10,
        borderTopRightRadius: 10,
        // overflow: 'hidden',

    },
    label: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 14,
        color: 'white'
    },
    address: {
        width: '30%',
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        marginBottom: 8,
    },
    iconLeftContainer: {
        backgroundColor: 'rgba(51, 197, 117, 1)',
        // width: '40%',
        // height: 32,
        borderRadius: 30,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        padding: 5,
        marginBottom: 15
    },
    labelBottom: {
        paddingLeft: 15,
        paddingRight: 10,
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 15,
    },
    closeContainer: {
        width: 30,
        height: 30,
        borderRadius: 30,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(0,0,0, .3)',
        padding: 10,
        marginBottom: 10
    }

});