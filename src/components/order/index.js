import React, {Component} from 'react';
import {View, Text, TouchableOpacity, Image, BackHandler, AsyncStorage, Alert} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import FIcon from 'react-native-vector-icons/dist/Feather';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import ProfileModal from './orderModal'
import moment_jalaali from 'moment-jalaali'
import Axios from 'axios'
;
export const url = 'https://bimehsho.com/api/v1';
Axios.defaults.baseURL = url;
import pic2 from '../../assets/insurances/2.png'
import pic3 from '../../assets/insurances/3.png'
import pic4 from '../../assets/insurances/4.png'
import pic5 from '../../assets/insurances/5.png'
import pic6 from '../../assets/insurances/6.png'
import pic7 from '../../assets/insurances/7.png'
import pic8 from '../../assets/insurances/8.png'
import pic9 from '../../assets/insurances/9.png'
import pic10 from '../../assets/insurances/10.png'
import pic11 from '../../assets/insurances/11.png'
import pic12 from '../../assets/insurances/12.png'
import pic13 from '../../assets/insurances/13.png'
import pic14 from '../../assets/insurances/14.png'
import pic15 from '../../assets/insurances/15.png'
import pic18 from '../../assets/insurances/18.png'
import pic19 from '../../assets/insurances/19.png'
import pic20 from '../../assets/insurances/20.png'
import pic21 from '../../assets/insurances/21.png'
import pic22 from '../../assets/insurances/22.png'
import pic23 from '../../assets/insurances/23.png'
import Loader from '../../components/loader'
import AlertView from '../../components/modalMassage'

class Order extends Component {
    constructor(props){
        super(props);
        this.state = {
            modalVisible: false,
            modalVisible2: false,
            title: '',
            loading: false,
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress(){
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
        // this.setState({loading: true});
        // Axios.post('/request/get_insurance_detail', {
        //     id: this.props.item.insurance_id
        // }).then(response=> {
        //     this.setState({loading: false, title: response.data.data[0].title})
        // })
        // .catch((error) => {
        //     console.log(error)
        //     // Alert.alert('','خطا');
        //     // this.setState({loading: false});
        //     this.setState({modalVisible2: true, loading: false});
        //
        //
        // });
    }
    setModalVisible() {
        this.setState({modalVisible: !this.state.modalVisible});
    }
    closeModal() {
        this.setState({modalVisible: false, modalVisible2: false});
    }
    render() {
        console.log('this.props.item', this.props.item)
        let status = '';
        switch(this.props.item.status) {
            case 'created':
                status = 'ایجاد شده';
                break;
            case 'pendding':
                status = 'در حال بررسی';
                break;
            case 'penddingToPublish':
                status = 'در حال صدور';
                break;
            case 'published':
                status = 'صادر شده';
                break;
            case 'send':
                status = 'ارسال شده';
                break;
            case 'delivery':
                status = 'دریافت شده توسط مشتری';
                break;
            case 'open':
                status = 'باز';
                break;
            case 'closed':
                status = 'بسته';
                break;
        }
        const id = this.props.item.insurance_id;
        let imgSrc = null;
        switch (id) {
            case 2:
                imgSrc = pic2;
                break;
            case 3:
                imgSrc = pic3;
                break;
            case 4:
                imgSrc = pic4;
                break;
            case 5:
                imgSrc = pic5;
                break;
            case 6:
                imgSrc = pic6;
                break;
            case 7:
                imgSrc = pic7;
                break;
            case 8:
                imgSrc = pic8;
                break;
            case 9:
                imgSrc = pic9;
                break;
            case 10:
                imgSrc = pic10;
                break;
            case 11:
                imgSrc = pic11;
                break;
            case 12:
                imgSrc = pic12;
                break;
            case 13:
                imgSrc = pic13;
                break;
            case 14:
                imgSrc = pic14;
                break;
            case 15:
                imgSrc = pic5;
                break;
            case 18:
                imgSrc = pic18;
                break;
            case 19:
                imgSrc = pic19;
                break;
            case 20:
                imgSrc = pic20;
                break;
            case 21:
                imgSrc = pic21;
                break;
            case 22:
                imgSrc = pic22;
                break;
            case 23:
                imgSrc = pic23;
                break;
        }
        const type = this.props.item.type;
        let typeLabel = '';
        switch(type) {
            case 'Third':
                typeLabel = 'بیمه شخص ثالث';
                break;
            case 'body':
                typeLabel = 'بیمه بدنه';
                break;
            case 'fire':
                typeLabel = 'بیمه آتش سوزی';
                break;
            case 'complete':
                typeLabel = 'بیمه درمان انفرادی';
                break;
            case 'travel':
                typeLabel = 'بیمه مسافرتی';
                break;
            case 'responsible':
                typeLabel = 'بیمه مسئولیت پزشکی';
                break;
            case 'life':
                typeLabel = 'بیمه عمر';
                break;
            case 'motor':
                typeLabel = 'بیمه موتورسیکلت';
                break;
            case 'jobs':
                typeLabel = 'بیمه مسئولیت مشاغل';
                break;
        }
        const newValue =  Math.floor(this.props.item.data.factorInfo.dataSelect.fianl_price).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        if(this.state.loading){
            return (<Loader />)
        }
        else
        return (
            <View style={styles.topContainer}  onPress={() => null}>
                <View style={styles.container}>
                    <View style={styles.header}>
                        <View style={styles.left}>
                            <View style={styles.info}>
                                <Text style={styles.labelInfo}>{this.props.item.data.insurance.title}</Text>
                                <View style={styles.price}>
                                    <Text style={styles.priceLabel}>تومان</Text>
                                    <Text style={styles.amount}>{newValue}</Text>
                                </View>
                            </View>
                            <Image source={imgSrc} style={styles.Image} />
                        </View>
                        <View style={styles.right}>
                            <View style={styles.rightContainer}>
                                <Text style={styles.label}>{typeLabel}</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.body}>
                        <View style={styles.regimContainer}>
                            <View style={styles.bodyLeft}>
                                <Text style={styles.redValue}>{status}</Text>
                                <Text style={styles.bodyLabel}>وضعیت سفارش : </Text>
                            </View>
                            <View style={styles.bodyRight}>
                                <Text style={styles.bodyValue}>نقدی</Text>
                                <Text style={styles.bodyLabel}> نوع خرید : </Text>
                            </View>
                        </View>
                        <View style={styles.regimContainer}>
                            <View style={styles.bodyLeft}>
                                <Text style={styles.bodyValue}>{this.props.item.id}</Text>
                                <Text style={styles.bodyLabel}>کد رهگیری: </Text>
                            </View>
                            <View style={styles.bodyLeft}>
                                <Text style={styles.bodyValue}>{moment_jalaali(this.props.item.created_at).format('jYYYY/jM/jD')}</Text>
                                <Text style={styles.bodyLabel}>تاریخ ایجاد: </Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.footer}>
                        {/*<View style={styles.iconLeftContainer}>*/}
                            {/*<Icon name="trash-o" size={18} color="white" />*/}
                        {/*</View>*/}
                        <View style={styles.iconRightContainer}>
                            <FIcon name="shopping-cart" size={18} color="white" style={{borderColor: 'white', borderWidth: 1, borderRadius: 20, padding: 3, marginRight: 5}} />
                            <TouchableOpacity onPress={() => this.setState({modalVisible: !this.state.modalVisible })}>
                                <Text style={styles.label}>مشاهده فاکتور </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <ProfileModal
                    closeModal={() => this.closeModal()}
                    modalVisible={this.state.modalVisible}
                    onChange={(visible) => this.setState({modalVisible: false})}
                    item={this.props.item}
                    imgSrc={imgSrc}
                    title={this.props.item.data.insurance.title}
                    typeLabel={typeLabel}
                    status={status}
                    time={moment_jalaali(this.props.item.created_at).format('jYYYY/jM/jD')}
                />
                <AlertView
                    closeModal={(title) => this.closeModal(title)}
                    modalVisible={this.state.modalVisible2}
                    modelCats={this.state.modelCats}
                    title={this.state.dateR ? 'لطفا تاریخ انقضا را وارد نمایید' : (this.state.updateR ? 'یادآوری با موفقیت به روزرسانی شد' : (this.state.deleteR ? 'یادآوری با موفقیت حذف شد' : 'مشکلی در برقراری ارتباط با سرور به وجود آمده لطفا دوباره سعی کنید' )) }
                />
            </View>
        );
    }
}
export default Order;