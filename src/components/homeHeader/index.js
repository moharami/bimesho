import React, {Component} from 'react';
import {TouchableOpacity, View, Text, BackHandler} from 'react-native';
import styles from './styles'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import FIcon from 'react-native-vector-icons/dist/Feather';
import LinearGradient from 'react-native-linear-gradient';
import {Actions} from 'react-native-router-flux'
import Steps from "../steps/index";

class HomeHeader extends Component {

    constructor(props){
        super(props);
        this.state = {
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    render() {
        return (
            <LinearGradient  start={{x: 0, y: 1}} end={{x: 1, y: 1}} colors={['#21b34f', '#95cc3a']} style={styles.container}>
                <View style={styles.top}>
                    <TouchableOpacity onPress={() => this.onBackPress()}>
                        <FIcon name="chevron-left" size={23} color="black" />
                    </TouchableOpacity>
                    <Text style={styles.headerTitle}>{this.props.pageTitle}</Text>
                    <TouchableOpacity onPress={() => Actions.drawerOpen()}>
                        <Icon name="bars" size={20} color="black" />
                    </TouchableOpacity>
                </View>
                <Steps active={this.props.active} />
            </LinearGradient>
        );
    }
}
export default HomeHeader;