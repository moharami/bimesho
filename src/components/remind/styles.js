import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    topContainer: {
        width: '100%',
        // height: '100%',
        borderRadius: 10,
        marginBottom: 30,
        elevation: 4
    },
    container: {
        flex: 1,
        backgroundColor: 'white',
        // borderColor: 'lightgray',
        // borderWidth: 1,
        borderRadius: 10,
        elevation: 4,
        position: 'relative'
    },
    header: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomColor: 'rgb(237, 237, 237)',
        borderBottomWidth: 1,
        padding: 20
    },
    headerLabel: {
        fontSize: 18,
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'center',
    },
    info: {
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
        padding: 10
    },
    price: {
        flexDirection: 'row'
    },
    priceLabel: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 12,
        paddingRight: 5
    },
    amount: {
        fontSize: 16,
        color: 'black'
    },
    Image: {
        width: 50,
        resizeMode: 'contain',
        height: 50,
        marginTop: 5
    },
    regimContainer: {
        width: '100%',
        // flexDirection: 'row',
        alignItems: 'flex-end',
        justifyContent: 'center',
        marginBottom: 8,
    },
    left: {
        flexDirection: 'row',
    },
    right: {
        // flexDirection: 'row',
    },
    rightContainer: {
        // flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingRight: 3,
        paddingLeft: 3,
        // borderRadius: 20,
        backgroundColor: 'rgba(122, 130, 153, 1)',
        // overflow: 'hidden'
        borderBottomRightRadius: 10,
        borderBottomLeftRadius: 10,
        borderTopRightRadius: 10,
        // overflow: 'hidden',

    },
    label: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 14,
        color: 'rgba(122, 130, 153, 1)'
    },
    edit: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 14,
        color: 'white'
    },
    value: {
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'black',
        fontSize: 14
    },
    redValue: {
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'red',
        fontSize: 12
    },
    body: {
        width: '100%',
        padding: 5,
        paddingTop: 15,
        paddingBottom: 15,
        borderBottomColor: 'rgb(237, 237, 237)',
        borderBottomWidth: 1,
    },
    bodyLeft: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
        paddingTop: 4,
        paddingBottom: 4
    },
    bodyRight: {
        flexDirection: 'row',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
    bodyLabel: {
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'rgb(150, 150, 150)',
        fontSize: 11
    },
    bodyValue: {
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'black',
        fontSize: 11
    },
    iconLeftContainer: {
        backgroundColor: 'rgba(255, 45, 85, 1)',
        width: 32,
        height: 32,
        borderRadius: 32,
        alignItems: 'center',
        justifyContent: 'center',
    },
    iconLeftEditContainer: {
        backgroundColor: 'rgba(170, 170, 170, 1)',
        width: 32,
        height: 32,
        borderRadius: 32,
        alignItems: 'center',
        justifyContent: 'center'
    },
    iconRightEditContainer: {
        backgroundColor: 'rgba(51, 197, 117, 1)',
        width: 32,
        height: 32,
        borderRadius: 32,
        alignItems: 'center',
        justifyContent: 'center'
    },
    iconRightContainer: {
        backgroundColor: '#21b34f',
        // width: '40%',

        height: 32,
        borderRadius: 30,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        paddingLeft: 5,
        paddingRight: 10
    },
    footer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 15
    },


    textContainer: {
        flex: 1,
        width: '100%',
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'space-between',
        height: 47,
        backgroundColor: 'white',
        paddingRight: 10,
        paddingLeft: 10,
        paddingBottom: 1
    },
    textLeft: {
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'flex-start',
    },
    picker: {
        height: 40,
        backgroundColor: 'white',
        width: '100%',
        color: 'gray',
        // borderColor: 'lightgray',
        // borderWidth: 1,
        // elevation: 4,
        borderRadius: 10,
        // overflow: 'hidden'

    }
});