import React, {Component} from 'react';
import {Text, View, ScrollView, Image, TouchableOpacity} from 'react-native';
import styles from './styles'
import 'moment/locale/fa';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import {Actions} from 'react-native-router-flux'
import moment_jalaali from 'moment-jalaali'
import f from '../../assets/instalment2.png'
class NewSlide extends Component {
    // componentWillMount() {
    //     this.props.headerCategories.map((item , index)=> {
    //             if(item.id === ){
    //
    //             }
    //     })
    // }
    render() {
        console.log(' category title', this.props.item.category.title)
        return (
            <TouchableOpacity style={styles.container} onPress={() => Actions.blogDetail({item: this.props.item})}>
                {
                    this.props.item.attachments[0] !== undefined ?
                        <Image source={{uri: 'https://bimesho.azarinpro.info/files?uid='+this.props.item.attachments[0].uid+'&width=350&height=350'}} style={styles.image} />
                        : null
                }
                {/*{*/}
                    {/*this.props.item.attachments[0] !== undefined ?*/}
                        {/*<Image source={f} style={styles.image} />*/}
                        {/*: null*/}
                {/*}*/}
                <View style={styles.content}>
                    <Text style={styles.contentText}>{this.props.item.title.substr(0, 50)+ '...'} </Text>
                </View>
            </TouchableOpacity>
        );
    }
}
export default NewSlide;