import { StyleSheet } from 'react-native';
// import env from '../../colors/env';
export default StyleSheet.create({
    container: {
        flex: 1,
        width: 150,
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        marginLeft: 20,
        backgroundColor: 'white',
        borderRadius: 12,
        // elevation: 6
        // elevation:4,
        // shadowOffset: { width: 5, height: 5 },
        // shadowColor: "grey",
        // shadowOpacity: 0.5,
        // shadowRadius: 10,
    },
    image: {
        width: 100,
        height: 100,
        flex: 1,
        borderRadius: 100,
        marginTop: 20,
        resizeMode: 'contain',
        // resizeMode:'contain',
        // overflow: 'hidden',
        alignSelf: 'center'
    },
    caption: {
        fontSize: 14,
        color: 'black',
        paddingTop: 8,
        fontFamily: 'IRANSansMobile(FaNum)',
    },
    date: {
        fontSize: 13,
        color: 'lightgray',
        paddingTop: 5,
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    title: {
        color: 'rgb(70, 70, 70)',
        paddingBottom: 5,
        paddingTop: 10,
        lineHeight: 30,
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 16
    },
    advRow: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 5
    },
    iconContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 5
    },
    content: {
        backgroundColor: 'white',
        paddingRight: 15,
        paddingLeft: 10,
        paddingBottom: 10,
        width: '100%',
        borderRadius: 12
    },
    contentText: {
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'rgb(50, 50, 50)',
        lineHeight: 20,
        paddingBottom: 20,
        paddingTop: 20,
        fontSize: 10
    },
    labelContainer: {
        alignItems: 'flex-end',
        justifyContent: 'flex-start'
    },
    bodyText: {
        color: 'black',
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    footerText: {
        color: 'rgba(122, 130, 153, 1)',
        fontSize: 12,
        fontFamily: 'IRANSansMobile(FaNum)'
    },

});
