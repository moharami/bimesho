
import React, {Component} from 'react';
import {Text, View, Image, TouchableOpacity} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import FIcon from 'react-native-vector-icons/dist/FontAwesome5';
import EIcon from 'react-native-vector-icons/dist/Entypo';
import MIcon from 'react-native-vector-icons/dist/MaterialIcons';

class ProfileItem extends Component {
    handleClick() {
        if(this.props.icon === 'wallet' ) {
            Actions.wallet({openDrawer: this.props.openDrawer, profileBack: true})
        }
        else if (this.props.icon === 'event-note') {
            Actions.reminders({openDrawer: this.props.openDrawer, profileBack: true})
        }
        else if (this.props.icon === 'v-card') {
            Actions.userInfo({openDrawer: this.props.openDrawer, mobile: this.props.mobile, profileBack: true})
        }
        else if (this.props.icon === 'receipt') {
            Actions.userOrders({openDrawer: this.props.openDrawer, modalOpen: false, profileBack: true})
        }
    }
    render() {
        let iconType = null;
        if(this.props.icon === 'wallet' ) {
            iconType = <FIcon name="wallet" size={40} color="#21b34f" />
        }
        else if (this.props.icon === 'event-note') {
            iconType = <MIcon name={this.props.icon} size={40} color="#21b34f" />
        }
        else if (this.props.icon === 'v-card') {
            iconType = <EIcon name={this.props.icon} size={40} color="#21b34f" />
        }
        else if (this.props.icon === 'receipt') {
            iconType = <MIcon name={this.props.icon} size={40} color="#21b34f" />
        }
        return (
            <TouchableOpacity style={[styles.container, {marginRight: this.props.left ? 10: 0, marginLeft: this.props.right ? 10 : 0}]} onPress={() => this.handleClick()}>
                {iconType}
                <Text style={styles.name}>{this.props.label}</Text>
            </TouchableOpacity>
        );
    }
}
export default ProfileItem;