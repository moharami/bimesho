import { StyleSheet } from 'react-native';
// import env from '../../colors/env';
export default StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
        borderWidth: 1,
        borderColor: 'rgb(237, 237, 237)',
        marginBottom: 20,
        borderRadius: 10,
        elevation: 5,
        paddingTop: 60,
        paddingBottom: 20
    },
    name: {
       paddingTop: 30,
        fontSize: 15,
        fontFamily: 'IRANSansMobile(FaNum)',
    }

});
