import React, {Component} from 'react';
import {Dimensions} from 'react-native';
import {Router, Stack, Scene, Drawer, Actions, ActionConst} from 'react-native-router-flux'
import Sidebar from '../sidebar'
import Home from '../screens/home'
import Login from '../screens/Login'
import Confirmation from '../screens/confirmation'
import Profile from '../screens/profile'
import UserInfo from '../screens/userInfo'
import UserOrders from '../screens/userOrders'
import Reminders from '../screens/reminders'
import Wallet from '../screens/wallet'
import InsuranceBuy from '../screens/insuranceBuy'
import VacleInfo from '../screens/vacleInfo'
import Prices from '../screens/prices'
import CustomerInfo from '../screens/customerInfo'
import PictureUpload from '../screens/pictureUpload'
import TimeSelecting from '../screens/timeSelecting'
import FinalConfirm from '../screens/finalConfirm'
import Payment from '../screens/payment'
import LatestNews from '../screens/latestNews'
import ImportantNews from '../screens/importantNews'
import BlogDetail from '../screens/blogDetail'
import About from '../screens/about'
import Rules from '../screens/rules'
import Splash from '../screens/splash'
import {store} from '../config/store';
import {Provider, connect} from 'react-redux';
import Register from "../screens/Register";
import ScalingDrawer from '../components/react-native-scaling-drawer'
import Configure from '../screens/configure'
import OtherVacleInfo from '../screens/otherVacleInfo'
import AddReminder from '../screens/addReminder'
import Marketing from '../screens/marketing'
import Refferer from '../screens/refferer'
import Comment from '../screens/comment'

const onBackAndroid = () => {
    return Actions.pop();
};
let defaultScalingDrawerConfig = {
    scalingFactor: 0.6,
    minimizeFactor: 0.6,
    swipeOffset: 20
};
class MainDrawerNavigator extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    componentWillReceiveProps(nextProps) {
        /** Active Drawer Swipe **/
        if (nextProps.navigation.state.index === 0)
            this._drawer.blockSwipeAbleDrawer(false);

        if (nextProps.navigation.state.index === 0 && this.props.navigation.state.index === 0) {
            this._drawer.blockSwipeAbleDrawer(false);
            this._drawer.close();
        }

        /** Block Drawer Swipe **/
        if (nextProps.navigation.state.index > 0) {
            this._drawer.blockSwipeAbleDrawer(true);
        }
    }

    setDynamicDrawerValue = (type, value) => {
        defaultScalingDrawerConfig[type] = value;
        /** forceUpdate show drawer dynamic scaling example **/
        this.forceUpdate();
    };
    render() {
        const RouterWidthRedux = connect()(Router);

        return (
            <Provider store={store}>
                <ScalingDrawer
                    content={<Sidebar openDrawer={() =>this._drawer.open()} closeDrawer={() =>this._drawer.close()} />}

                    ref={ref => this._drawer = ref}
                    {...defaultScalingDrawerConfig}
                    onClose={() => console.log('close')}
                    onOpen={() => console.log('open')}
                >
                <RouterWidthRedux openDrawer={() =>this._drawer.open()} >
                    <Drawer
                        type="reset"
                        store={store}
                        drawerPosition='right'
                        key="drawer"
                        contentComponent={Sidebar}
                        drawerWidth={Dimensions.get('window').width* .7}
                        hideNavBar>
                        <Scene>
                            {/*<Scene key="root">*/}
                            {/*<Scene key="Splash" component={Splash} initial hideNavBar/>*/}
                            {/*</Scene>*/}
                            {/*<Stack key="container" hideNavBar>*/}
                                <Scene key="Splash" component={Splash} initial title="Splash"  hideNavBar/>
                                <Scene key="login" component={Login} title="Login" hideNavBar/>
                                <Scene key="confirmation" component={Confirmation} title="Confirmation" hideNavBar/>
                                <Scene key="register" component={Register} title="Register" hideNavBar/>
                                <Scene key="home" component={Home} title="Home" hideNavBar />
                                <Scene key="profile"  component={Profile} title="Profile" hideNavBar/>
                                <Scene key="userInfo" component={UserInfo} title="UserInfo" hideNavBar/>
                                <Scene key="userOrders" component={UserOrders} title="UserOrders" hideNavBar/>
                                <Scene key="reminders" component={Reminders} title="Reminders" hideNavBar/>
                                <Scene key="wallet" component={Wallet} title="Wallet" hideNavBar/>
                                <Scene key="insuranceBuy"  component={InsuranceBuy} title="InsuranceBuy" hideNavBar />
                                <Scene key="vacleInfo" component={VacleInfo} title="VacleInfo" hideNavBar/>
                                <Scene key="prices" component={Prices} title="Prices" hideNavBar/>
                                <Scene key="customerInfo" component={CustomerInfo} title="CustomerInfo" hideNavBar/>
                                <Scene key="pictureUpload" component={PictureUpload} title="PictureUpload" hideNavBar/>
                                <Scene key="timeSelecting" component={TimeSelecting} title="TimeSelecting" hideNavBar/>
                                <Scene key="finalConfirm" component={FinalConfirm} title="FinalConfirm" hideNavBar/>
                                <Scene key="payment" component={Payment} title="Payment" hideNavBar/>
                                <Scene key="latestNews" component={LatestNews} title="LatestNews" hideNavBar/>
                                <Scene key="importantNews" component={ImportantNews} title="ImportantNews" hideNavBar/>
                                <Scene key="blogDetail" component={BlogDetail} title="BlogDetail" hideNavBar/>
                                <Scene key="about" path="bime" component={About} title="About" hideNavBar/>
                                <Scene key="rules" component={Rules} title="Rules" hideNavBar/>
                                <Scene key="configure" component={Configure} title="Configure" hideNavBar/>
                                <Scene key="otherVacleInfo" component={OtherVacleInfo} title="OtherVacleInfo" hideNavBar/>
                                <Scene key="addReminder" component={AddReminder} title="AddReminder" hideNavBar/>
                                <Scene key="marketing" component={Marketing} title="Marketing" hideNavBar/>
                                <Scene key="refferer" component={Refferer} title="Refferer" hideNavBar/>
                                <Scene key="comment" component={Comment} title="Comment" hideNavBar/>
                            {/*</Stack>*/}
                        </Scene>
                    </Drawer>
                </RouterWidthRedux>
                </ScalingDrawer>
            </Provider>


        );
    }
}
export default MainDrawerNavigator;



