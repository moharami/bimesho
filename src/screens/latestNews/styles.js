

import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        position: 'relative',
        zIndex: 1,
        backgroundColor: 'rgba(245, 246, 250, 1)'
    },
    header: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        height: 90,
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 9990,
    },
    top: {
        // flex: 1,
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'space-between',
        height: 70,
        // position: 'absolute',
        // top: 0,
        // right: 0,
        // left: 0,
        // zIndex: 9990,
        // borderBottomColor: 'lightgray',
        // borderBottomWidth: 1,
        paddingRight: 15,
        paddingLeft: 15,
        paddingTop: 20
    },
    bodyContainer: {
        paddingBottom: 150,
        // borderWidth: 1,
        // borderColor: 'rgb(237, 237, 237)',
        // paddingRight: 20,
        // paddingLeft: 20,
        paddingTop: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    scroll: {
        paddingTop: 90,
        // paddingRight: 15,
        // paddingLeft: 15,

    },
    footer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        // height: 120,
        position: 'absolute',
        bottom: 0,
        right: 0,
        left: 0,
        zIndex: 9999,
        padding: 15
    },
    iconRightContainer: {
        backgroundColor: 'white',
        borderRadius: 30,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 15,
        paddingRight: 10,
        paddingLeft: 10,
        elevation: 4,
        width: 40,
        height: 40,
    },
    iconLeftContainer: {
        backgroundColor: 'rgba(200, 200, 200, 1)',
        // width: '40%',
        // height: 32,
        borderRadius: 30,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        padding: 5,

    },
    label: {
        paddingLeft: 15,
        paddingRight: 10,
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 15,
    },
    catContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'space-between',
        transform: [
            {rotateY: '180deg'},
        ],
        paddingTop: 30
    },
    navContainer: {
        alignItems: "center",
        justifyContent: 'center',
        width: 84,
        paddingBottom: 40,

    },
    text: {
        fontFamily: 'IRANSansMobile(FaNum)',
        // paddingBottom: 55,
        position: 'absolute',
        bottom: 15

    },
    slideContainers: {
        flexDirection: 'row',
        transform: [
            {rotateY: '180deg'},
        ],
    },
    topLabel: {
        paddingRight: 10,
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 18,
    },
    seeMoreText: {
        paddingLeft: 10,
        color: '#21b34f',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 12,
    },
    labelContainer: {
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'flex-end',
        width: '100%',
        paddingBottom: 20,
        paddingTop: 20
    },
    body: {
        // flexDirection: 'row',
        flexDirection: 'row-reverse',
        flexWrap: 'wrap',
        // paddingLeft: 15,
        // padding: 10,
        marginBottom: 20,
        // direction: 'rtl'
    },
    subImage: {
        width: '95%',
        height: 240,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 20
    },
    imageText: {
        backgroundColor: "transparent",
        fontSize: 14,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: "white"

    },
});
