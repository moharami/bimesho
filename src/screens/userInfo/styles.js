import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        // alignItems: 'center',
        // justifyContent: 'flex-end',
        position: 'relative',

    },
    image: {
        width: '100%',
        height: '36%'
        // resizeMode: 'contain'
    },
    imageRow: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'space-between',
        paddingTop: 20,
        padding: 15
    },
    profileImage: {
        width: 85,
        height: 85,
        borderRadius: 85,
    },
    profile: {
        width: 90,
        // marginRight: '30%'
    },
    name: {
        fontSize: 15,
        color: 'rgb(255, 255, 255)',
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'center',
        paddingTop: 10
    },
    tel: {
        fontSize: 14,
        color: 'rgb(150, 150, 150)',
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'center',
        paddingTop: 10
    },
    TrapezoidStyle: {
        flex: 1,
        position: 'absolute',
        bottom: '75%',
        right: 0,
        left: 0,
        zIndex: 100,
        borderTopWidth: 50,
        borderLeftWidth: 0,
        borderBottomWidth: 0,
        borderTopColor: 'transparent',
        borderRightColor: 'rgb(233, 233, 233)',
        borderLeftColor: 'transparent',
        borderBottomColor: 'transparent',
    },
    scroll: {
        // position: 'absolute',
        // flex: 1,
        // top: 150,
        // right: 0,
        // left: 0,
        // zIndex: 9998,
        marginBottom: 80
    },
    body: {
        // alignItems: 'center',
        // justifyContent: 'center',
        padding: 20,
    },
    label: {
        fontSize: 14, color: 'rgb(107, 107, 107)',
        paddingBottom: 15,
        // paddingTop: 20
    },
});
