
import React, {Component} from 'react';
import { View, TouchableOpacity, Text, TextInput,ImageBackground, Dimensions, ScrollView, BackHandler, Alert, AsyncStorage} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux';
import background from '../../assets/bg.png'
import Axios from 'axios'
export const url = 'https://bimehsho.com/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import Icon from 'react-native-vector-icons/dist/Feather';
import FooterMenu from "../../components/footerMenu/index";
import WalletAcount from "../../components/walletAcount";
import LinearGradient from "react-native-linear-gradient";

class Wallet extends Component {
    constructor(props){
        super(props);
        this.state = {
            text: '',
            loading: true,
            peoples: [],
            modalVisible: false,
            wallet: null,
            requestStatus: ''
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress(){
        if(this.props.profileBack) {
             Actions.pop({refresh: {refresh:Math.random()}});
        }
        else {
            Actions.insuranceBuy({openDrawer: this.props.openDrawer})
        }
        return true;
    };
    componentWillMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
        AsyncStorage.getItem('token').then((info) => {
            const newInfo = JSON.parse(info);
            console.log('uuuuuuuu', newInfo.user_id)
            // this.setState({loading: true});
            Axios.post('/request/my-wallet', {
                user_id: newInfo.user_id
            }).then(response => {
                console.log('eewwalet', response.data.data)
                this.setState({wallet: Math.floor(response.data.data)}, ()=> { this.numberWithCommas(response.data.data)});

                Axios.post('/request/market/get-status', {
                    user_id: newInfo.user_id
                }).then(response => {
                    this.setState({loading: false, statuses: response.data.data});
                    console.log('statuses',response.data.data)
                    if(response.data.data === "") {
                        this.setState({noRequest: true});
                    }
                    else if(response.data.data !== "" && response.data.data.status === "request") {
                        this.setState({requestStatus: 'request'});
                    }
                    else if(response.data.data !== "" && response.data.data.status === "approve") {
                        this.setState({requestStatus: 'approve'});
                    }
                    else if(response.data.data !== "" && response.data.data.status === "reject") {
                        this.setState({requestStatus: 'reject'});
                    }
                })
                    .catch((error) =>{
                        console.log(error)
                        // let errorObject=JSON.parse(JSON.stringify(error.response));
                        // console.log(errorObject);
                        // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                        // this.setState({loading: false});
                        this.setState({modalVisible: true, loading: false});

                    });
            })
                .catch((error) =>{
                    // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                    // this.setState({loading: false});
                    this.setState({modalVisible: true, loading: false});
                });
        });

    }
    numberWithCommas(x) {
        console.log('x', x);
        const newValue =  x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        console.log('new val', newValue);

        this.setState({wallet: newValue});
    }
    render() {
        if(this.state.loading){
            return (<Loader />)
        }
        else return (
            <View style={styles.container}>
                <LinearGradient
                    start={{x: 0, y: 1}} end={{x: 1, y: 1}} colors={['#21b34f', '#95cc3a']}>
                    <View style={styles.image}>
                        <View style={styles.imageRow}>
                            <TouchableOpacity onPress={() => this.onBackPress()}>
                                <Icon name="arrow-left" size={20} color="white" style={{paddingRight: 20}} />
                            </TouchableOpacity>
                            <Text style={styles.label}>کیف پول من</Text>
                            <TouchableOpacity onPress={() => Actions.drawerOpen()}>
                                <Icon name="menu" size={26} color="white" />
                            </TouchableOpacity>
                        </View>
                    </View>
                </LinearGradient>
                <ScrollView style={styles.scroll}>
                    <View style={styles.body}>
                        <WalletAcount wallet  walletAmount={this.state.wallet} requestStatus={this.state.requestStatus} />
                        {/*<WalletPeople peoples={this.state.peoples} />*/}
                    </View>
                </ScrollView>
                <FooterMenu active="profile" openDrawer={this.props.openDrawer}/>
                {/*<LinearGradient*/}
                    {/*start={{x: 0, y: 1}} end={{x: 1, y: 1}} colors={['#21b34f', '#95cc3a']}>*/}
                    {/*<View style={[styles.TrapezoidStyle, {borderRightWidth: Dimensions.get('window').width}]} />*/}
                {/*</LinearGradient>*/}
            </View>

        );
    }
}
export default Wallet;