import React, {Component} from 'react';
import { View, TouchableOpacity, Text, Linking, ActivityIndicator ,Alert, Image, BackHandler, NetInfo, Dimensions, AsyncStorage, ScrollView} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux';
import background from '../../assets/bg.png'
import Axios from 'axios';
import homeBG from '../../assets/homeBG6.png';
export const url = 'https://bimehsho.com/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import ProfileItem from "../../components/profileItem/index";
import Icon from 'react-native-vector-icons/dist/Feather';
import FooterMenu from "../../components/footerMenu/index";
import AlertView from "../../components/modalMassage";
import bime1 from "../../assets/newInsurance/body.png";
import bime2 from "../../assets/newInsurance/third.png";
import bime3 from "../../assets/newInsurance/travel.png";
import bime4 from "../../assets/newInsurance/medical.png";
import bime5 from "../../assets/newInsurance/fire.png";
import bime6 from "../../assets/newInsurance/life.png";
import bime7 from "../../assets/newInsurance/indi.png";
import bime10 from "../../assets/newInsurance/earth.png";
import motor from "../../assets/motor.png";
import moment_jalaali from "moment-jalaali";
import Logo from '../../assets/logo.png'
import SlideItems from '../../components/newSlide'
import SoonModal from './soonModal'
import {connect} from 'react-redux';
import {store} from '../../config/store';
class InsuranceBuy extends Component {
    constructor(props){
        super(props);
        this.state = {
            text: '',
            login: true,
            loading: false,
            modalVisible: false,
            modalVisible2: false,
            wifi: false,
            isConnected: false,
            logout: true,
            spacialPosts: []
        };
    }
    componentWillUpdate(){
        BackHandler.addEventListener("hardwareBackPress",  this.onBackPress.bind(this));
    }

    closeModal() {
        this.setState({modalVisible: false});
    }
    closeModal2() {
        this.setState({modalVisible2: false});
    }
    componentWillMount() {
        AsyncStorage.getItem('insuranceInfo').then((info) => {
            if(info !== null) {
                const newInfo = JSON.parse(info);
                Linking.getInitialURL().then(url => {
                    console.log('url', url)
                    if(url !== null) {
                        Actions.comment({openDrawer: this.props.openDrawer, modalOpen: true, insTitle: newInfo.title, insId: newInfo.id});
                        console.log('userId', newInfo.user_id)
                        console.log('nnnnnnewinfo', newInfo)
                    }
                });
            }
            else {
                Linking.getInitialURL().then(url => {
                    console.log('url', url)
                    if(url !== null) {
                        Actions.insuranceBuy({openDrawer: this.props.openDrawer, modalOpen: false});
                    }
                });
            }
        });
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
        NetInfo.getConnectionInfo().then((connectionInfo) => {
            this.setState({isConnected: connectionInfo.type !== 'none'}, ()=> {
                if(this.state.isConnected === false){
                    this.setState({modalVisible: true, loading: false, wifi: true})
                }
            })
        });

        this.setState({loading: true})
        Axios.post('/request/blog/list').then(response => {
            this.setState({spacialPosts: response.data.data.data});
            console.log('spacial posts', response.data.data.data)
            AsyncStorage.getItem('token').then((info) => {
                if(info !== null) {
                    const newInfo = JSON.parse(info);
                    console.log('iiiiiiiiinfo', newInfo)
                    Axios.post('/user', {
                        mobile: newInfo.mobile

                    }).then(response=> {
                        console.log('profile', newInfo.mobile)
                        store.dispatch({type: 'USER_INFO_FETCHED', payload: response.data.data});
                        this.setState({loading: false})


                    })
                        .catch((error) => {
                            // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                            // this.setState({loading: false});
                            this.setState({modalVisible: true, loading: false});
                        });
                }
                else{
                    this.setState({loading: false})
                }
            });
        })
            .catch((error) => {
                console.log(error);
                this.setState({modalVisible: true, loading: false});
            });

    }
    logout = () => {
        Alert.alert(
            'خروج از اپلیکیشن',
            'آیا از  خروج خود مطمئن هستید؟',
            [
                {text: 'خیر', onPress: () => this.setState({logout: false}), style: 'cancel'},
                {text: 'بله', onPress: () =>  this.setState({logout: true}, () => { BackHandler.exitApp();})},
            ]
        )
    }
    onBackPress() {
        console.log('some thing loged')
        this.logout()
        return true;
    };

    render() {
        const {user} = this.props;
        return (
            <View style={styles.container}>
                <ScrollView style={styles.scroll}>
                    <View style={styles.topContainer}>
                        <View style={styles.imageRow}>
                            <View style={styles.profile}>
                                {/*<View style={styles.iconContainer}>*/}
                                    {/*<Icon name="user" size={20} />*/}
                                {/*</View>*/}
                                {/*<Text style={styles.name}>{user !== null ? user.fname : ''} {user !== null ? user.lname : ''}</Text>*/}
                            </View>
                            <TouchableOpacity onPress={() => Actions.drawerOpen()}>
                                <Icon name="menu" size={26} color="white" style={{paddingRight: 15}} />
                            </TouchableOpacity>
                        </View>
                        {/*<Image style={{position: 'absolute', top: 20, left: '39%'}}*/}
                               {/*source={require('../../assets/hmLogo.png')}/>*/}
                        <View style={styles.body}>
                            <View style={styles.row}>
                                <View style={styles.item}>
                                    <TouchableOpacity onPress={() => Actions.otherVacleInfo({openDrawer: this.props.openDrawer, fire: true, pageTitle: 'بیمه آتش سوزی'})} style={styles.imageContainer}>
                                        <Image source={bime5} style={styles.bodyImage}  />
                                        <Text style={styles.label}>آتش سوزی</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.item}>
                                    <TouchableOpacity style={styles.imageContainer} onPress={() => Actions.vacleInfo({openDrawer: this.props.openDrawer, bodyBime: true, pageTitle: 'بیمه بدنه خودرو'})}>
                                        <Image source={bime1} style={styles.bodyImage} />
                                        <Text style={styles.label}>بدنه</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.item}>
                                    <TouchableOpacity style={styles.imageContainer} onPress={() => Actions.vacleInfo({openDrawer: this.props.openDrawer, thirdBime: true,motor: false, pageTitle: 'بیمه شخص ثالث'})}>
                                        <Image source={bime2} style={styles.bodyImage} />
                                        <Text style={styles.label}>شخص ثالث</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.item}>
                                    <TouchableOpacity onPress={() => Actions.otherVacleInfo({openDrawer: this.props.openDrawer, travel: true, pageTitle: 'بیمه مسافرتی'})}  style={styles.imageContainer}>
                                        <Image source={bime3} style={styles.bodyImage} />
                                        <Text style={[styles.label, {paddingTop: 15, paddingBottom: 10}]}>مسافرتی</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={styles.row}>
                                <View style={styles.item}>
                                    <TouchableOpacity onPress={()=> this.setState({modalVisible2: true})}  style={styles.imageContainer}>
                                    {/*<TouchableOpacity onPress={() => Actions.otherVacleInfo({openDrawer: this.props.openDrawer, individual: true, pageTitle: 'بیمه درمان انفرادی'})} style={styles.imageContainer}>*/}
                                        <Image source={bime7} style={styles.bodyImage} />
                                        <Text style={styles.label}>درمانی</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.item}>
                                    <TouchableOpacity onPress={()=> this.setState({modalVisible2: true})}  style={styles.imageContainer}>
                                    {/*<TouchableOpacity onPress={() => Actions.otherVacleInfo({openDrawer: this.props.openDrawer, medical: true, pageTitle: 'بیمه مسئولیت پزشکی'})} style={styles.imageContainer}>*/}
                                        <Image source={bime4} style={styles.bodyImage} />
                                        <Text style={[styles.label, {paddingTop: 15}]}>پزشکی</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.item}>
                                    <TouchableOpacity onPress={()=> this.setState({modalVisible2: true})}  style={styles.imageContainer} >
                                    {/*<TouchableOpacity onPress={() => Actions.otherVacleInfo({openDrawer: this.props.openDrawer, age: true, pageTitle: 'بیمه عمر'})} style={styles.imageContainer}>*/}
                                        <Image source={bime6} style={styles.bodyImage} />
                                        <Text style={styles.label}> عمر</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.item}>
                                    <TouchableOpacity onPress={() => Actions.otherVacleInfo({openDrawer: this.props.openDrawer, earth: true,  pageTitle: 'بیمه زلزله'})} style={styles.imageContainer}>
                                        <Image source={bime10} style={styles.bodyImage} />
                                        <Text style={styles.label}>زلزله</Text>
                                    </TouchableOpacity>
                                </View>
                                {/*<View style={styles.item}>*/}
                                    {/*<TouchableOpacity style={[styles.imageContainer, { paddingTop: 30, paddingBottom: 20}]} onPress={() => Actions.vacleInfo({openDrawer: this.props.openDrawer, thirdBime: true, motor: true, pageTitle: 'بیمه موتور سیکلت'})}>*/}
                                        {/*<Image source={motor} style={[styles.bodyImage, {tintColor: 'white'}]} />*/}
                                        {/*<Text style={styles.label}>موتور سیکلت</Text>*/}
                                    {/*</TouchableOpacity>*/}
                                {/*</View>*/}
                            </View>
                        </View>
                        <AlertView
                            closeModal={(title) => this.closeModal(title)}
                            modalVisible={this.state.modalVisible}
                            title={(this.state.wifi ? 'لطفا وضعیت وصل بودن به اینترنت را بررسی کنید': 'مشکلی در برقراری ارتباط با سرور به وجود آمده لطفا دوباره سعی کنید')}
                        />
                        <SoonModal
                            closeModal={(title) => this.closeModal2(title)}
                            onChange={(visible) => this.setState({modalVisible2: false})}
                            modalVisible={this.state.modalVisible2}
                        />
                    </View>
                    {
                        this.state.loading ?
                            <View style={{paddingTop: 30}}>
                                <ActivityIndicator
                                    size={30}
                                    animating={true}
                                    color='green'
                                />
                            </View>
                            :
                            <View style={[styles.body, {paddingBottom: 50}]}>
                                <Image source={Logo} style={{width: '50%', resizeMode: 'contain'}} />
                                <Text style={styles.bodyLabel}>آخرین مطالب و مقالات</Text>
                                <View style={{width: '100%'}}>
                                    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={{ transform: [{ scaleX: -1}]}}>
                                        <View style={styles.slideContainers}>
                                            {
                                                this.state.spacialPosts ?
                                                    this.state.spacialPosts.map((item) => {return <SlideItems item={item} key={item.id}  />}): null
                                            }
                                        </View>
                                    </ScrollView>
                                </View>
                            </View>
                    }
                </ScrollView>
                <FooterMenu active="bime" openDrawer={this.props.openDrawer}/>
                {/*<View style={[styles.TrapezoidStyle, {borderRightWidth: Dimensions.get('window').width}]} />*/}
            </View>

        );
    }
}
// export default InsuranceBuy;
function mapStateToProps(state) {
    return {
        user: state.auth.user,

    }
}
export default connect(mapStateToProps)(InsuranceBuy);