import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        position: 'relative'
    },
    topContainer: {
        width: '100%',
        // resizeMode: 'contain',
        // height: '100%',
        // height: '35%',
        // resizeMode: 'contain',
        backgroundColor: '#78727a',
        paddingBottom: 20,

    },
    // TrapezoidStyle: {
    //     flex: 1,
    //     position: 'absolute',
    //     bottom: '75%',
    //     right: 0,
    //     left: 0,
    //     zIndex: 100,
    //
    //     borderTopWidth: 50,
    //     borderLeftWidth: 0,
    //     borderBottomWidth: 0,
    //     borderTopColor: 'rgb(61, 99, 223)',
    //     borderRightColor: 'rgb(233, 233, 233)',
    //     borderLeftColor: 'transparent',
    //     borderBottomColor: 'transparent',
    // },
    imageRow: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'space-between',
        paddingTop: 5,
    },
    profileImage: {
        width: 85,
        height: 85,
        borderRadius: 85,
    },
    profile: {
        // width: 90,
        // marginRight: '30%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: 30,
        paddingBottom: 7
    },
    slideContainers: {
        flexDirection: 'row',
        transform: [
            {rotateY: '180deg'},
        ],
    },








    imageContainer: {
        // backgroundColor: 'rgb(233, 233, 233)',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 20,
        borderRadius: 15,
        backgroundColor: '#33c575'

        // elevation: 8,
        // borderWidth: 2,
        // borderTopColor: '#38d0e6',
        // borderRightColor: '#38d0e6',
        // borderLeftColor: '#24d1ba',
        // borderBottomColor: '#24d1ba',
    },
    name: {
        fontSize: 13,
        color: 'rgb(255, 255, 255)',
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'center',
        paddingTop: 10,
        paddingLeft: 20,
        paddingBottom: 5
    },

    reminderItem: {
        width: '85%',
        backgroundColor: 'rgb(233, 233, 233)',
        borderRadius: 15,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20,
        paddingBottom: 7

    },
    bodyImage: {
        width: 35,
        resizeMode: 'contain',
        tintColor: 'white'
    },
    bodyyImage: {
        width: 35,
        resizeMode: 'contain',
        tintColor: 'white'
    },
    scroll: {
        // position: 'absolute',
        flex: 1,
        // top: '33%',
        // right: 0,
        // left: 0,
        // bottom: 0,
        // zIndex: 9997,
        marginBottom: 90,
        backgroundColor: 'rgb(245, 245, 245)'
    },
    body : {
        // position: 'absolute',
        // flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-start',
        // top: 120,
        // right: 0,
        // left: 0,
        // zIndex: 9998,
        paddingRight: 10,
        paddingLeft: 10,
        // paddingBottom: '50%',
        // paddingTop: 2,
        // backgroundColor: 'rgb(245, 245, 245)'
// backgroundColor: 'rgba(11, 11, 11, .2)',


        // position: 'absolute',
        // flex: 1,
        // top: '18%',
        // right: 0,
        // left: 0,
        // // bottom: 0,
        // zIndex: 9999,
        // marginBottom: 60

    },
    row: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        height: 130
        // paddingBottom: 10
    },
    item: {
        width: '24%',
        height: 100,
        // height: '55%',
        // backgroundColor: 'red'
        // borderRadius: 15,


    },
    lastrow: {
        width: '100%',
        height: 130,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        // paddingBottom: 20,
        // paddingRight: '10%',
        // paddingLeft: '10%',
    },
    iconContainer: {
        width: 30,
        height: 30,
        borderRadius: 30,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    },
    label: {
        fontSize: 11,
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'center',
        paddingTop: 7,
        color: 'white'
    },
    bodyLabel: {
        fontSize: 13,
        fontFamily: 'IRANSansMobile(FaNum)',
        alignSelf: 'flex-end',
        paddingTop: 20,
        paddingBottom: 20,
        color: 'black'
    },
});
