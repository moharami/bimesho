import React, {Component} from 'react';
import { View, TouchableOpacity, Text, TextInput,ImageBackground, Image, BackHandler, Dimensions, ScrollView, AsyncStorage} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux';
import background from '../../assets/bg.png'
import Axios from 'axios';
export const url = 'https://bimehsho.com/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import ProfileItem from "../../components/profileItem/index";
import Icon from 'react-native-vector-icons/dist/Feather';
import FooterMenu from "../../components/footerMenu/index";
import {store} from '../../config/store';
import {connect} from 'react-redux';
import AlertView from '../../components/modalMassage'
import LinearGradient from 'react-native-linear-gradient'

class Profile extends Component {
    constructor(props){
        super(props);
        this.state = {
            text: '',
            loading: false,
            signed: false,
            modalVisible: false
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress(){
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    closeModal() {
        this.setState({modalVisible: false});
    }
    componentWillMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    render() {
        const {user} = this.props;
        if(this.state.loading){
            return (<Loader />)
        }
        else return (
        <View style={styles.container}>
            <LinearGradient
                start={{x: 0, y: 1}} end={{x: 1, y: 1}} colors={['#21b34f', '#95cc3a']}>
                <View style={styles.image}>
                    <View style={styles.imageRow}>
                        <View style={styles.profile}>
                            {/*{*/}
                            {/*user.attachments? <Image source={{uri: 'http://https://bimehsho.com/api/v1/files?uid='+this.props.user.attachments.uid+'&width=350&height=350' }} style={styles.profileImage} />: null*/}
                            {/*}*/}
                            {/*<Text style={styles.name}>{user.fname} {user.lname}</Text>*/}
                            <Text style={styles.name}>{user !== null ? user.fname : ''} {user !== null ? user.lname : ''}</Text>
                        </View>
                        <TouchableOpacity onPress={() => Actions.drawerOpen()}>
                            <Icon name="menu" size={26} color="white" style={{paddingRight: 15}} />
                        </TouchableOpacity>
                    </View>
                </View>
            </LinearGradient>
            <ScrollView style={styles.scroll}>
                <View style={styles.body}>
                    <View style={styles.row}>
                        <ProfileItem icon="receipt" label="سفارش های من" left openDrawer={this.props.openDrawer}/>
                        <ProfileItem icon="v-card" label="اطلاعات فردی" right openDrawer={this.props.openDrawer} mobile={this.props.mobile}/>
                    </View>
                    <View style={styles.row}>
                        <ProfileItem icon="wallet" label="کیف پول" left openDrawer={this.props.openDrawer}/>
                        <ProfileItem icon="event-note" label="یادآوری" right openDrawer={this.props.openDrawer}/>
                    </View>
                    <AlertView
                        closeModal={(title) => this.closeModal(title)}
                        modalVisible={this.state.modalVisible}

                        title={this.state.signed ?  'لطفا برای مشاهده این بخش ابتدا ثبت نام کنید': 'مشکلی در برقراری ارتباط با سرور به وجود آمده لطفا دوباره سعی کنید'}
                    />
                </View>
            </ScrollView>
            <FooterMenu active="profile" openDrawer={this.props.openDrawer} />
            {/*<LinearGradient*/}
                {/*start={{x: 0, y: 1}} end={{x: 1, y: 1}} colors={['#21b34f', '#95cc3a']}>*/}
                {/*<View style={[styles.TrapezoidStyle, {borderRightWidth: Dimensions.get('window').width}]} />*/}
            {/*</LinearGradient>*/}
        </View>
        );
    }
}
function mapStateToProps(state) {
    return {
        user: state.auth.user,

    }
}
export default connect(mapStateToProps)(Profile);
