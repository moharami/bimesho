
import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, NetInfo, BackHandler, Keyboard, Image, TextInput, TouchableHighlight} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import IIcon from 'react-native-vector-icons/dist/Ionicons';
import FIcon from 'react-native-vector-icons/dist/Feather';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import {store} from '../../config/store';
import {connect} from 'react-redux';
import Axios from 'axios';
export const url = 'https://bimehsho.com/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import HomeHeader from "../../components/homeHeader/index";
import vacle1 from "../../assets/vacle/vacle1.png";
import vacle2 from "../../assets/vacle/vacle2.png";
import vacle3 from "../../assets/vacle/vacle3.png";
import SwitchRow from '../../components/switchRow'
import moment_jalaali from 'moment-jalaali'
// import PersianCalendarPicker from 'react-native-persian-calendar-picker';
import AlertView from '../../components/modalMassage'
import ModalFilterPicker from 'react-native-modal-filter-picker'
import Selectbox from 'react-native-selectbox'
import AlertModal from '../../components/alertModal'

class VacleInfo extends Component {
    constructor(props){
        super(props);
        this.state={
            loading:false,
            loading2:false,
            loading3:false,
            status:1,
            activeButton:9,
            activeVacle:1,
            activeVacleName:'سواری',
            showPicker:false,
            showPicker2:false,
            selectedStartDate:null,
            selectedStartDate2:null,
            modalVisible: false,
            type:"",
            name:0,
            nameTitle:null,
            year:{key: 0, label: "1398 - 2019", value: 0},
            yearTitle:null,
            damageNumber:{key: 0, label: "بدون خسارت", value: 0},
            damageNumberFin:{key: 0, label: "بدون خسارت", value: 0},
            damageNumberDriver:{key: 0, label: "بدون خسارت", value: 0},
            damageNumberTitle:null,
            damageNumberFinTitle:null,
            damageNumberDriverTitle:null,
            damageYear:{key: 0, label: "بدون تخفیف", value: 0},
            driverOffs:{key: 0, label: "بدون تخفیف", value: 0},
            insOffs:{key: 0, label: "بدون تخفیف", value: 0},
            damageYearTitle:'',
            driverOffsTitle:'',
            insOffsTitle:'',
            timeOf:0,
            dateMassage: false,
            nextStep: false,
            usage:{key: 1, label: "شخصی", value: 1},
            usageTitle:null,
            offs:0,
            bodyThirdBime:{key: 0, label: "بدون تخفیف", value: 0},
            bodyThirdBimeTitle:null,
            bodyBime:{key: 0, label: "بدون تخفیف", value: 0},
            bodyBimeTitle:null,
            vaclePrice:"",
            carCats:[],
            kindCats:[],
            modelCats:[],
            vacleType:0,
            vacleTypeTitle:null,
            Chemical:false,
            break_glass:false,
            natural_disaster:false,
            Steal:false,
            Price:false,
            Transit:false,
            transfer:false,
            feature:[],
            vacleId: null,
            filter: 'point-down',
            insurances: [],
            redBorder: false,
            toolTipVisible: true,
            visibleName: false,
            visibleModel: false,
            priceAlert: false,
            fillName: false,
            zero: false,
            good: false,
            is_cash: true
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
        NetInfo.getConnectionInfo().then((connectionInfo) => {
            console.log('connectionInfo.type', connectionInfo.type !== 'none')
            this.setState({isConnected: connectionInfo.type !== 'none' && connectionInfo.type !== 'unknown'}, ()=> {
                if(this.state.isConnected === false){
                    this.setState({modalVisible: true, loading: false, wifi: true})
                }
                else  if(this.state.isConnected === true){
                    this.getCat(1);
                    // Axios.get('/request/get_insurance').then(response => {
                    //     this.setState({loading: false, insurances: response.data.data});
                    // })
                    //     .catch((error) =>{
                    //         this.setState({modalVisible: true, loading: false});
                    //
                    //     });
                }
            })
        });
    }
    test(itemValue) {
        if(this.props.thirdBime) {
            // if(this.props.motor === false) {
            //     if( this.state.activeVacle !== 0 && this.state.name !== 0  && this.state.vacleType !== 0 && this.state.usage.value !== 0  && this.state.selectedStartDate !== null && this.state.driverOffs !== 0  && this.state.damageNumberDriver !== 0 ) {
            //         this.setState({ nextStep: true });
            //     }
            // }
            // else if(this.props.motor === true) {
            //     if( this.state.name !== 0  && this.state.vacleType !== 0 && this.state.selectedStartDate !== null && this.state.driverOffs !== 0  && this.state.damageNumberDriver !== 0) {
            //         this.setState({ nextStep: true });
            //     }
            // }
            if(this.props.motor === false) {
                if( this.state.activeVacle !== 0 && this.state.name !== 0  && this.state.vacleType !== 0 && this.state.usage.value !== 0  && this.state.selectedStartDate !== null && this.state.selectedStartDate2 !== null && this.state.driverOffs !== 0  && this.state.damageNumberDriver !== 0 && this.state.driverOffs !== 0 && this.state.damageNumber !== 0 && this.state.damageNumberFin !== 0 && this.state.insOffs !== 0 ) {
                    this.setState({ nextStep: true });
                }
            }
            else if(this.props.motor === true) {
                if( this.state.name !== 0  && this.state.vacleType !== 0 && this.state.selectedStartDate !== null && this.state.selectedStartDate2 !== null && this.state.driverOffs !== 0  && this.state.damageNumberDriver !== 0 && this.state.driverOffs !== 0&& this.state.damageNumber !== 0 && this.state.damageNumberFin !== 0 && this.state.insOffs !== 0 ) {
                    this.setState({ nextStep: true });
                }
            }
            else {
                this.setState({ nextStep: false})
            }
        }
        if(this.props.bodyBime) {
            if( this.state.activeVacle !== 0 && this.state.name !== 0  && this.state.vacleType !== 0 && this.state.vaclePrice !== "") {
                this.setState({ nextStep: true });
            }
            else {
                this.setState({ nextStep: false})
            }
        }
    }
    onDateChange(date, input) {
        console.log('input', input)
        if(input === 1){
            setTimeout(() => {this.setState({showPicker: false, showPicker2: false})}, 200)
            this.setState({ selectedStartDate: date }, ()=> { this.test();});
        }
        else if(input === 2) {
            setTimeout(() => {this.setState({showPicker:  false, showPicker2: false})}, 200)
            this.setState({ selectedStartDate2: date }, ()=> { this.test();});
        }
    }
    // onDateChange2(date) {
    //     setTimeout(() => {this.setState({showPicker2: false})}, 200)
    //     this.setState({ selectedStartDate2: date }, ()=> { this.test();});
    // }
    nextStep() {
        if(this.state.nextStep) {
            // AsyncStorage.getItem('token').then((info) => {
            //     const newInfo = JSON.parse(info);
            //     const token = newInfo.token;
            //     const id = newInfo.user_id;
            let user_details = {
                _token:null,
                user_id: null,
                fname:null,
                lname:null,
                national_id:null,
                birthday:null,
                state:null,
                city:null,
                tel:null,
                post_code:null,
                address:null,
                reciver:null,
                new_state:null,
                new_city:null,
                new_tel:null,
                new_post_code:null,
                new_address:null,
                new_reciver:null,
                mobile:null,
                new_mobile:null,
                date_0:null,
                date_1:null,
                time_delivery:null,
                date_2:null,
                address_selected:null,
                lat:null,
                lng:null,
            };
            if(this.props.bodyBime) {
                let factor = {
                    user_id:null,
                    body_car_cate:this.state.activeVacle,
                    body_car_name:this.state.name,
                    body_car_model:this.state.vacleType,
                    body_car_years:this.state.year.value,
                    body_car_price:this.state.vaclePrice,
                    insurance_id:null,
                    chemical:this.state.Chemical,
                    break_glass:this.state.break_glass,
                    natural_disaster:this.state.natural_disaster,
                    steal:this.state.Steal,
                    price:this.state.Price,
                    Transit:this.state.Transit,
                    transfer:this.state.transfer,
                    third_dmg_years:this.state.bodyThirdBime.value,
                    body_dmg_years:this.state.bodyBime.value,
                    sort:this.state.filter,
                    before:null,
                    one:null,
                    two:null,
                    dataSelect:{
                        body_car_cate:this.state.activeVacleName,
                        body_car_name:this.state.nameTitle,
                        body_car_model:this.state.vacleTypeTitle,
                        body_car_years:this.state.yearTitle,
                        third_dmg_years:this.state.bodyThirdBimeTitle,
                        body_dmg_years:this.state.bodyBimeTitle,
                        feature: this.state.feature,
                        fianl_price:null
                    }
                };
                if(this.state.vaclePrice < 1000000 || this.state.vaclePrice > 9000000000){
                    this.setState({modalVisible: true, priceAlert: true, loading: false});
                }
                else {
                    this.setState({loading:true});
                    Axios.post('/request/req_body', {
                        body_car_cate:this.state.activeVacle,
                        body_car_name:this.state.name,
                        body_car_model:this.state.vacleType,
                        body_car_years:this.state.year.value,
                        third_dmg_years:this.state.bodyThirdBime.value ,
                        body_dmg_years:this.state.bodyBime.value,
                        body_car_price:this.state.vaclePrice,
                        insurance_id:null,
                        Chemical:this.state.Chemical,
                        break_glass:this.state.break_glass,
                        natural_disaster:this.state.natural_disaster,
                        Steal:this.state.Steal,
                        Price:this.state.Price,
                        Transit:this.state.Transit,
                        transfer:this.state.transfer,
                        zero:this.state.zero ? 1 : 0,
                        good:this.state.good ? 1 : 0,
                        is_cash:this.state.is_cash ? 1: 0,
                        sort: this.state.filter
                    }).then(response=> {
                        this.setState({loading: false});
                        // console.log(response.data.data)
                        // if(this.state.feature !== null) {
                        //     let str = "";
                        //     this.state.feature.map((item)=>  str = str + item +", ")
                        //     factor.dataSelect.feature = str;
                        // }
                        console.log('body respponse', response.data)
                        Actions.prices({openDrawer:this.props.openDrawer, bime: Object.values(response.data), pageTitle: 'قیمت ها', factor:factor, user_details:user_details, insurType: 'body' , Chemical:this.state.Chemical,
                            break_glass:this.state.break_glass,
                            natural_disaster:this.state.natural_disaster,
                            Steal:this.state.Steal,
                            Price:this.state.Price,
                            Transit:this.state.Transit,
                            transfer:this.state.transfer})
                    })
                        .catch((error) => {
                            console.log(error)
                            console.log(error.response)
                            //  Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                            // this.setState({loading: false});
                            this.setState({modalVisible: true, loading: false});

                        });
                }
            }
            else if(this.props.thirdBime || this.props.motor) {
                console.log('selectedStartDate2', this.state.selectedStartDate2)
                console.log('selectedStartDate', this.state.selectedStartDate)

                if(this.state.selectedStartDate2 < this.state.selectedStartDate || moment_jalaali(this.state.selectedStartDate2).format('jYYYY/jM/jD') === moment_jalaali(this.state.selectedStartDate).format('jYYYY/jM/jD')) {
                    this.setState({modalVisible: true, dateMassage: true, loading: false});
                }
                else {
                    this.setState({dateMassage: false})
                    console.log('moment_jalaali(this.state.selectedStartDate)', moment_jalaali(this.state.selectedStartDate).format('jYYYY/jM/jD').replace(/\//gi, '-'))
                    console.log('moment_jalaali(this.state.selectedStartDate2)', moment_jalaali(this.state.selectedStartDate2).format('jYYYY/jM/jD').replace(/\//gi, '-'))
                    let stttr = moment_jalaali(this.state.selectedStartDate).format('jYYYY/jM/jD').replace(/\//gi, '-')
                    stttr = stttr.replace(/۰/g, "0");
                    stttr = stttr.replace(/۱/g, "1");
                    stttr = stttr.replace(/۲/g, "2");
                    stttr = stttr.replace(/۳/g, "3");
                    stttr = stttr.replace(/۴/g, "4");
                    stttr = stttr.replace(/۵/g, "5");
                    stttr = stttr.replace(/۶/g, "6");
                    stttr = stttr.replace(/۷/g, "7");
                    stttr = stttr.replace(/۸/g, "8");
                    stttr = stttr.replace(/۹/g, "9");
                    console.log('neW strrr  ', stttr );

                    let stttr2 =  moment_jalaali(this.state.selectedStartDate2).format('jYYYY/jM/jD').replace(/\//gi, '-')
                    stttr2 = stttr2.replace(/۰/g, "0");
                    stttr2 = stttr2.replace(/۱/g, "1");
                    stttr2 = stttr2.replace(/۲/g, "2");
                    stttr2 = stttr2.replace(/۳/g, "3");
                    stttr2 = stttr2.replace(/۴/g, "4");
                    stttr2 = stttr2.replace(/۵/g, "5");
                    stttr2 = stttr2.replace(/۶/g, "6");
                    stttr2 = stttr2.replace(/۷/g, "7");
                    stttr2 = stttr2.replace(/۸/g, "8");
                    stttr2 = stttr2.replace(/۹/g, "9");
                    console.log('neW strrr  ', stttr2 )
                    let factor = {
                        user_id:null,
                        insurance_id: null,
                        third_car_cate:this.state.activeVacle,
                        third_car_model:this.state.vacleType,
                        third_car_name:this.state.name,
                        third_car_years:this.state.year.value,
                        car_dmg_percent: this.state.insOffs.value,
                        car_dmg_life: this.state.damageNumber.value,
                        car_dmg_finance: this.state.damageNumberFin.value,
                        driver_dmg_percent: this.state.driverOffs.value,
                        driver_dmg_count: this.state.damageNumberDriver.value,
                        date_start_third: stttr,
                        date_end_third: stttr2,
                        usein: this.props.motor ? null : this.state.usage.value,
                        commitments: this.state.activeButton,
                        sort:this.state.filter,
                        before:null,
                        one:null,
                        two:null,
                        tree:null,
                        dataSelect:{
                            third_car_cate:this.state.activeVacleName,
                            third_car_model:this.state.vacleTypeTitle,
                            third_car_name:this.state.nameTitle,
                            third_car_years:this.state.yearTitle,
                            car_dmg_percent: this.state.insOffsTitle,
                            car_dmg_life: this.state.damageNumberTitle,
                            car_dmg_finance: this.state.damageNumberFinTitle,
                            driver_dmg_percent: this.state.driverOffsTitle,
                            driver_dmg_count: this.state.damageNumberDriverTitle,
                            date_start_third: stttr,
                            date_end_third: stttr2,
                            third_car_uses:this.state.usageTitle,
                            commitments:this.state.activeButton,
                            feature:null,
                            fianl_price:null
                        }
                    };
                    this.setState({loading:true});

                    // console.log('expiredDate', moment(this.state.selectedStartDate).unix())

                    console.log('third_car_cate',this.state.activeVacle)
                    console.log('third_car_model',this.state.vacleType)
                    console.log('third_car_name',this.state.name)
                    console.log('third_car_years',this.state.year.value)

                    console.log('car_dmg_percent', this.state.insOffs.value)
                    console.log('car_dmg_life',this.state.damageNumber.value)
                    console.log('car_dmg_finance', this.state.damageNumberFin.value)
                    console.log('driver_dmg_percent', this.state.driverOffs.value)
                    console.log('driver_dmg_count', this.state.damageNumberDriver.value)
                    console.log('date_start_third', stttr)
                    console.log('date_end_third', stttr2)


                    console.log('usein',this.state.usage.value)
                    console.log('commitments',this.state.activeButton)
                    Axios.post('/request/req_third_person', {
                        insurance_id: null,
                        third_car_cate:this.state.activeVacle,
                        third_car_model:this.state.vacleType,
                        third_car_name:this.state.name,
                        third_car_years:this.state.year.value,
                        car_dmg_percent: this.state.insOffs.value,
                        car_dmg_life: this.state.damageNumber.value,
                        car_dmg_finance: this.state.damageNumberFin.value,
                        driver_dmg_percent: this.state.driverOffs.value,
                        driver_dmg_count: this.state.damageNumberDriver.value,
                        date_start_third: stttr,
                        date_end_third: stttr2,
                        usein: this.state.usage.value,
                        commitments: this.state.activeButton,
                        sort: this.state.filter
                    }).then(response=> {
                        this.setState({loading: false});
                        console.log('third bime',response.data)
                        console.log('insurType:this.props.motor ? motorThird lllllllllllllllllllllllllllll', this.props.motor ?'motor':'Third')

                        Actions.prices({openDrawer:this.props.openDrawer, bime: response.data.data, pageTitle: 'قیمت ها', factor:factor, user_details:user_details, insurType:this.props.motor ?'motor':'Third'})
                    })
                        .catch((response) => {
                            console.log(response.response)
                            // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                            // this.setState({loading: false});
                            this.setState({modalVisible: true, loading: false});

                        });
                }
            }
            // });
        }
        else{
            this.setState({redBorder: true})
        }
    }
    getCat(id) {
        console.log(id);
        let idNew = this.props.motor? this.state.vacleId : id;

        this.setState({loading3:true});
        let baseAddress = '';
        if(this.props.bodyBime){
            baseAddress = '/request/get-kind-cars-body/'
        }
        else {
            baseAddress = '/request/get-kind-cars/'
        }
        Axios.get(baseAddress+idNew).then(response => {
            this.setState({loading3: false, kindCats: response.data.data});
            console.log('kind cars', response.data.data);
        })
            .catch((error) =>{
                //  Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                // this.setState({loading: false});
                this.setState({modalVisible: true, loading3: false});

            });
    }
    getModel(id) {
        console.log(id);
        this.setState({loading2:true});
        let baseAddress = '';
        if(this.props.bodyBime){
            baseAddress = '/request/get-model-cars-body/'
        }
        else {
            baseAddress = '/request/get-model-cars/'
        }
        Axios.get(baseAddress+id).then(response => {
            this.setState({loading2: false, modelCats: response.data.data});
            console.log('model cars', response.data.data);
        })
            .catch((error) =>{
                // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                // this.setState({loading2: false});
                this.setState({modalVisible: true, loading2: false});

            });
    }
    switchButtons(status, id, title) {
        console.log(status)
        if(id === 1) {
            this.setState({
                Chemical: status
            }, () => {
                if(status) {
                    let feature = this.state.feature;
                    feature.push(title)
                    this.setState({feature: feature});
                }
            })
        }
        else if(id === 2) {
            this.setState({
                break_glass: status
            }, () => {
                if(status) {
                    let feature = this.state.feature;
                    feature.push(title)
                    this.setState({feature: feature});
                }
            })
        }
        else if(id === 3) {
            this.setState({
                natural_disaster: status
            }, () => {
                if(status) {
                    let feature = this.state.feature;
                    feature.push(title)
                    this.setState({feature: feature});
                }
            })
        }
        else if(id === 4) {
            this.setState({
                Steal: status
            }, () => {
                if(status) {
                    let feature = this.state.feature;
                    feature.push(title)
                    this.setState({feature: feature});
                }
            })
        }
        else if(id === 5) {
            this.setState({
                Price: status
            }, () => {
                if(status) {
                    let feature = this.state.feature;
                    feature.push(title)
                    this.setState({feature: feature});
                }
            })
        }
        else if(id === 6) {
            this.setState({
                Transit: status
            }, () => {
                if(status) {
                    let feature = this.state.feature;
                    feature.push(title)
                    this.setState({feature: feature});
                }
            })
        }
        else if(id === 7) {
            this.setState({
                transfer: status
            }, () => {
                if(status) {
                    let feature = this.state.feature;
                    feature.push(title)
                    this.setState({feature: feature});
                }
            })
        }
        else if(id === 8) {
            this.setState({
                zero: status
            })
        }
        else if(id === 9) {
            this.setState({
                good: status
            })
        }
        else if(id === 10) {
            this.setState({
                is_cash: status
            })
        }
    }
    closeModal() {
        this.setState({showPicker: false, showPicker2: false});
    }
    closeModal3() {
        this.setState({modalVisible: false});
    }
    onShowName = () => {
        this.setState({ visibleName: true });
    }

    onSelectName = (picked) => {
        console.log('picked', picked)
        this.setState({
            name: picked,
            visibleName: false
        }, () =>{
            this.state.kindCats.length !== 0 && this.state.kindCats.map((item) => {
                if(picked === item.id) {
                    this.setState({nameTitle: item.name}, () => {console.log(this.state.nameTitle)
                    });
                }
            })
            this.test(picked);
            this.getModel(picked);
        })
    }

    onCancelName = () => {
        this.setState({
            visibleName: false
        });
    }


    onShowModel = () => {
        if(this.state.name === 0) {
            this.setState({modalVisible: true, fillName: true });
        }
        else {
            this.setState({ visibleModel: true });

        }
    }

    onSelectModel = (picked) => {
        console.log('picked', picked)
        this.setState({
            vacleType: picked,
            visibleModel: false
        }, () =>{
            this.state.modelCats.length !== 0 && this.state.modelCats.map((item) => {
                if(picked === item.id) {
                    this.setState({vacleTypeTitle: item.name}, () => {console.log(this.state.vacleTypeTitle)
                    });
                }
            })
            this.test();
        })
    }

    onCancelModel = () => {
        this.setState({
            visibleModel: false
        })
    }



    renderOption = (settings) =>{
        const { item, getLabel } = settings
        return (
            <View style={styles.optionContainer}>
                <View style={styles.innerContainer}>
                    <View style={[styles.box, { backgroundColor: item.color }]} />
                    <Text style={{ color: item.color, alignSelf: 'flex-start' }}>{getLabel(item)}</Text>
                </View>
            </View>
        )
    }

    render() {
        // const now = new Date()
        // alert(moment_jalaali(now))
        const vaclePriceBody = this.state.vaclePrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")

        let arr1 = [], arr2 = [];
        let yearsArray = []
        for(let i= 1299; i<1399; i++) {
            arr1.push(i.toString())
        }
        for(let j= 1920; j <2020; j++) {
            arr2.push(j.toString())
        }
        for(let c= 0 ; c <arr1.length; c++){
            const p = arr1[c]+ " - " +arr2[c];
            yearsArray.push(p)

        }
        // console.log('yearsArray', yearsArray)

        // const {posts, categories, user} = this.props;
        // const text11 =  <Text style={{textAlign: "right", fontFamily: "IRANSansMobile(FaNum)"}} >1971</Text>
        // const yearsArray = ["1397 - 2018", "1396 - 2017", "1395 - 2016", "1394 - 2015", "1393 - 2014", "1392 - 2013", "1391 - 2012", "1390 - 2011", "1389 - 2010", "1388 - 2009", "1387 - 2008", "1386 - 2007", "1385 - 2006", "1384 - 2005", "1383 - 2004", "1382 - 2003", "1381 - 2002", "1380 - 2001", "1379 - 2000","1378 - 1999","1377 - 1998","1376 - 1997","1375 - 1996","1374 - 1995","1373 - 1995","1372 - 1994","1371 - 1993","1370 - 1992","1369 - 1991","1368 - 1990","1367 - 1989","1366 - 1988","1364 - 1987","1363 - 1986","1362 - 1985","1361 - 1984","1360 - 1983"];
        const offsArray = ["بدون تخفیف-صفر کیلومتر","5 درصد", "10 درصد" , "15 درصد", "20 درصد" , "25 درصد", "30 درصد", "35 درصد" , "40 درصد", "45 درصد", "50 درصد" , "55 درصد" , "60 درصد", "65 درصد", "70 درصد"];
        const offsArrayDriver = ["بدون تخفیف","یک سال تخفیف", "دو سال تخفیف" , "سه سال تخفیف", "چهار سال تخفیف", "پنج سال تخفیف" , "شش سال تخفیف", "هفت سال تخفیف", "هشت سال تخفیف و بیشتر" ];
        const bodyThirdBimeArray = ["یک سال ", "دو سال " , "سه سال ", "چهار سال ", "پنج سال " , "شش سال ", "هفت سال ", "هشت سال و بیشتر " ];
        const dmgNumberArray = [ "بدون خسارت", "1", "2", "3 و یا بیشتر"];
        const dmgNumberDriverArray = [ "بدون خسارت", "یک بار مالی", "دو بار مالی", "دو بار مالی و بیشتر"];
        const usageArray = ["انتخاب نشده", "شخصی", "تاکسی درون شهری", "تاکسی برون شهری", "مدارس"];
        // if(this.state.loading){
        //     return (<Loader />)
        // }

        // {
        //     this.state.kindCats.map((item) => {return {key: item.id, label: item.name}}
        //     )
        // }
        let items = [];

        let newyearsArray = [];
        for(let o=0; o< yearsArray.length; o++){
            // let x = yearsArray.pop();
            // newyearsArray.push(x)

            newyearsArray[o] = yearsArray[yearsArray.length-o-1]
        }
        // console.log('neeeewwwwyearsArray', newyearsArray)

        newyearsArray.map((item, index) => {items.push({key: index, label: item, value: index})})

        console.log('items', items)
        // let newItems = [];
        // for(let o=0; o<items.length; o++){
        //     let x = items.pop();
        //     newItems.push(x)
        // }

        let dmgItems = [];
        dmgNumberArray.map((item, index) => {dmgItems.push({key: index, label: item, value: index === 3 ? 4 : index})})

        let dmgItemsDriver = [];
        dmgNumberDriverArray.map((item, index) => {dmgItemsDriver.push({key: index, label: item, value: index})})

        let offsArrayItems = [];
        offsArray.map((item, index) => {offsArrayItems.push({key: index, label: item, value: index === 0 ? 0 : index*5})})

        let offsArrayItemsDriver = [];
        offsArrayDriver.map((item, index) => {offsArrayItemsDriver.push({key: index, label: item, value: index})})

        let usageItems = [];
        usageArray.map((item, index) => {usageItems.push({key: index, label: item, value: index})})

        let bodyThirdBimeItems = [];
        bodyThirdBimeItems.push({key: 10, label: 'باخسارت', value: 10})
        offsArrayDriver.map((item, index) => {bodyThirdBimeItems.push({key: index, label: item, value: index})})

        if(this.state.loading){
            return (<Loader />)
        }

        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <HomeHeader active={1} pageTitle={this.props.pageTitle} openDrawer={this.props.openDrawer}/>
                </View>
                <ScrollView style={styles.scroll}>
                    <View style={styles.bodyContainer}>
                        <View style={{width: "100%", zIndex: 0, paddingBottom: 70}}>
                            {
                                this.props.motor ? null :
                                    <View>
                                        <Text style={styles.text}>نوع وسیله نقلیه</Text>
                                        <View style={[styles.vacleContainer, {borderRadius: 10, borderColor: this.state.redBorder && this.state.activeVacle === 0 ? 'red' : 'transparent', borderWidth: 1}]}>
                                            {
                                                [{id: 1, name: 'سواری'}, {id: 2, name: 'بارکش'}, {id: 3, name: 'موتورسیکلت'}].map((item, index) =>
                                                    <TouchableOpacity key={index} style={[styles.vacle, {backgroundColor: this.state.activeVacle === item.id ? 'white': 'transparent', elevation: this.state.activeVacle === item.id ? 4 : 0, borderColor:  (this.state.activeVacle === item.id ? 'rgb(50, 197, 117)' : 'transparent'), borderWidth: 1}]} onPress={()=>{
                                                        this.setState({
                                                            activeVacle: item.id,
                                                            activeVacleName: item.name
                                                        }, () => {
                                                            this.test();
                                                            this.getCat(item.id);
                                                        })}}>
                                                        {
                                                            this.props.bodyBime ? (item.name !== 'موتورسیکلت' ?
                                                                    <Text style={styles.vacleText}>{item.name}</Text> : null

                                                            ) : <Text style={styles.vacleText}>{item.name}</Text>

                                                        }
                                                        {/*<Text style={styles.vacleText}>{item.name}</Text>*/}
                                                        {
                                                            item.name === "موتورسیکلت" ?
                                                                (this.props.bodyBime ? null:  <Image source={vacle3} style={[styles.Image, {tintColor: this.state.activeVacle === item.id ?  "rgb(15, 76, 183)" : undefined}]} />) :
                                                                (
                                                                    item.name ===  "بارکش" ?
                                                                        <Image source={vacle2} style={[styles.Image, {tintColor:  this.state.activeVacle === item.id ?  "rgb(15, 76, 183)" : undefined}]} />:
                                                                        <Image source={vacle1} style={[styles.Image, {tintColor: this.state.activeVacle === item.id ?  "rgb(15, 76, 183)" : "gray"}]} />
                                                                )
                                                        }
                                                    </TouchableOpacity>
                                                )
                                            }
                                        </View>
                                    </View>
                            }
                            <Text style={styles.text}>نام وسیله نقلیه</Text>
                            <TouchableOpacity onPress={this.onShowName} style={{position: 'relative',  zIndex: 3, width: '100%', backgroundColor: 'white', height: 42,  borderColor: this.state.redBorder && this.state.name === 0 ? 'red' : (this.state.name !== 0 ? 'rgb(50, 197, 117)' : '#ccc'), borderWidth: 1, borderRadius: 10}}>
                                <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>
                                <Text style={styles.itemLabel}>{this.state.loading3 ? "در حال بارگذاری..." : this.state.name === 0 ? "انتخاب نشده" : this.state.nameTitle}</Text>
                                <ModalFilterPicker
                                    visible={this.state.visibleName}
                                    onSelect={this.onSelectName}
                                    onCancel={this.onCancelName}
                                    options={ this.state.kindCats.map((item) => {return {key: item.id, label: item.name}})}
                                    placeholderText="جستجو ..."
                                    cancelButtonText="لغو"
                                    filterTextInputStyle={{textAlign: 'right',  fontFamily: 'IRANSansMobile(FaNum)',}}
                                    optionTextStyle={{textAlign: 'right', width: '100%', fontFamily: 'IRANSansMobile(FaNum)',}}
                                    titleTextStyle={{textAlign: 'right', fontFamily: 'IRANSansMobile(FaNum)',}}
                                />
                            </TouchableOpacity>
                            <Text style={styles.text}>مدل وسیله نقلیه</Text>
                            <TouchableOpacity onPress={this.onShowModel} style={{position: 'relative',  zIndex: 3, width: '100%', backgroundColor: 'white', height: 42,  borderColor: this.state.redBorder && this.state.vacleType === 0 ? 'red' : (this.state.vacleType !== 0 ? 'rgb(50, 197, 117)' : '#ccc'), borderWidth: 1, borderRadius: 10}}>
                                <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>
                                <Text style={styles.itemLabel}>{this.state.loading ? "در حال بارگذاری..." : this.state.vacleType === 0 ? "انتخاب نشده" : this.state.vacleTypeTitle}</Text>
                                <ModalFilterPicker
                                    visible={this.state.visibleModel}
                                    onSelect={this.onSelectModel}
                                    onCancel={this.onCancelModel}
                                    options={ this.state.modelCats.map((item) => {return {key: item.id, label: item.name}})}
                                    placeholderText="جستجو ..."
                                    cancelButtonText="لغو"
                                    filterTextInputStyle={{textAlign: 'right', fontFamily: 'IRANSansMobile(FaNum)',}}
                                    optionTextStyle={{textAlign: 'right', width: '100%', fontFamily: 'IRANSansMobile(FaNum)',}}
                                    titleTextStyle={{textAlign: 'right', fontFamily: 'IRANSansMobile(FaNum)',}}
                                />
                            </TouchableOpacity>
                            <Text style={styles.text}>سال ساخت</Text>
                            <View style={{position: 'relative',  zIndex: 3, width: '100%', backgroundColor: 'white', height: 42, borderRadius: 10, borderColor:  (this.state.year.value !== 0 ? 'rgb(50, 197, 117)' : '#ccc'), borderWidth: 1}}>
                                {/*<FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>*/}
                                <Selectbox
                                    style={{height: 42, paddingTop: '3%', paddingRight: 10}}
                                    selectLabelStyle={{textAlign: 'left', color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13, paddingTop: 0}}
                                    optionLabelStyle={{textAlign: 'center', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                    selectedItem={this.state.year}
                                    cancelLabel="لغو"
                                    onChange={(itemValue) =>{
                                        this.setState({
                                            year: itemValue
                                        }, () => {
                                            this.setState({yearTitle: itemValue.label}, () => {console.log(this.state.yearTitle)});
                                            this.test();
                                        })}}
                                    items={items} />
                            </View>
                            {
                                this.props.thirdBime ?
                                    <View>
                                        <Text style={styles.text}>درصد تخفیف بیمه نامه قبلی</Text>
                                        <View style={{position: 'relative',  zIndex: 3, width: '100%', backgroundColor: 'white', height: 42, borderRadius: 10, borderColor:  (this.state.insOffs.value !== 0 ? 'rgb(50, 197, 117)' : '#ccc'), borderWidth: 1}}>
                                            {/*<FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>*/}
                                            <Selectbox
                                                style={{height: 42, paddingTop: '3%', paddingRight: 10}}
                                                selectLabelStyle={{textAlign: 'right', color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13, paddingTop: 0}}
                                                optionLabelStyle={{textAlign: 'center', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                                selectedItem={this.state.insOffs}
                                                cancelLabel="لغو"
                                                onChange={(itemValue) =>{
                                                    this.setState({
                                                        insOffs: itemValue
                                                    }, () => {
                                                        this.setState({insOffsTitle: offsArray[itemValue.key]}, () => {console.log(this.state.insOffsTitle)});
                                                        this.test();
                                                    })}}
                                                items={offsArrayItems} />
                                        </View>

                                        <Text style={styles.text}>تعداد خسارت جانی</Text>
                                        <View style={{position: 'relative',  zIndex: 3, width: '100%', backgroundColor: 'white', height: 42, borderRadius: 10,  borderColor:  (this.state.damageNumber.value !== 0 ? 'rgb(50, 197, 117)' : '#ccc'), borderWidth: 1}}>
                                            {/*<FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>*/}
                                            <Selectbox
                                                style={{height: 42, paddingTop: '3%', paddingRight: 10}}
                                                selectLabelStyle={{textAlign: 'right', color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13, paddingTop: 0}}
                                                optionLabelStyle={{textAlign: 'center', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                                selectedItem={this.state.damageNumber}
                                                cancelLabel="لغو"
                                                onChange={(itemValue) =>{
                                                    this.setState({
                                                        damageNumber: itemValue
                                                    }, () => {
                                                        this.setState({damageNumberTitle: dmgNumberArray[itemValue.value]}, () => {console.log(this.state.damageNumberTitle)});
                                                        this.test();
                                                    })}}
                                                items={dmgItems} />
                                        </View>

                                        <Text style={styles.text}>تعداد خسارت  مالی</Text>
                                        <View style={{position: 'relative',  zIndex: 3, width: '100%', backgroundColor: 'white', height: 42, borderRadius: 10,  borderColor:  (this.state.damageNumberFin.value !== 0 ? 'rgb(50, 197, 117)' : '#ccc'), borderWidth: 1}}>
                                            {/*<FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>*/}
                                            <Selectbox
                                                style={{height: 42, paddingTop: '3%', paddingRight: 10}}
                                                selectLabelStyle={{textAlign: 'right', color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13, paddingTop: 0}}
                                                optionLabelStyle={{textAlign: 'center', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                                selectedItem={this.state.damageNumberFin}
                                                cancelLabel="لغو"
                                                onChange={(itemValue) =>{
                                                    this.setState({
                                                        damageNumberFin: itemValue
                                                    }, () => {
                                                        this.setState({damageNumberFinTitle: dmgNumberArray[itemValue.value]}, () => {console.log(this.state.damageNumberFinTitle)});
                                                        this.test();
                                                    })}}
                                                items={dmgItems} />
                                        </View>
                                        <Text style={styles.text}>درصد تخفیف حوادث راننده</Text>
                                        <View style={{position: 'relative',  zIndex: 3, width: '100%', backgroundColor: 'white', height: 42, borderRadius: 10, borderColor:  (this.state.driverOffs.value !== 0 ? 'rgb(50, 197, 117)' : '#ccc'), borderWidth: 1 }}>
                                            {/*<FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>*/}
                                            <Selectbox
                                                style={{height: 42, paddingTop: '3%', paddingRight: 10}}
                                                selectLabelStyle={{textAlign: 'right', color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13, paddingTop: 0}}
                                                optionLabelStyle={{textAlign: 'center', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                                selectedItem={this.state.driverOffs}
                                                cancelLabel="لغو"
                                                onChange={(itemValue) =>{
                                                    this.setState({
                                                        driverOffs: itemValue
                                                    }, () => {
                                                        this.setState({driverOffsTitle: offsArray[itemValue.key]}, () => {console.log(this.state.driverOffsTitle)});
                                                        this.test();
                                                    })}}
                                                items={offsArrayItems} />
                                        </View>
                                        <Text style={styles.text}>تعداد خسارت حوادث راننده</Text>
                                        <View style={{position: 'relative',  zIndex: 3, width: '100%', backgroundColor: 'white', height: 42, borderRadius: 10,  borderColor:  (this.state.damageNumberDriver.value !== 0 ? 'rgb(50, 197, 117)' : '#ccc'), borderWidth: 1}}>
                                            {/*<FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>*/}
                                            <Selectbox
                                                style={{height: 42, paddingTop: '3%', paddingRight: 10}}
                                                selectLabelStyle={{textAlign: 'right', color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13, paddingTop: 0}}
                                                optionLabelStyle={{textAlign: 'center', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                                selectedItem={this.state.damageNumberDriver}
                                                cancelLabel="لغو"
                                                onChange={(itemValue) =>{
                                                    this.setState({
                                                        damageNumberDriver: itemValue
                                                    }, () => {
                                                        this.setState({damageNumberDriverTitle: dmgNumberArray[itemValue.value]}, () => {console.log(this.state.damageNumberDriverTitle)});
                                                        this.test();
                                                    })}}
                                                items={dmgItems} />
                                        </View>
                                        <Text style={styles.text}>تاریخ شروع بیمه نامه قبلی</Text>

                                        <TouchableOpacity onPress={() => this.setState({showPicker: true})} style={{position: 'relative',  zIndex: 3, width: '100%', backgroundColor: 'white', height: 42,  borderColor: this.state.redBorder && this.state.selectedStartDate === null ? 'red' :  (this.state.selectedStartDate !== null ? 'rgb(50, 197, 117)' : '#ccc'), borderWidth: 1, borderRadius: 10}}>
                                            <Text style={{textAlign: 'right', paddingTop: '3%', paddingRight: 10}}>{this.state.selectedStartDate !==null ? moment_jalaali(this.state.selectedStartDate).format('jYYYY/jM/jD') : 'انتخاب کنید'}</Text>
                                            <Icon name="calendar-o" size={20} color="#21b34f" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>
                                        </TouchableOpacity>
                                        <Text style={styles.text}>تاریخ انقضای بیمه نامه قبلی</Text>

                                        <TouchableOpacity onPress={() => this.setState({showPicker2: true})} style={{position: 'relative',  zIndex: 3, width: '100%', backgroundColor: 'white', height: 42,  borderColor: this.state.redBorder && this.state.selectedStartDate2 === null ? 'red' :  (this.state.selectedStartDate2 !== null ? 'rgb(50, 197, 117)' : '#ccc'), borderWidth: 1, borderRadius: 10}}>
                                            <Text style={{textAlign: 'right', paddingTop: '3%', paddingRight: 10}}>{this.state.selectedStartDate2 !==null ? moment_jalaali(this.state.selectedStartDate2).format('jYYYY/jM/jD') : 'انتخاب کنید'}</Text>
                                            <Icon name="calendar-o" size={20} color="#21b34f" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>
                                        </TouchableOpacity>
                                        {
                                            this.props.motor ? null :
                                                <View>
                                                    <Text style={styles.text}>کاربری</Text>
                                                    <View style={{position: 'relative',  zIndex: 3, width: '100%', backgroundColor: 'white', height: 42, borderRadius: 10, borderColor: this.state.redBorder && this.state.usage.value === 0 ? 'red' :  (this.state.usage.value !== 0 ? 'rgb(50, 197, 117)' : '#ccc'), borderWidth: this.state.redBorder && this.state.usage.value === 0 ? 1 : (this.state.usage.value !== 0 ? 1 : 0 )}}>
                                                        {/*<FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>*/}
                                                        <Selectbox
                                                            style={{height: 42, paddingTop: '3%', paddingRight: 10}}
                                                            selectLabelStyle={{textAlign: 'right', color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13, paddingTop: 0}}
                                                            optionLabelStyle={{textAlign: 'center', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                                            selectedItem={this.state.usage}
                                                            cancelLabel="لغو"
                                                            onChange={(itemValue) =>{
                                                                this.setState({
                                                                    usage: itemValue
                                                                }, () => {
                                                                    this.setState({usageTitle: usageArray[itemValue.value]}, () => {console.log(this.state.usageTitle)});
                                                                    this.test();
                                                                })}}
                                                            items={usageItems} />
                                                    </View>
                                                </View>
                                        }
                                        <Text style={styles.text}>حداکثر پوشش بیمه</Text>

                                        <View style={styles.numberOfMeals}>
                                            <TouchableOpacity onPress={()=>{this.setState({activeButton: 9}, () => {this.test();})}}>
                                                <View style={[styles.circle, {backgroundColor: this.state.activeButton === 9 ? '#21b34f' : 'white'}]}>
                                                    <Text style={[styles.questionText, {fontSize: 14, color: this.state.activeButton === 9 ? 'white' : 'black'}]}>9</Text>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={()=>{this.setState({activeButton: 10}, () => {this.test();})}}>
                                                <View style={[styles.circle, {backgroundColor: this.state.activeButton === 10 ? '#21b34f' : 'white'}]}>
                                                    <Text style={[styles.questionText, {fontSize: 14, color: this.state.activeButton === 10 ? 'white' : 'black'}]}>10</Text>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={()=>{this.setState({activeButton: 15}, () => {this.test();})}}>
                                                <View style={[styles.circle, {backgroundColor: this.state.activeButton === 15 ? '#21b34f' : 'white'}]}>
                                                    <Text style={[styles.questionText, {fontSize: 14, color: this.state.activeButton === 15 ? 'white' : 'black'}]}>15</Text>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={()=>{this.setState({activeButton: 18}, () => {this.test();})}}>
                                                <View style={[styles.circle, {backgroundColor: this.state.activeButton === 18 ? '#21b34f' : 'white'}]}>
                                                    <Text style={[styles.questionText, {fontSize: 14, color: this.state.activeButton === 18 ? 'white' : 'black'}]}>18</Text>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={()=>{this.setState({activeButton: 20}, () => {this.test();})}}>
                                                <View style={[styles.circle, {backgroundColor: this.state.activeButton === 20 ? '#21b34f' : 'white'}]}>
                                                    <Text style={[styles.questionText, {fontSize: 14, color: this.state.activeButton === 20 ? 'white' : 'black' }]}>20</Text>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={()=>{this.setState({activeButton: 25}, () => {this.test();})}}>
                                                <View style={[styles.circle, {backgroundColor: this.state.activeButton === 25 ? '#21b34f' : 'white'}]}>
                                                    <Text style={[styles.questionText, {fontSize: 14, color: this.state.activeButton === 25 ? 'white' : 'black'}]}>25</Text>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={()=>{this.setState({activeButton: 28}, () => {this.test();})}}>
                                                <View style={[styles.circle, {backgroundColor: this.state.activeButton === 28 ? '#21b34f' : 'white'}]}>
                                                    <Text style={[styles.questionText, {fontSize: 14, color: this.state.activeButton === 28 ? 'white' : 'black'}]}>28</Text>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={()=>{this.setState({activeButton: 30}, () => {this.test();})}}>
                                                <View style={[styles.circle, {backgroundColor: this.state.activeButton === 30 ? '#21b34f' : 'white'}]}>
                                                    <Text style={[styles.questionText, {fontSize: 14, color: this.state.activeButton === 30 ? 'white' : 'black'}]}>30</Text>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={()=>{this.setState({activeButton: 35}, () => {this.test();})}}>
                                                <View style={[styles.circle, {backgroundColor: this.state.activeButton === 35 ? '#21b34f' : 'white'}]}>
                                                    <Text style={[styles.questionText, {fontSize: 14, color: this.state.activeButton === 35 ? 'white' : 'black'}]}>35</Text>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={()=>{this.setState({activeButton: 36}, () => {this.test();})}}>
                                                <View style={[styles.circle, {backgroundColor: this.state.activeButton === 36 ? '#21b34f' : 'white'}]}>
                                                    <Text style={[styles.questionText, {fontSize: 14, color: this.state.activeButton === 36 ? 'white' : 'black'}]}>36</Text>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={()=>{this.setState({activeButton: 40}, () => {this.test();})}}>
                                                <View style={[styles.circle, {backgroundColor: this.state.activeButton === 40 ? '#21b34f' : 'white'}]}>
                                                    <Text style={[styles.questionText, {fontSize: 14, color: this.state.activeButton === 40 ? 'white' : 'black'}]}>40</Text>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={()=>{this.setState({activeButton: 45}, () => {this.test();})}}>
                                                <View style={[styles.circle, {backgroundColor: this.state.activeButton === 45 ? '#21b34f' : 'white'}]}>
                                                    <Text style={[styles.questionText, {fontSize: 14, color: this.state.activeButton === 45 ? 'white' : 'black'}]}>45</Text>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={()=>{this.setState({activeButton: 50}, () => {this.test();})}}>
                                                <View style={[styles.circle, {backgroundColor: this.state.activeButton === 50 ? '#21b34f' : 'white'}]}>
                                                    <Text style={[styles.questionText, {fontSize: 14, color: this.state.activeButton === 50 ? 'white' : 'black'}]}>50</Text>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={()=>{this.setState({activeButton: 100}, () => {this.test();})}}>
                                                <View style={[styles.circle, {backgroundColor: this.state.activeButton === 100 ? '#21b34f' : 'white'}]}>
                                                    <Text style={[styles.questionText, {fontSize: 14, color: this.state.activeButton === 100 ? 'white' : 'black'}]}>100</Text>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={()=>{this.setState({activeButton: 120}, () => {this.test();})}}>
                                                <View style={[styles.circle, {backgroundColor: this.state.activeButton === 120 ? '#21b34f' : 'white'}]}>
                                                    <Text style={[styles.questionText, {fontSize: 14, color: this.state.activeButton === 120 ? 'white' : 'black'}]}>120</Text>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={()=>{this.setState({activeButton: 180}, () => {this.test();})}}>
                                                <View style={[styles.circle, {backgroundColor: this.state.activeButton === 180 ? '#21b34f' : 'white'}]}>
                                                    <Text style={[styles.questionText, {fontSize: 14, color: this.state.activeButton === 180 ? 'white' : 'black'}]}>180</Text>
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                        <View style={{width: '100%', backgroundColor: 'white',borderRadius: 10,  borderColor:'rgb(39, 203, 253)' , borderWidth: 1, marginTop: 20, padding: 20}}>
                                            <View style={styles.textRow}>
                                                <Text style={styles.value}>360،000،000 تومان</Text>
                                                <Text style={styles.labelTxt}>خسارت جانی: </Text>
                                            </View>
                                            <View style={styles.textRow}>
                                                <Text style={styles.value}>{this.state.activeButton}،000،000 تومان</Text>
                                                <Text style={styles.labelTxt}>خسارت مالی: </Text>
                                            </View>
                                            <View style={styles.textRow}>
                                                <Text style={styles.value}>270،000،000 تومان</Text>
                                                <Text style={styles.labelTxt}>خسارت راننده: </Text>
                                            </View>
                                        </View>
                                    </View>
                                    : null
                            }
                            {
                                this.props.bodyBime ?
                                    <View>
                                        <Text style={styles.text}>تخفیف بیمه شخص ثالث</Text>
                                        <View style={{position: 'relative',  zIndex: 3, width: '100%', backgroundColor: 'white', height: 42, borderRadius: 10, borderColor:  (this.state.bodyThirdBime.value !== 0 ? 'rgb(50, 197, 117)' : '#ccc'), borderWidth: 1}}>
                                            {/*<FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>*/}
                                            <Selectbox
                                                style={{height: 42, paddingTop: '3%', paddingRight: 10}}
                                                selectLabelStyle={{textAlign: 'right', color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13, paddingTop: 0}}
                                                optionLabelStyle={{textAlign: 'center', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                                selectedItem={this.state.bodyThirdBime}
                                                cancelLabel="لغو"
                                                onChange={(itemValue) =>{
                                                    this.setState({
                                                        bodyThirdBime: itemValue
                                                    }, () => {
                                                        this.setState({bodyThirdBimeTitle: offsArray[itemValue.key]}, () => {console.log(this.state.bodyThirdBimeTitle)});
                                                        this.test();
                                                    })}}
                                                items={offsArrayItems} />
                                        </View>

                                        <Text style={styles.text}>سال های عدم خسارت در بیمه بدنه</Text>
                                        <View style={{position: 'relative',  zIndex: 3, width: '100%', backgroundColor: 'white', height: 42, borderRadius: 10, borderColor:  (this.state.bodyBime.value !== 0 ? 'rgb(50, 197, 117)' : '#ccc'), borderWidth: 1}}>
                                            {/*<FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>*/}
                                            <Selectbox
                                                style={{height: 42, paddingTop: '3%', paddingRight: 10}}
                                                selectLabelStyle={{textAlign: 'right', color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13, paddingTop: 0}}
                                                optionLabelStyle={{textAlign: 'center', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                                selectedItem={this.state.bodyBime}
                                                cancelLabel="لغو"
                                                onChange={(itemValue) =>{
                                                    this.setState({
                                                        bodyBime: itemValue
                                                    }, () => {
                                                        this.setState({bodyBimeTitle: bodyThirdBimeArray[itemValue.value]}, () => {console.log(this.state.bodyBimeTitle)});
                                                        this.test();
                                                    })}}
                                                items={bodyThirdBimeItems} />
                                        </View>
                                        <Text style={styles.text}>ارزش خودرو(تومان)</Text>
                                        <View style={{position: 'relative',  zIndex: 3, width: '100%', backgroundColor: 'white', height: 42, borderRadius: 10, borderColor: this.state.redBorder && this.state.vaclePrice === '' ? 'red' : (this.state.vaclePrice !== '' ? 'rgb(50, 197, 117)' : '#ccc'), borderWidth: 1}}>

                                            <TextInput
                                                onChangeText={(text) =>{
                                                    this.setState({
                                                        vaclePrice: text.toString().replace(/,/g, "")
                                                    }, () => {
                                                        this.test();
                                                    })}}
                                                placeholder='ارزش خودرو'
                                                keyboardType={"numeric"}
                                                placeholderTextColor={'gray'}
                                                underlineColorAndroid='transparent'
                                                // value={this.state.vaclePrice}
                                                value={vaclePriceBody}
                                                secureTextEntry={this.props.secure}
                                                maxLength={10}
                                                style={{
                                                    height: 42,
                                                    paddingRight: 25,
                                                    width: '100%',
                                                    color: 'gray',
                                                    fontSize: 14,
                                                    textAlign: 'right',
                                                    // elevation: 4,
                                                    fontFamily: 'IRANSansMobile(FaNum)',
                                                    shadowColor: 'lightgray',
                                                    shadowOffset: {width: 5,height: 5},
                                                    shadowOpacity: 1
                                                }}
                                            />
                                        </View>
                                        <Text style={styles.featureLabel}>نحوه پرداخت</Text>
                                        <SwitchRow label="پرداخت نقدی" switchButtons={(status) => this.switchButtons(status, 10, 'is_cash')} valueOn/>
                                        <Text style={styles.featureLabel}>مشخصات خودرو</Text>
                                        <SwitchRow label="خوودرو صفر" switchButtons={(status) => this.switchButtons(status, 8, 'zero')}  />
                                        <SwitchRow label="خودرو باکیفیت" switchButtons={(status) => this.switchButtons(status, 9, 'good')} />
                                        <Text style={styles.featureLabel}>پوشش های اجباری</Text>
                                        <SwitchRow label="آتش سوزی" switchButtons={() => null} on />
                                        <SwitchRow label="صاعقه" switchButtons={() => null} on />
                                        <SwitchRow label="انفجار" switchButtons={() => null} on />
                                        <SwitchRow label="حادثه" switchButtons={() => null} on />
                                        <SwitchRow label="سرقت کلی" switchButtons={() => null} on />
                                        <Text style={styles.featureLabel}>پوشش های اختیاری</Text>
                                        <SwitchRow label="پاشیدن مواد شیمیایی و اسیدی" switchButtons={(status) => this.switchButtons(status, 1, 'chemical')} />
                                        <SwitchRow label="شکست شیشه" switchButtons={(status) => this.switchButtons(status, 2, 'break_glass')}/>
                                        <SwitchRow label="سیل، زلزله و بلایای طبیعی" switchButtons={(status) => this.switchButtons(status, 3, 'natural_disaster')} />
                                        <SwitchRow label="سرقت درجا" switchButtons={(status) => this.switchButtons(status, 4, 'steal')} />
                                        <SwitchRow label="نوسانات ارزش بازار" switchButtons={(status) => this.switchButtons(status, 5, 'price')} />
                                        <SwitchRow label="ایاب و ذهاب" switchButtons={(status) => this.switchButtons(status, 6, 'transit')} />
                                        <SwitchRow label="خروج از کشور" switchButtons={(status) => this.switchButtons(status, 7, 'transfer')} />
                                    </View> : null
                            }
                        </View>
                        <AlertModal
                            closeModal={(title) => this.closeModal(title)}
                            input={this.state.showPicker ? 1 : 2 }
                            modalVisible={this.state.showPicker || this.state.showPicker2}
                            onChange={(visible) => this.setState({showPicker: false, showPicker2: false})}
                            onDateChange={(date, input) => this.onDateChange(date, input)}
                            title={this.state.dateMassage ? 'تاریخ انقضا نمی تواند کمتر یا مساوی تاریخ شروع باشد' : this.state.priceAlert ? 'قیمت باید بین 1,000,000  تا 9,000,000,000 تومان باشد' : 'مشکلی در برقراری ارتباط با سرور به وجود آمده لطفا دوباره سعی کنید' }
                        />
                        <AlertView
                            closeModal={(title) => this.closeModal3(title)}
                            modalVisible={this.state.modalVisible}
                            onChange={(visible) => this.setState({modalVisible: false})}
                            title={this.state.priceAlert ? 'قیمت باید بین 1,000,000  تا 9,000,000,000 تومان باشد' :(this.state.fillName ? 'لطفا ابتدا نام وسیله نقلیه را انتخاب نمایید' : 'مشکلی در برقراری ارتباط با سرور به وجود آمده لطفا دوباره سعی کنید')  }
                        />
                    </View>
                </ScrollView>
                <View style={styles.footer}>
                    <TouchableOpacity style={[styles.iconLeftContainer, {backgroundColor: this.state.nextStep ? 'rgba(255, 193, 39, 1)' : 'rgba(200, 200, 200, 1)' }]} onPress={() => this.nextStep()}>
                        <FIcon name="arrow-left" size={18} color="white" style={{borderColor: 'white', borderWidth: 1, borderRadius: 20, padding: 3, marginRight: 5}} />
                        <Text style={styles.label}>بعدی</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.iconRightContainer} onPress={() => this.onBackPress()}>
                        <IIcon name="md-close" size={18} color="rgba(17, 103, 253, 1)" />
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}
export default VacleInfo;
