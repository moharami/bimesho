import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, StyleSheet, Picker, AsyncStorage, BackHandler, Alert, Image, TextInput} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import {store} from '../../config/store';
import {connect} from 'react-redux';
import Axios from 'axios';
import { CustomPicker } from 'react-native-custom-picker'
export const url = 'https://bimehsho.com/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import HomeHeader from "../../components/homeHeader/index";
import InsuranceInfo from '../../components/insuranceInfo'
import Selectbox from 'react-native-selectbox'
import AlertView from '../../components/modalMassage'
import moment from 'moment'
import moment_jalaali from 'moment-jalaali'
import F5Icon from 'react-native-vector-icons/dist/FontAwesome5';
import FIcon from 'react-native-vector-icons/dist/FontAwesome';
import FEIcon from 'react-native-vector-icons/dist/Feather';
import EIcon from 'react-native-vector-icons/dist/Entypo';
import FOIcon from 'react-native-vector-icons/dist/Foundation';
import MIcon from 'react-native-vector-icons/dist/MaterialCommunityIcons';

class Prices extends Component {
    constructor(props){
        super(props);
        this.state = {
            loading: false,
            status:1,
            showPicker:false,
            selectedStartDate:null,
            buy:false,
            factor:this.props.factor,
            instalment: false,
            buyId: null,
            filterUsage: { key: 0, label: "انتخاب کنید",  value: 0 },
            filterYear: { key: 0, label: "انتخاب کنید",  value: 0 },
            filter: {name: 'hand-point-down', label: "انتخاب کنید",  value: "point-down" },
            newInsurances: [],
            order: false,
            signed: false,
            modalVisible: false,
            damageYearTitle: '',
            usageTitle: '',
            activeButton: 9,
            show: false,
            selectedItem: null,
            iconItem: null
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount(){
        console.log('herer is prices')
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    handleNext(price, id, instalmentStatus, activeBuy) {
        console.log('prrrps factor', this.props.factor);

        this.setState({
            buy: true,
            buyId: activeBuy
        }, () => {
            let factor = this.props.factor;
            if(this.props.insurType === 'Third' ) {
                factor.insurance_id=id;
                factor.dataSelect.fianl_price=price;
                if(instalmentStatus){
                    factor.before=Math.floor((price/2)/100)*100;
                    factor.one=Math.floor((price/2)/1000)*1000;
                    factor.two=Math.floor((price/2)/1000)*1000;
                }
            }
            if(this.props.insurType === 'responsible') {
                factor.record_selected=id;
                factor.insurance_id=id;
                factor.price=price;
                factor.dataSelect.insurance_id=id;
                factor.dataSelect.record_selected=id;
                factor.dataSelect.fianl_price=price;
                if(instalmentStatus){
                    factor.before=Math.floor((price/2)/100)*100;
                    factor.one=Math.floor((price/2)/1000)*1000;
                    factor.two=Math.floor((price/2)/1000)*1000;
                }
            }
            else if(this.props.insurType==='body') {
                factor.dataSelect.fianl_price=price;
                factor.insurance_id=id;
                if(instalmentStatus){
                    factor.before=Math.floor((price/2)/100)*100;
                    factor.one=Math.floor((price/2)/1000)*1000;
                    factor.two=Math.floor((price/2)/1000)*1000;
                }
            }
            else if(this.props.insurType==='travel') {
                factor.dataSelect.fianl_price=price;
                factor.dataSelect.insurance.id=id;
                factor.insurance_id=id;
                factor.insurance.id=id;
                factor.price=price;
                if(instalmentStatus){
                    factor.before=Math.floor((price/2)/100)*100;
                    factor.one=Math.floor((price/2)/1000)*1000;
                    factor.two=Math.floor((price/2)/1000)*1000;
                }
            }
            else if(this.props.insurType === 'life') {
                factor.dataSelect.fianl_price = price;
                factor.insurance_id = id;
                factor.insurance.id = id;
                factor.record_selected = id;
                factor.price = price;
                if(instalmentStatus){
                    factor.before=Math.floor((price/2)/100)*100;
                    factor.one=Math.floor((price/2)/1000)*1000;
                    factor.two=Math.floor((price/2)/1000)*1000;
                }
            }
            else if(this.props.insurType==='fire' || this.props.insurType === 'earth') {
                factor.dataSelect.fianl_price=price;
                factor.insurance_id=id;
                if(instalmentStatus){
                    factor.before=Math.floor((price/2)/100)*100;
                    factor.one=Math.floor((price/2)/1000)*1000;
                    factor.two=Math.floor((price/2)/1000)*1000;
                }
            }
            else if(this.props.insurType==='complete') {
                factor.dataSelect.fianl_price=price;
                factor.insurance_id=id;
                factor.complete_selected_id=id;
                if(instalmentStatus){
                    factor.before=Math.floor((price/2)/100)*100;
                    factor.one=Math.floor((price/2)/1000)*1000;
                    factor.two=Math.floor((price/2)/1000)*1000;
                }
            }
            else if(this.props.insurType==='motor') {
                factor.insurance_id=id;
                factor.dataSelect.fianl_price=price;
                if(instalmentStatus){
                    factor.before=Math.floor((price/2)/100)*100;
                    factor.one=Math.floor((price/2)/1000)*1000;
                    factor.two=Math.floor((price/2)/1000)*1000;
                }
            }
            this.setState({factor:factor, instalment: instalmentStatus}, () => {this.nextStep()})
        })
    }
    nextStep() {
        // console.log('here is next step')
        if(this.state.buy) {
            // console.log('this.state.buy', this.state.buy)

            if(this.props.insBuy) {
                // console.log('this.props.insBuy', this.props.insBuy)

                if(this.props.user.birthday !== null && this.props.user.birthday !== 'null' ) {
                    // let stttr = moment_jalaali(this.props.user.birthday, 'YYYY-MM-DD').format('jYYYY/jMM/jDD')
                    let stttr = this.props.user.birthday;
                    // console.log('neW STTR ', stttr )
                    // stttr = stttr.replace(/۰/g, "0");
                    // stttr = stttr.replace(/۱/g, "1");
                    // stttr = stttr.replace(/۲/g, "2");
                    // stttr = stttr.replace(/۳/g, "3");
                    // stttr = stttr.replace(/۴/g, "4");
                    // stttr = stttr.replace(/۵/g, "5");
                    // stttr = stttr.replace(/۶/g, "6");
                    // stttr = stttr.replace(/۷/g, "7");
                    // stttr = stttr.replace(/۸/g, "8");
                    // stttr = stttr.replace(/۹/g, "9");
                    let newBirthday = stttr.split('-')
                    if(newBirthday.length === 1) {
                        newBirthday = stttr.split('/')
                    }

                    Actions.customerInfo({openDrawer: this.props.openDrawer, insBuy: true,  pageTitle:'مشخصات خریدار', factor:this.props.factor, user_details:this.props.user_details, insurType:this.props.insurType, instalment: this.props.instalment, birthdayYear: newBirthday[0], birthdayMonth: newBirthday[1], birthdayDay: newBirthday[2]})
                }
                else {
                    Actions.customerInfo({openDrawer: this.props.openDrawer, insBuy: true, pageTitle:'مشخصات خریدار', factor:this.props.factor, user_details:this.props.user_details, insurType:this.props.insurType, instalment: this.props.instalment, birthdayYear: '', birthdayMonth: '', birthdayDay: ''})
                }
            }
            else {
                AsyncStorage.getItem('token').then((info) => {
                    if(info !== null) {
                        const newInfo = JSON.parse(info);
                        // Axios.defaults.headers.common['Authorization'] = 'Bearer ' + newInfo.token;
                        const expiresTime = newInfo.expires_at;
                        const currentTime = new Date().getTime()/1000;
                        // console.log('expiresTime> currentTime', expiresTime> currentTime)

                        if(expiresTime> currentTime ){

                            // Actions.home({openDrawer: this.props.openDrawer, loged: true})
                            const token = newInfo.token;
                            const id = newInfo.user_id;
                            let newFactor = this.state.factor;
                            let newUserdetails = this.props.user_details;
                            newUserdetails.user_id = id
                            newUserdetails._token = token

                            if(this.props.insurType === 'body') {
                                newFactor.user_id = id;
                            }
                            if(this.props.insurType === 'Third') {
                                newFactor.user_id = id;
                            }
                            if(this.props.insurType === 'motor') {
                                newFactor.user_id = id;
                            }
                            else if(this.props.insurType === 'fire' || this.props.insurType === 'earth') {
                                newFactor.user_id = id;
                            }
                            else if(this.props.insurType === 'complete') {
                                newFactor.user_id = id;
                            }
                            else if(this.props.insurType === 'travel') {
                                newFactor.user_id = id;
                                newFactor.insurance.user_id = id;
                                newFactor.dataSelect.insurance.user_id = id;
                            }
                            else if(this.props.insurType === 'responsible') {
                                newFactor.user_id = id;
                            }
                            else if(this.props.insurType === 'life') {
                                newFactor.user_id = id;
                                newFactor.insurance.user_id = id;
                            }

                            if(this.props.user.birthday !== null && this.props.user.birthday !== 'null') {
                                let stttr = this.props.user.birthday;
                                // let stttr = moment_jalaali(this.props.user.birthday, 'YYYY-MM-DD').format('jYYYY/jMM/jDD')
                                // console.log('neW STTR ', stttr )
                                // stttr = stttr.replace(/۰/g, "0");
                                // stttr = stttr.replace(/۱/g, "1");
                                // stttr = stttr.replace(/۲/g, "2");
                                // stttr = stttr.replace(/۳/g, "3");
                                // stttr = stttr.replace(/۴/g, "4");
                                // stttr = stttr.replace(/۵/g, "5");
                                // stttr = stttr.replace(/۶/g, "6");
                                // stttr = stttr.replace(/۷/g, "7");
                                // stttr = stttr.replace(/۸/g, "8");
                                // stttr = stttr.replace(/۹/g, "9");
                                let newBirthday = stttr.split('-')
                                if(newBirthday.length === 1) {
                                    newBirthday = stttr.split('/')
                                }
                                Actions.customerInfo({openDrawer: this.props.openDrawer, pageTitle:'مشخصات خریدار', factor:newFactor, user_details:newUserdetails, insurType:this.props.insurType, instalment: this.state.instalment, birthdayYear: newBirthday[0], birthdayMonth: newBirthday[1], birthdayDay: newBirthday[2]})
                            }
                            else {
                                Actions.customerInfo({openDrawer: this.props.openDrawer, pageTitle:'مشخصات خریدار', factor:newFactor, user_details:newUserdetails, insurType:this.props.insurType, instalment: this.state.instalment, birthdayYear: '', birthdayMonth: '', birthdayDay: ''})
                            }
                        }
                        else {
                            Actions.login({openDrawer: this.props.openDrawer, insBuy: true, pageTitle:'مشخصات خریدار', factor:this.state.factor, user_details:this.props.user_details, insurType:this.props.insurType, instalment: this.state.instalment})
                            this.setState({modalVisible: false});
                            // this.setState({info: true});
                        }
                    }
                    else {
                        Actions.login({openDrawer: this.props.openDrawer, insBuy: true, pageTitle:'مشخصات خریدار', factor:this.state.factor, user_details:this.props.user_details, insurType:this.props.insurType, instalment: this.state.instalment});
                        this.setState({modalVisible: false});
                    }
                });
            }
        }
    }
    orderInsurance() {
        this.setState({loading:true})
        if(this.props.insurType === 'body') {
            Axios.post('/request/req_body', {
                body_car_cate:this.props.factor.body_car_cate,
                body_car_name:this.props.factor.body_car_name,
                body_car_model:this.props.factor.body_car_model,
                body_car_years:this.props.factor.body_car_years,
                third_dmg_years:this.props.factor.third_dmg_years,
                body_dmg_years:this.props.factor.body_dmg_years,
                body_car_price:this.props.factor.body_car_price,
                insurance_id:null,
                Chemical:this.props.Chemical,
                break_glass:this.props.break_glass,
                natural_disaster:this.props.natural_disaster,
                Steal:this.props.Steal,
                Price:this.props.Price,
                Transit:this.props.Transit,
                transfer:this.props.transfer,
                sort: this.state.filter.value
            }).then(response=> {
                this.setState({ newInsurances: response.data.data, loading: false, order: true });
                // console.log('response', this.state.loading)
            })
                .catch((error) => {
                    console.log(error)
                    // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                    // this.setState({loading: false});
                    this.setState({modalVisible: true, loading: false});
                });
        }
        else if( this.props.insurType === 'Third' || this.props.insurType === 'motor') {
            Axios.post('/request/req_third_person', {
                date_start_third:this.props.factor.date_start_third,
                date_end_third:this.props.factor.date_end_third,
                insurance_id: null,
                third_car_cate:this.props.factor.third_car_cate,
                third_car_model:this.props.factor.third_car_model,
                third_car_name:this.props.factor.third_car_name,
                car_dmg_percent:this.props.factor.car_dmg_percent,
                car_dmg_life:this.props.factor.car_dmg_life,
                car_dmg_finance:this.props.factor.car_dmg_finance,
                driver_dmg_percent:this.props.factor.driver_dmg_percent,
                driver_dmg_count: this.props.factor.driver_dmg_count,
                third_car_years:this.props.factor.third_car_years,
                usein:this.props.factor.usein,
                commitments:this.props.factor.commitments,
                sort: this.state.filter.value
            }).then(response=> {
                this.setState({newInsurances: response.data.data, loading: false});
                // console.log('loading', this.state.loading)
            })
                .catch((error) => {
                    console.log(error)
                    // console.log(error.response)
                    // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                    // this.setState({loading: false});
                    this.setState({modalVisible: true, loading: false});
                });
        }
        if(this.props.insurType === 'fire' || this.props.insurType === 'earth') {
            Axios.post('/request/req_fire', {
                fire_home_type: this.props.factor.fire_home_type,
                fire_home_count: this.props.factor.fire_home_count,
                fire_structure: this.props.factor.fire_structure,
                fire_home_price: this.props.factor.fire_home_price,
                fire_meters: this.props.factor.fire_meters,
                insurance_id: null,
                earthquake: this.props.earthquake,
                pipe: this.props.pipe,
                earth: this.props.earth,
                rain: this.props.rain,
                tornado: this.props.tornado,
                flood: this.props.flood,
                airplane: this.props.airplane,
                steal: this.props.steal,
                area_price: this.props.factor.area_price,
                sort: this.state.filter.value
            }).then(response=> {
                this.setState({ newInsurances: response.data.data, loading: false});
            })
                .catch((error) => {
                    console.log(error)
                    // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                    // this.setState({loading: false});
                    this.setState({modalVisible: true, loading: false});
                });
        }
        if(this.props.insurType === 'travel') {
            Axios.post('/request/req_travel', {
                travel_country: this.props.factor.travel_country,
                travel_type: this.props.factor.travel_type,
                travel_age: this.props.factor.travel_age,
                travel_time: this.props.factor.travel_time,
                sort: this.state.filter.value
            }).then(response=> {
                this.setState({ newInsurances: response.data.data, loading: false});
            })
                .catch((error) => {
                    console.log(error)
                    // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                    // this.setState({loading: false});
                    this.setState({modalVisible: true, loading: false});
                });
        }
        if(this.props.insurType === 'responsible') {
            Axios.post('/request/req_responsible', {
                res_cost: this.props.factor.res_cost,
                res_job: this.props.factor.res_job,
                res_time: this.props.factor.res_time,
                resp_type_text: this.props.factor.resp_type_text,
                sort: this.state.filter.value
            }).then(response=> {
                this.setState({ newInsurances: response.data.data, loading: false});
            })
                .catch((error) => {
                    console.log(error)
                    // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                    // this.setState({loading: false});
                    this.setState({modalVisible: true, loading: false});
                });
        }
        if(this.props.insurType === 'complete') {
            Axios.post('/request/req_complete', {
                age: this.props.factor.age,
                type: this.props.factor.type,
                insurancer: this.props.factor.insurancer,
                count_men: this.props.factor.count_men,
                resp_type: this.props.factor.resp_type,
                sort: this.state.filter.value
            }).then(response=> {
                this.setState({ newInsurances: response.data.data, loading: false});
            })
                .catch((error) => {
                    console.log(error)
                    // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                    // this.setState({loading: false});
                    this.setState({modalVisible: true, loading: false});

                });
        }
        if(this.props.insurType === 'life') {
            Axios.post('/request/req_life', {
                life_cost: this.props.factor.life_cost,
                life_pay: this.props.factor.life_pay,
                life_years: this.props.factor.life_years,
                sort: this.state.filter.value
            }).then(response=> {
                this.setState({ newInsurances: response.data.data, loading: false});
            })
                .catch((error) => {
                    console.log(error)
                    // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                    // this.setState({loading: false});
                    this.setState({modalVisible: true, loading: false});

                });
        }
    }
    closeModal() {
        this.setState({modalVisible: false});
    }
    checkData(num) {
        let newFactor = this.props.factor;
        if(num === 1) {
            newFactor.third_dmg_years = this.state.filterYear.value;
        }
        else if(num === 2) {
            newFactor.usein = this.state.filterUsage.value;
        }
        else if(num === 3) {
            newFactor.commitments = this.state.activeButton;
        }
        this.setState({factor: newFactor, loading: true});
        Axios.post('/request/req_third_person', {
            date_start_third:this.props.factor.date_start_third,
            date_end_third:this.props.factor.date_end_third,
            insurance_id: null,
            third_car_cate:this.props.factor.third_car_cate,
            third_car_model:this.props.factor.third_car_model,
            third_car_name:this.props.factor.third_car_name,
            car_dmg_percent:this.props.factor.car_dmg_percent,
            car_dmg_life:this.props.factor.car_dmg_life,
            car_dmg_finance:this.props.factor.car_dmg_finance,
            driver_dmg_percent:this.props.factor.driver_dmg_percent,
            driver_dmg_count: this.props.factor.driver_dmg_count,
            third_car_years:this.props.factor.third_car_years,
            usein:this.props.factor.usein,
            commitments:this.props.factor.commitments,
            sort: this.state.filter.value
        }).then(response=> {
            this.setState({newInsurances: response.data.data, loading: false});
        })
            .catch((error) => {
                console.log(error)
                // console.log(error.response)
                // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                // this.setState({loading: false});
                this.setState({modalVisible: true, loading: false});
            });
    }
    renderField(settings) {
        const { selectedItem, defaultText, getLabel, clear } = settings
        // console.log('settings', settings)
        let iconItem = null;

        if(selectedItem !== undefined) {
            // console.log('selectedItem', selectedItem)

            switch(selectedItem.name){
                case 'hand-point-down':
                    iconItem = <F5Icon name="hand-point-down" size={18} color="rgb(153, 33, 24)" style={{paddingRight: 15}} />
                    break;
                case 'price-tag':
                    iconItem = <EIcon name="price-tag" size={18} color="rgb(153, 33, 24)" style={{paddingRight: 15}} />
                    break;
                case 'area-chart':
                    iconItem = <FIcon name="area-chart" size={18} color="rgb(153, 33, 24)" style={{paddingRight: 15}} />
                    break;
                case 'star':
                    iconItem = <F5Icon name="star" size={18} color="rgb(153, 33, 24)" style={{paddingRight: 15}} />
                    break;
                case 'graph-pie':
                    iconItem = <FOIcon name="graph-pie" size={18} color="rgb(153, 33, 24)" style={{paddingRight: 15}} />
                    break;
                case 'line-chart':
                    iconItem = <FIcon name="line-chart" size={18} color="rgb(153, 33, 24)" style={{paddingRight: 15}} />
                    break;
                case 'office-building':
                    iconItem = <MIcon name="office-building" size={18} color="rgb(153, 33, 24)" style={{paddingRight: 15}} />
                    break;
                case 'truck':
                    iconItem = <F5Icon name="truck" size={18} color="rgb(153, 33, 24)" style={{paddingRight: 15}} />
                    break;
                case 'alarm':
                    iconItem = <MIcon name="alarm" size={18} color="rgb(153, 33, 24)" style={{paddingRight: 15}} />
                    break;
                case 'wallet':
                    iconItem = <F5Icon name="wallet" size={18} color="rgb(153, 33, 24)" style={{paddingRight: 15}} />
                    break;
                case 'percent':
                    iconItem = <F5Icon name="percent" size={18} color="rgb(153, 33, 24)" style={{paddingRight: 15}} />
                    break;
                case 'warning':
                    iconItem = <EIcon name="warning" size={18} color="rgb(153, 33, 24)" style={{paddingRight: 15}} />
                    break;


            }
            this.setState({selectedItem: selectedItem, iconItem: iconItem})
        }

        return (
            <View style={styles2.container}>
                <View style={{width: '100%'}}>
                    {!this.state.selectedItem && <Text style={[styles2.text, { color: 'grey' }]}>{defaultText}</Text>}
                    {this.state.selectedItem && (
                        //
                        <View style={styles2.optionContainer}>
                            <View style={styles2.innerContainer}>
                                {/*<View style={[styles2.box, { backgroundColor: item.color }]} />*/}
                                <Text style={{ alignSelf: 'flex-start', paddingRight: 10 }}>{getLabel(this.state.selectedItem)}</Text>
                                {this.state.iconItem}
                            </View>
                        </View>
                    )}
                </View>
            </View>
        )
    }
    renderOption(settings) {
        const { item, getLabel } = settings
        let iconItem = null;
        switch(item.name){
            case 'hand-point-down':
                iconItem = <F5Icon name="hand-point-down" size={22} color="rgb(153, 33, 24)" style={{paddingRight: 15}} />
                break;
            case 'price-tag':
                iconItem = <EIcon name="price-tag" size={22} color="rgb(153, 33, 24)" style={{paddingRight: 15}} />
                break;
            case 'area-chart':
                iconItem = <FIcon name="area-chart" size={22} color="rgb(153, 33, 24)" style={{paddingRight: 15}} />
                break;
            case 'star':
                iconItem = <F5Icon name="star" size={22} color="rgb(153, 33, 24)" style={{paddingRight: 15}} />
                break;
            case 'graph-pie':
                iconItem = <FOIcon name="graph-pie" size={22} color="rgb(153, 33, 24)" style={{paddingRight: 15}} />
                break;
            case 'line-chart':
                iconItem = <FIcon name="line-chart" size={22} color="rgb(153, 33, 24)" style={{paddingRight: 15}} />
                break;
            case 'office-building':
                iconItem = <MIcon name="office-building" size={22} color="rgb(153, 33, 24)" style={{paddingRight: 15}} />
                break;
            case 'truck':
                iconItem = <F5Icon name="truck" size={22} color="rgb(153, 33, 24)" style={{paddingRight: 15}} />
                break;
            case 'alarm':
                iconItem = <MIcon name="alarm" size={22} color="rgb(153, 33, 24)" style={{paddingRight: 15}} />
                break;
            case 'wallet':
                iconItem = <F5Icon name="wallet" size={22} color="rgb(153, 33, 24)" style={{paddingRight: 15}} />
                break;
            case 'percent':
                iconItem = <F5Icon name="percent" size={22} color="rgb(153, 33, 24)" style={{paddingRight: 15}} />
                break;
            case 'warning':
                iconItem = <EIcon name="warning" size={22} color="rgb(153, 33, 24)" style={{paddingRight: 15}} />
                break;


        }
        return (
            <View style={styles2.optionContainer}>
                <View style={styles2.innerContainer}>
                    {/*<View style={[styles2.box, { backgroundColor: item.color }]} />*/}
                    <Text style={{ color: item.color, alignSelf: 'flex-start', paddingRight: 10 }}>{getLabel(item)}</Text>
                    {iconItem}
                </View>
            </View>
        )
    }
    render() {
        const offsArray = ["یک سال تخفیف", "دو سال تخفیف" , "سه سال تخفیف", "چهار سال تخفیف", "پنج سال تخفیف" , "شش سال تخفیف", "هفت سال تخفیف", "هشت سال تخفیف" , "نه سال تخفیف", "ده سال تخفیف", "یازده سال تخفیف" , "دوازده سال تخفیف"];
        const usageArray = ["تاکسی", "شخصی", "درون شهری", "برون شهری", "اداری", "مدارس"];

        const items = [
            { name: 'hand-point-down', label: "انتخاب کنید",  value: "point-down" },
            { name: 'price-tag', label: "قیمت (کم به زیاد)",  value: "price-tag" },
            { name: 'area-chart', label: "قیمت (زیاد به کم)",  value: "price-tag2" },
            { name: 'star', label: "سطح توانگری (زیاد به کم)",  value: "star-half" },
            { name: 'graph-pie', label: "رتبه رضایت مشتریان",  value: "pie-chart2" },
            { name: 'area-chart', label: "رتبه شکایت مشتریان",  value: "stats-dots" },
            { name: 'line-chart', label: "سهم بازار",  value: "stats-growth" },
            { name: 'office-building', label: "مراکز پرداخت خسارت",  value: "office" },
            { name: 'truck', label: "رتبه بیمه مرکزی",  value: "truck" },
            { name: 'alarm', label: "مدت زمان پاسخگویی به شکایات",  value: "alarm" },
            { name: 'wallet', label: "پشتوانه مالی",  value: "wallet" },
            { name: 'percent', label: "مبلغ تخفیف بیمه",  value: "percent" },
            { name: 'warning', label: "جریمه دیرکرد",  value: "warning2" }
        ];
        const usageItems = [
            { key: 0, label: "انتخاب کنید",  value: 0 },
            { key: 1, label: "تاکسی",  value: 1} ,
            { key: 2, label: "شخصی", value: 2} ,
            { key: 3, label: "درون شهری",  value: 3 },
            { key: 4, label: "برون شهری",  value: 4 },
            { key: 5, label: "اداری",  value: 5 },
            { key: 6, label: "مدارس",  value: 6 },
        ];
        const yearsItems = [];
        yearsItems.push({ key: 0, label: "انتخاب کنید",  value: 0 })
        const newItems =  offsArray.map((item, index) => { return{ key: index+1, label: item,  value: index+1 }});
        newItems.map((item, index) => yearsItems.push(item));
        console.log(yearsItems)

        console.log('asdhkasdhkad',this.state.newInsurances)
        console.log('object vlues ',Object.values(this.state.newInsurances))
        const {user} = this.props;
        if(this.state.loading){
            return (<Loader />)
        }
        else return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <HomeHeader active={2} pageTitle={this.props.pageTitle} openDrawer={this.props.openDrawer}/>
                </View>
                <ScrollView style={styles.scroll}>
                    <View style={styles.bodyContainer}>
                        {
                            this.props.insurType === 'Third' || this.props.insurType === 'motor' ?
                                <View style={{width: '100%'}}>
                                    <View style={{width: '100%'}}>
                                        {
                                            this.props.insurType === 'motor' ? null :
                                                <View>
                                                    <Text style={styles.text}>کاربری</Text>
                                                    <Selectbox
                                                        style={{ height: 40,
                                                            backgroundColor: 'white',
                                                            width: '100%',
                                                            borderColor: 'lightgray',
                                                            borderWidth: 1,
                                                            borderRadius: 10,
                                                            marginBottom: 20,
                                                            paddingRight: 15,
                                                            paddingTop: 10,
                                                        }}
                                                        selectedItem={this.state.filterUsage}
                                                        onChange={(itemValue, index) =>{
                                                            this.setState({
                                                                filterUsage: itemValue
                                                            }, () => {
                                                                this.setState({usageTitle: usageArray[index-1]}, () => {this.checkData(2)});
                                                            })}}
                                                        items={usageItems} />
                                                </View>
                                        }
                                        <Text style={styles.text}>حداکثر پوشش بیمه</Text>
                                        <View style={styles.numberOfMeals}>
                                            <TouchableOpacity onPress={()=>{this.setState({activeButton: 9}, () => this.checkData(3))}}>
                                                <View style={[styles.circle, {backgroundColor: this.state.activeButton === 9 ? '#21b34f' : 'white', marginRight: 5, marginBottom:5}]}>
                                                    <Text style={[styles.questionText, {fontSize: 14, color: this.state.activeButton === 9 ? 'white' : 'black'}]}>9</Text>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={()=>{this.setState({activeButton: 10}, () => this.checkData(3))}}>
                                                <View style={[styles.circle, {backgroundColor: this.state.activeButton === 10 ? '#21b34f' : 'white', marginRight: 5, marginBottom:5}]}>
                                                    <Text style={[styles.questionText, {fontSize: 14, color: this.state.activeButton === 10 ? 'white' : 'black'}]}>10</Text>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={()=>{this.setState({activeButton: 15}, () => this.checkData(3))}}>
                                                <View style={[styles.circle, {backgroundColor: this.state.activeButton === 15 ? '#21b34f' : 'white', marginRight: 5, marginBottom:5}]}>
                                                    <Text style={[styles.questionText, {fontSize: 14, color: this.state.activeButton === 15 ? 'white' : 'black'}]}>15</Text>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={()=>{this.setState({activeButton: 18}, () => this.checkData(3))}}>
                                                <View style={[styles.circle, {backgroundColor: this.state.activeButton === 18 ? '#21b34f' : 'white', marginRight: 5, marginBottom:5}]}>
                                                    <Text style={[styles.questionText, {fontSize: 14, color: this.state.activeButton === 18 ? 'white' : 'black'}]}>18</Text>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={()=>{this.setState({activeButton: 20}, () => this.checkData(3))}}>
                                                <View style={[styles.circle, {backgroundColor: this.state.activeButton === 20 ? '#21b34f' : 'white', marginRight: 5, marginBottom:5}]}>
                                                    <Text style={[styles.questionText, {fontSize: 14, color: this.state.activeButton === 20 ? 'white' : 'black' }]}>20</Text>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={()=>{this.setState({activeButton: 25}, () => this.checkData(3))}}>
                                                <View style={[styles.circle, {backgroundColor: this.state.activeButton === 25 ? '#21b34f' : 'white', marginRight: 5, marginBottom:5}]}>
                                                    <Text style={[styles.questionText, {fontSize: 14, color: this.state.activeButton === 25 ? 'white' : 'black'}]}>25</Text>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={()=>{this.setState({activeButton: 28}, () => this.checkData(3))}}>
                                                <View style={[styles.circle, {backgroundColor: this.state.activeButton === 28 ? '#21b34f' : 'white', marginRight: 5, marginBottom:5}]}>
                                                    <Text style={[styles.questionText, {fontSize: 14, color: this.state.activeButton === 28 ? 'white' : 'black'}]}>28</Text>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={()=>{this.setState({activeButton: 30}, () => this.checkData(3))}}>
                                                <View style={[styles.circle, {backgroundColor: this.state.activeButton === 30 ? '#21b34f' : 'white', marginRight: 5, marginBottom:5}]}>
                                                    <Text style={[styles.questionText, {fontSize: 14, color: this.state.activeButton === 30 ? 'white' : 'black'}]}>30</Text>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={()=>{this.setState({activeButton: 35}, () => this.checkData(3))}}>
                                                <View style={[styles.circle, {backgroundColor: this.state.activeButton === 35 ? '#21b34f' : 'white', marginRight: 5, marginBottom:5}]}>
                                                    <Text style={[styles.questionText, {fontSize: 14, color: this.state.activeButton === 35 ? 'white' : 'black'}]}>35</Text>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={()=>{this.setState({activeButton: 36}, () => this.checkData(3))}}>
                                                <View style={[styles.circle, {backgroundColor: this.state.activeButton === 36 ? '#21b34f' : 'white', marginRight: 5, marginBottom:5}]}>
                                                    <Text style={[styles.questionText, {fontSize: 14, color: this.state.activeButton === 36 ? 'white' : 'black'}]}>36</Text>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={()=>{this.setState({activeButton: 40}, () => this.checkData(3))}}>
                                                <View style={[styles.circle, {backgroundColor: this.state.activeButton === 40 ? '#21b34f' : 'white', marginRight: 5, marginBottom:5}]}>
                                                    <Text style={[styles.questionText, {fontSize: 14, color: this.state.activeButton === 40 ? 'white' : 'black'}]}>40</Text>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={()=>{this.setState({activeButton: 45}, () => this.checkData(3))}}>
                                                <View style={[styles.circle, {backgroundColor: this.state.activeButton === 45 ? '#21b34f' : 'white', marginRight: 5, marginBottom:5}]}>
                                                    <Text style={[styles.questionText, {fontSize: 14, color: this.state.activeButton === 45 ? 'white' : 'black'}]}>45</Text>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={()=>{this.setState({activeButton: 50}, () => this.checkData(3))}}>
                                                <View style={[styles.circle, {backgroundColor: this.state.activeButton === 50 ? '#21b34f' : 'white', marginRight: 5, marginBottom:5}]}>
                                                    <Text style={[styles.questionText, {fontSize: 14, color: this.state.activeButton === 50 ? 'white' : 'black'}]}>50</Text>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={()=>{this.setState({activeButton: 100}, () => this.checkData(3))}}>
                                                <View style={[styles.circle, {backgroundColor: this.state.activeButton === 100 ? '#21b34f' : 'white', marginRight: 5, marginBottom:5}]}>
                                                    <Text style={[styles.questionText, {fontSize: 14, color: this.state.activeButton === 100 ? 'white' : 'black'}]}>100</Text>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={()=>{this.setState({activeButton: 120}, () => this.checkData(3))}}>
                                                <View style={[styles.circle, {backgroundColor: this.state.activeButton === 120 ? '#21b34f' : 'white', marginRight: 5, marginBottom:5}]}>
                                                    <Text style={[styles.questionText, {fontSize: 14, color: this.state.activeButton === 120 ? 'white' : 'black'}]}>120</Text>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={()=>{this.setState({activeButton: 180}, () => this.checkData(3))}}>
                                                <View style={[styles.circle, {backgroundColor: this.state.activeButton === 180 ? '#21b34f' : 'white', marginRight: 5, marginBottom:5}]}>
                                                    <Text style={[styles.questionText, {fontSize: 14, color: this.state.activeButton === 180 ? 'white' : 'black'}]}>180</Text>
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                    {/*<TouchableOpacity style={styles.filter} onPress={() =>  this.setState({show: !this.state.show})}>*/}
                                    {/*<FIcon name="filter" size={18} color="#21b34f" />*/}
                                    {/*<Text style={styles.filterTxt}>فیلتر</Text>*/}
                                    {/*</TouchableOpacity>*/}
                                    {/*{*/}
                                    {/*this.state.show?*/}
                                    {/**/}
                                    {/*: null*/}
                                    {/*}*/}
                                </View>
                                : null
                        }
                        {/*<Text style={styles.text}>مشاهده بر اساس</Text>*/}
                        {/*<View style={styles2.seeContainer}>*/}
                        {/*<CustomPicker*/}
                        {/*placeholder={'انتخاب کنید'}*/}
                        {/*options={items}*/}
                        {/*getLabel={item => item.label}*/}
                        {/*fieldTemplate={this.renderField.bind(this)}*/}
                        {/*optionTemplate={this.renderOption.bind(this)}*/}
                        {/*// headerTemplate={this.renderHeader}*/}
                        {/*// footerTemplate={this.renderFooter}*/}
                        {/*onValueChange={itemValue => {*/}
                        {/*this.setState({*/}
                        {/*filter: itemValue*/}
                        {/*}, () => {*/}
                        {/*this.orderInsurance();*/}
                        {/*})*/}
                        {/*}}*/}
                        {/*/>*/}
                        {/*</View>*/}
                        {
                            this.state.newInsurances.length !== 0 ? this.state.newInsurances.map((item, index) => <InsuranceInfo item={item} key={index} handleNext={(price, id, instalment, activeBuy) => this.handleNext(price, id, instalment, activeBuy)} insurType={this.props.insurType} buyId={this.state.buyId} />) :
                                this.props.bime && this.props.bime.map((item, index) => <InsuranceInfo item={item} key={index} handleNext={(price, id, instalment, activeBuy) => this.handleNext(price, id, instalment, activeBuy)} insurType={this.props.insurType} buyId={this.state.buyId} />)
                        }
                        <AlertView
                            closeModal={(title) => this.closeModal(title)}
                            modalVisible={this.state.modalVisible}
                            onChange={() => this.setState({modalVisible: false})}

                            title={this.state.signed ? 'لطفا برای مشاهده این بخش ابتدا ثبت نام کنید' : 'مشکلی در برقراری ارتباط با سرور به وجود آمده لطفا دوباره سعی کنید'}
                        />
                    </View>
                </ScrollView>
                <View style={styles.footer}>
                    <TouchableOpacity onPress={() => this.nextStep()} style={[styles.iconLeftContainer, {backgroundColor: this.state.buy ? 'rgba(255, 193, 39, 1)' : 'rgba(200, 200, 200, 1)' }]}>
                        <FEIcon name="arrow-left" size={18} color="white" style={{borderColor: 'white', borderWidth: 1, borderRadius: 20, padding: 3, marginRight: 5}} />
                        <Text style={styles.label}>بعدی</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.iconRightContainer} onPress={() => this.onBackPress()}>
                        <FEIcon name="arrow-right" size={18} color="#21b34f" />
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}
const styles2 = StyleSheet.create({
    container: {
        height: 40,
        width: '100%'
    },
    seeContainer: {
        height: 40,
        backgroundColor: 'white',
        width: '100%',
        borderColor: 'lightgray',
        borderWidth: 1,
        elevation: 4,
        borderRadius: 10,
        paddingRight: 15,
        marginBottom: 30
    },
    innerContainer: {
        flexDirection: 'row',
        alignItems: 'stretch'
    },
    text: {
        fontSize: 18
    },
    headerFooterContainer: {
        padding: 10,
        alignItems: 'center'
    },
    clearButton: { backgroundColor: 'grey', borderRadius: 5, marginRight: 10, padding: 5 },
    optionContainer: {
        padding: 10,
        borderBottomColor: 'grey',
        borderBottomWidth: 1,
        borderRadius: 10,
        alignItems: 'flex-end',
        justifyContent: 'flex-end'
    },
    optionInnerContainer: {
        flex: 1,
        flexDirection: 'row'
    },
    box: {
        width: 20,
        height: 20,
        marginRight: 10
    }
})
function mapStateToProps(state) {
    return {
        user: state.auth.user,
    }
}
export default connect(mapStateToProps)(Prices);