
import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        position: 'relative',
        zIndex: 1,
        backgroundColor: 'rgb(246, 246, 246)'
    },
    header: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: 'white',
        height: 120,
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 9990
    },
    top: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: 'white',
        height: 120,
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 9990,
        borderBottomColor: 'lightgray',
        borderBottomWidth: 1,
        paddingRight: 15,
        paddingLeft: 15
    },
    iconContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    headerTitleContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    headerTitle: {
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 16,
        paddingRight: '40%'
    },
    bodyContainer: {
        paddingBottom: 180,
        paddingRight: 10,
        paddingLeft: 10,
        paddingTop: 30,
        alignItems: 'flex-end',
        justifyContent: 'center',
        // position: 'relative',
        // zIndex: 0


    },
    scroll: {
        paddingTop: 120,
        paddingRight: 15,
        paddingLeft: 15,

    },
    filter:{
        width: 70,
        height: 45,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 15,
        backgroundColor: 'white',
        elevation: 5,
        alignSelf: 'flex-end'
    },
    filterTxt: {
        fontSize: 13,
        color: 'gray',
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    buttonTitle: {
        fontSize: 15,
        color: 'lightgray',
        fontFamily: 'IRANSansMobile(FaNum)',
    },
    text: {
        fontSize: 16,
        color: 'rgba(122, 130, 153, 1)',
        paddingTop: 15,
        paddingBottom: 15,
        fontFamily: 'IRANSansMobile(FaNum)',
    },
    picker: {
        height: 40,
        backgroundColor: 'white',
        width: '100%',
        color: 'gray',
        borderColor: 'lightgray',
        borderWidth: 1,
        elevation: 4,
        borderRadius: 10,
        marginBottom: 20,
        // overflow: 'hidden'
    },
    questionTextContainer: {
        width: '65%'
    },
    questionText: {
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 12

    },
    questionSubText: {
        color: 'gray',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 13
    },
    numberOfMeals: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        flexWrap: 'wrap',
        paddingBottom: 25
    },
    circle: {
        width: 45,
        height: 45,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 45,
        backgroundColor: 'white',
        elevation: 2,
        marginBottom: 5,
    },
    vacleContainer: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    vacle: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingRight: 15,
        paddingLeft: 15,
        paddingTop: 10,
        paddingBottom: 10,
        borderRadius: 10,

    },
    // Image: {
    //     tintColor: 'rgb(15, 76, 183)'
    // },
    vacleText: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 13,
        paddingRight: 10
    },
    footer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        // height: 120,
        position: 'absolute',
        bottom: 30,
        right: 0,
        left: 0,
        zIndex: 9999,
        padding: 15
    },
    iconRightContainer: {
        backgroundColor: 'white',
        borderRadius: 30,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 15,
        paddingRight: 10,
        paddingLeft: 10,
        elevation: 4,
        width: 40,
        height: 40,
    },
    iconLeftContainer: {
        backgroundColor: 'rgba(200, 200, 200, 1)',
        // width: '40%',
        // height: 32,
        borderRadius: 30,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        padding: 5,

    },
    label: {
        paddingLeft: 15,
        paddingRight: 10,
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 15,
    },


});
