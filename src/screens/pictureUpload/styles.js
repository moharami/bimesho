
import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        position: 'relative',
        zIndex: 1,
        backgroundColor: 'rgb(246, 246, 246)',
        // backgroundColor: 'red'
    },
    header: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: 'white',
        height: 120,
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 9990
    },
    bodyContainer: {
        paddingBottom: 180,
        paddingRight: 10,
        paddingLeft: 10,
        paddingTop: 30
    },
    footer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        // height: 120,
        position: 'absolute',
        bottom: 30,
        right: 0,
        left: 0,
        zIndex: 9999,
        padding: 15
    },
    iconLeftContainer: {
        backgroundColor: 'rgba(200, 200, 200, 1)',
        // width: '40%',
        // height: 32,
        borderRadius: 30,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        padding: 5,

    },
    label: {
        paddingLeft: 15,
        paddingRight: 10,
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 15,
    },
    iconRightContainer: {
        backgroundColor: 'white',
        borderRadius: 30,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 15,
        paddingRight: 10,
        paddingLeft: 10,
        elevation: 4,
        width: 40,
        height: 40,
    },
    scroll: {
        paddingTop: 120,
        paddingRight: 15,
        paddingLeft: 15,
    },
    topLabel: {
        paddingLeft: 15,
        paddingRight: 10,
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 18,
    },
    labelContainer: {
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'flex-end',
        paddingBottom: 20
    },

});

