
import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        zIndex: 1,
        position: 'relative',
        backgroundColor: 'rgba(245, 246, 250, 1)'
    },
    bodyContainer: {
        paddingBottom: 220,
        // paddingTop: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    scroll: {
        // paddingTop: 90
    },
    label: {
        paddingLeft: 15,
        paddingRight: 10,
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 15,
    },
    catContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'space-between',
        transform: [
            {rotateY: '180deg'},
        ],
        paddingTop: 30
    },
    navContainer: {
        alignItems: "center",
        justifyContent: 'center',
        width: 84,
        paddingBottom: 40
    },
    text: {
        fontFamily: 'IRANSansMobile(FaNum)',
        // paddingBottom: 55,
        // position: 'absolute',
        // bottom: 15

    },
    slideContainers: {
        flexDirection: 'row',
        transform: [
            {rotateY: '180deg'},
        ],
    },
    topLabel: {
        paddingRight: 10,
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 18,
    },
    seeMoreText: {
        paddingLeft: 10,
        color: '#21b34f',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 12,
    },
    labelContainer: {
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'flex-end',
        width: '100%',
        paddingBottom: 20,
    },
    body: {
        // flexDirection: 'row-reverse',
        // flexWrap: 'wrap',
        padding: 10,
        marginBottom: 20,
    },
    subImage: {
        width: '100%',
        height: 270,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 20
    },
    imageText: {
        backgroundColor: "transparent",
        fontSize: 14,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: "white"

    },
    rightContainer: {
        // flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingRight: 3,
        paddingLeft: 3,
        // borderRadius: 20,
        backgroundColor: '#21b34f',
        // overflow: 'hidden'
        borderBottomLeftRadius: 10,
        borderTopRightRadius: 10,
        // overflow: 'hidden',

    },
    imageLabel: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 14,
        color: 'white'
    },
    advRow: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
        marginBottom: 5,
    },
    iconContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 5
    },
    content: {
        backgroundColor: 'white',
        paddingRight: 15,
        paddingLeft: 10,
        paddingBottom: 10,
        width: '100%'

    },
    headerText: {
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'black',
        lineHeight: 20,
        paddingBottom: 10,
        fontSize: 18
    },
    fContainer: {
        flexDirection: 'row',
        alignItems: 'flex-end',
        justifyContent: 'flex-end'
    },
    bodyText: {
        color: 'black',
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    footerText: {
        color: 'rgba(122, 130, 153, 1)',
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)',
        paddingRight: 15
    },
    contentText: {
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'rgba(51, 54, 64, 1)',
        lineHeight: 20,
        paddingBottom: 10,
        paddingTop: 20,
        fontSize: 17
    },
    TrapezoidStyle: {
        flex: 1,
        position: 'absolute',
        top: -70,
        right: 0,
        left: 0,
        zIndex: 100,

        borderTopWidth: 50,
        borderLeftWidth: 0,
        borderBottomWidth: 0,
        borderTopColor: 'transparent',
        borderRightColor: 'rgba(245, 246, 250, 1)',
        borderLeftColor: 'transparent',
        borderBottomColor: 'transparent',
    },
    footer: {
        flex: 1,
        position: 'absolute',
        bottom: 60,
        right: 30,
        left: 30,
        zIndex: 50
    },
    footerMenu: {

        // width: '80%',
        height: 40,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
        borderRadius: 20
    },
    shareBtn: {
        width: 40,
        height: 40,
        borderRadius: 40,
        flex: 1,
        position: 'absolute',
        bottom: 60,
        right: 30,
        zIndex: 50,
        backgroundColor: '#21b34f',
        alignItems: 'center',
        justifyContent: 'center',
    }
});
