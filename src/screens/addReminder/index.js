
import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, Keyboard, KeyboardAvoidingView, AsyncStorage, BackHandler, Alert, Image, TextInput} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import FIcon from 'react-native-vector-icons/dist/Feather';
import {store} from '../../config/store';
import {connect} from 'react-redux';
import Axios from 'axios';
export const url = 'https://bimehsho.com/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import PersianCalendarPicker from 'react-native-persian-calendar-picker';
import moment from 'moment'
import AlertView from '../../components/modalMassage'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import moment_jalaali from 'moment-jalaali'
import ModalFilterPicker from 'react-native-modal-filter-picker'
import Selectbox from 'react-native-selectbox'
import AlertModal from '../../components/alertModal'
import LinearGradient from 'react-native-linear-gradient'

class AddReminder extends Component {
    constructor(props){
        super(props);
        this.state = {
            loading:false,
            showPicker:false,
            selectedStartDate:null,
            fname:this.props.user.fname,
            lname:this.props.user.lname,
            province:0,
            provinceTitle: '',
            town:0,
            townTitle: '',
            mobile:this.props.user.mobile,
            mobile2:"",
            timeIn: {key: 0, label: "انتخاب نشده", value: 0},
            insurances: [],
            provinces: [],
            city: [],
            type: {key: 0, label: "انتخاب نشده", value: 0},
            loading2: false,
            modalVisible: false,
            add: false,
            fillAll: false,
            visibleinput2: false,
            visibleinput3: false,
            fillFname: false,
            fillLname: false,
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    closeModal() {
        this.setState({modalVisible: false});
        if(this.state.add){
            Actions.reset('reminders', {openDrawer: this.props.openDrawer});
        }
    }
    onShowinput2 = () => {
        this.setState({ visibleinput2: true });
    }

    onSelectInput2 = (picked) => {
        console.log('picked', picked)
        this.setState({
            province: picked,
            visibleinput2: false
        }, () =>{
            console.log('ppppicked', picked)
            this.state.provinces.map((item) => {
                if(picked === item.id) {
                    this.setState({provinceTitle: item.name}, () => {this.getCat(picked);console.log(this.state.provinceTitle)});
                }
            })
        })
    }
    onCancelinput2 = () => {
        this.setState({
            visibleinput2: false
        })
    }
    onShowinput3 = () => {
        this.setState({ visibleinput3: true });
    }

    onSelectInput3 = (picked) => {
        console.log('picked', picked)
        this.setState({
            town: picked,
            visibleinput3: false
        }, () =>{
            console.log('ppppicked', picked)
            this.state.city.map((item) => {
                if(picked === item.id) {
                    this.setState({townTitle: item.name}, () => {console.log(this.state.townTitle)});
                }
            })
        })
    }
    onCancelinput3 = () => {
        this.setState({
            visibleinput3: false
        })
    }
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
        Axios.get('/request/get_insurance').then(response => {
            this.setState({insurances: response.data.data});
            Axios.post('/request/get-province', {}).then(response => {
                this.setState({loading: false, provinces: response.data.data});
            })
                .catch((error) => {
                    // Alert.alert('','خطا')
                    // this.setState({loading: false});
                    this.setState({modalVisible: true, loading: false});
                    console.log(error);
                });
        })
            .catch((error) => {
                // Alert.alert('','خطا')
                // this.setState({loading: false});
                this.setState({modalVisible: true, loading: false});
                console.log(error);
            });
    }
    onDateChange(date) {
        setTimeout(() => {this.setState({showPicker: false})}, 100);
        this.setState({ selectedStartDate: date });
    }
    getCat(id) {
        this.setState({loading2:true});
        Axios.post('/request/get-city', {id: id}).then(response => {
            this.setState({loading2: false, city: response.data.data});
            console.log('kind cars', response.data.data);
        })
            .catch((error) =>{
                // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                // this.setState({loading2: false});
                this.setState({modalVisible: true, loading: false});

            });
    }
    addRemind() {
        this.setState({loading: true});
        AsyncStorage.getItem('token').then((info) => {
            const newInfo = JSON.parse(info);
            Axios.post('/request/remember/add', {
                user_id: newInfo.user_id,
                insurance_id: this.state.type.value,
                fname: this.state.fname,
                lname: this.state.lname,
                mobile: this.state.mobile,
                province: this.state.province,
                city: this.state.town,
                reminder: this.state.timeIn.value,
                expired_date: this.state.selectedStartDate
            }).then(response => {
                this.setState({modalVisible: true, loading: false, add: true});
                // Alert.alert('','یادآوری با موفقیت افزوده شد');
                // Actions.reminders();
            })
                .catch((error) =>{
                    // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                    // this.setState({loading: false});
                    this.setState({modalVisible: true, loading: false});

                });
        });
    }
    test() {
        if( this.state.province !== 0 && this.state.town !== 0  && this.state.timeIn.value !== 0 && this.state.type.value !== 0  && this.state.selectedStartDate !== null && this.state.mobile !== '') {
            this.addRemind();
        }
        else if(/^[-]?\d+$/.test(this.state.fname) === true) {
            this.setState({modalVisible: true, fillFname: true});
        }
        else if(/^[-]?\d+$/.test(this.state.lname) === true) {
            this.setState({modalVisible: true, fillLname: true});
        }
        else {
            // Alert.alert('','لطفا تمام موارد را پر نمایید');
            this.setState({modalVisible: true, fillAll: true});
        }
    }
    onChange(){
        this.setState({showPicker: false});
    }
    onDateChange(date) {
        setTimeout(() => {this.setState({showPicker: false})}, 200)
        this.setState({ selectedStartDate: date });
    }
    closeModal2() {
        this.setState({showPicker: false});
    }
    render() {
        const {user} = this.props;
        let Items = [{key: 0, label: "انتخاب نشده", value: 0}];
        this.state.insurances.map((item) => {Items.push({key: item.id, label: item.title, value: item.id})})
        console.log('items', Items)

        // const {posts, categories, user} = this.props;
        if(this.state.loading){
            return (<Loader />)
        }
        else
            return (
                <KeyboardAvoidingView behavior="padding" style={styles.container}>
                    <LinearGradient  start={{x: 0, y: 1}} end={{x: 1, y: 1}} colors={['#21b34f', '#95cc3a']} style={styles.linearcontainer}>
                        <View style={styles.header}>
                            <TouchableOpacity onPress={() => this.onBackPress()}>
                                <FIcon name="arrow-left" size={20} color="white" style={{paddingLeft: 10, paddingRight: '34%'}} />
                            </TouchableOpacity>
                            <Text style={styles.headerTitle}>افزودن یادآوری</Text>
                        </View>
                    </LinearGradient>
                    <ScrollView style={styles.scroll}>
                        <View style={styles.bodyContainer}>
                            <View style={{width: '100%', zIndex: 0, paddingBottom: 70}}>
                                <Text style={styles.text}>نام</Text>
                                {/*<View style={{position: 'relative', zIndex: 3, width: '100%', backgroundColor: 'yellow'}}>*/}
                                <View style={{position: 'relative', zIndex: 3, width: '100%', height: 42, borderRadius: 10, backgroundColor: 'white',  borderColor: this.state.fname !== '' ? 'rgb(50, 197, 117)' : '#ccc', borderWidth: 1}}>
                                    <TextInput
                                        placeholder="نام"
                                        placeholderTextColor={'gray'}
                                        underlineColorAndroid='transparent'
                                        value={this.state.fname}
                                        maxLength={12}
                                        style={{
                                            height: 40,
                                            paddingRight: 15,
                                            width: '100%',
                                            color: 'gray',
                                            fontSize: 14,
                                            textAlign: 'right'
                                        }}
                                        // onChangeText={(text) => this.setState({text: text, fill: ++this.state.fill})}
                                        onChangeText={(text) => {this.setState({fname: text})}}
                                    />
                                </View>
                                <Text style={styles.text}>نام خانوادگی</Text>
                                <View style={{position: 'relative', zIndex: 3, width: '100%', height: 42,  borderRadius: 10, backgroundColor: 'white',  borderColor: this.state.lname !== '' ? 'rgb(50, 197, 117)' : '#ccc', borderWidth: 1}}>
                                    <TextInput
                                        placeholder="نام خانوادگی"
                                        placeholderTextColor={'gray'}
                                        underlineColorAndroid='transparent'
                                        value={this.state.lname}
                                        maxLength={16}
                                        style={{
                                            height: 40,
                                            paddingRight: 15,
                                            width: '100%',
                                            color: 'gray',
                                            fontSize: 14,
                                            textAlign: 'right',
                                            // elevation: 4
                                        }}
                                        onChangeText={(text) => {this.setState({lname: text})}}
                                    />
                                </View>
                                <Text style={styles.text}>تاریخ انقضا</Text>
                                <TouchableOpacity onPress={() => this.setState({showPicker: true})} style={{position: 'relative',  zIndex: 3, width: '100%', backgroundColor: 'white', height: 42,  borderRadius: 10,  borderColor: this.state.selectedStartDate !== null ? 'rgb(50, 197, 117)' : '#ccc', borderWidth: 1}}>
                                    <Text style={{textAlign: 'right', paddingTop: '3%', paddingRight: 10}}>{this.state.selectedStartDate !==null ? moment_jalaali(this.state.selectedStartDate).format('jYYYY/jM/jD') : 'انتخاب کنید'}</Text>
                                    <Icon name="calendar-o" size={20} color="#21b34f" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>
                                </TouchableOpacity>
                                <Text style={styles.text}>استان</Text>
                                <TouchableOpacity onPress={this.onShowinput2} style={{position: 'relative',  zIndex: 3, width: '100%', backgroundColor: 'white', height: 42, borderColor: this.state.province !== 0 ? 'rgb(50, 197, 117)' : '#ccc', borderWidth: 1, borderRadius: 10}}>
                                    <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>
                                    <Text style={{paddingTop: 5, paddingRight: 10,  fontFamily: 'IRANSansMobile(FaNum)', color: 'gray' }}>{this.state.loading ? "در حال بارگذاری..." : this.state.province === 0 ? "انتخاب نشده" : this.state.provinceTitle}</Text>
                                    <ModalFilterPicker
                                        visible={this.state.visibleinput2}
                                        onSelect={this.onSelectInput2}
                                        onCancel={this.onCancelinput2}
                                        options={ this.state.provinces.map((item) => {return {key: item.id, label: item.name, value: item.id}})}
                                        placeholderText="جستجو ..."
                                        cancelButtonText="لغو"
                                        filterTextInputStyle={{textAlign: 'right'}}
                                        optionTextStyle={{textAlign: 'right', width: '100%'}}
                                        titleTextStyle={{textAlign: 'right'}}
                                    />
                                </TouchableOpacity>

                                <Text style={styles.text}>شهر</Text>
                                <TouchableOpacity onPress={this.onShowinput3} style={{position: 'relative',  zIndex: 3, width: '100%', backgroundColor: 'white', height: 42, borderColor: this.state.town !== 0 ? 'rgb(50, 197, 117)' : '#ccc', borderWidth: 1, borderRadius: 10}}>
                                    <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>
                                    <Text style={{paddingTop: 5, paddingRight: 10,  fontFamily: 'IRANSansMobile(FaNum)', color: 'gray' }}>{this.state.loading2 ? "در حال بارگذاری..." : this.state.town === 0 ? "انتخاب نشده" : this.state.townTitle}</Text>
                                    <ModalFilterPicker
                                        visible={this.state.visibleinput3}
                                        onSelect={this.onSelectInput3}
                                        onCancel={this.onCancelinput3}
                                        options={ this.state.city.map((item) => {return {key: item.id, label: item.name, value: item.id}})}
                                        placeholderText="جستجو ..."
                                        cancelButtonText="لغو"
                                        filterTextInputStyle={{textAlign: 'right'}}
                                        optionTextStyle={{textAlign: 'right', width: '100%'}}
                                        titleTextStyle={{textAlign: 'right'}}
                                    />
                                </TouchableOpacity>

                                <Text style={styles.text}>موبایل</Text>
                                <View style={{position: 'relative', zIndex: 3, width: '100%', height: 42, borderRadius: 10, backgroundColor: 'white',  borderColor: this.state.mobile !== '' ? 'rgb(50, 197, 117)' : '#ccc', borderWidth: 1}}>
                                    <TextInput
                                        keyboardType={"numeric"}
                                        placeholder="موبایل"
                                        placeholderTextColor={'gray'}
                                        underlineColorAndroid='transparent'
                                        value={this.state.mobile}
                                        maxLength={11}
                                        style={{
                                            height: 40,
                                            paddingRight: 15,
                                            width: '100%',
                                            color: 'gray',
                                            fontSize: 14,
                                            textAlign: 'right',
                                            // elevation: 4
                                        }}
                                        onChangeText={(text) => {this.setState({mobile: text})}}
                                    />
                                </View>

                                <Text style={styles.text}>موبایل</Text>
                                <View style={{position: 'relative', zIndex: 3, width: '100%', height: 42, borderRadius: 10, backgroundColor: 'white',   borderColor: this.state.mobile2 !== '' ? 'rgb(50, 197, 117)' : '#ccc', borderWidth: 1}}>
                                    <TextInput
                                        keyboardType={"numeric"}
                                        placeholder="شماره موبایل دوم(اختیاری)"
                                        placeholderTextColor={'gray'}
                                        underlineColorAndroid='transparent'
                                        value={this.state.mobile2}
                                        maxLength={11}
                                        style={{
                                            height: 40,
                                            paddingRight: 15,
                                            width: '100%',
                                            color: 'gray',
                                            fontSize: 14,
                                            textAlign: 'right',
                                            // elevation: 4
                                        }}
                                        onChangeText={(text) => {this.setState({mobile2: text})}}
                                    />
                                </View>
                                <Text style={styles.text}>نوع بیمه</Text>


                                <View style={{position: 'relative',  zIndex: 3, width: '100%', backgroundColor: 'white', height: 42, borderRadius: 10,  borderColor: this.state.type.value !== 0 ? 'rgb(50, 197, 117)' : '#ccc', borderWidth: 1}}>
                                    {/*<FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>*/}
                                    <Selectbox
                                        style={{height: 42, paddingTop: '3%', paddingRight: 10}}
                                        selectLabelStyle={{textAlign: 'right', color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13, paddingTop: 0}}
                                        optionLabelStyle={{textAlign: 'center', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                        selectedItem={this.state.type}
                                        cancelLabel="لغو"
                                        onChange={(itemValue) =>{
                                            this.setState({
                                                type: itemValue
                                            }, () => {
                                                console.log('item value', itemValue)
                                            })
                                        }}
                                        items={Items} />
                                </View>
                                <Text style={styles.text}>زمان یادآوری</Text>

                                <View style={{position: 'relative',  zIndex: 3, width: '100%', backgroundColor: 'white', height: 42, borderRadius: 10,  borderColor: this.state.timeIn.value !== 0 ? 'rgb(50, 197, 117)' : '#ccc', borderWidth: 1}}>
                                    {/*<FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>*/}
                                    <Selectbox
                                        style={{height: 42, paddingTop: '3%', paddingRight: 10}}
                                        selectLabelStyle={{textAlign: 'right', color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13, paddingTop: 0}}
                                        optionLabelStyle={{textAlign: 'center', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                        selectedItem={this.state.timeIn}
                                        cancelLabel="لغو"
                                        onChange={(itemValue) =>{
                                            this.setState({
                                                timeIn: itemValue
                                            })
                                        }}
                                        items={[
                                            {key: 0, label: "انتخاب نشده", value: 0},
                                            {key: 1, label: "ماهیانه", value: 1},
                                            {key: 2, label: "سه ماهه", value: 3},
                                            {key: 3, label: "شش ماهه", value: 6},
                                            {key: 4, label: "سالیانه", value: 12}
                                        ]} />
                                </View>
                                <View style={{alignItems: 'center', justifyContent: 'center', paddingTop: 30}}>
                                    <TouchableOpacity onPress={() => this.test()} style={styles.advertise}>
                                        <Text style={styles.buttonTitle}>افزودن</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            {/*{*/}
                            {/*this.state.showPicker ?*/}
                            {/*<View style={{*/}
                            {/*position: 'absolute',*/}
                            {/*bottom: '70%',*/}
                            {/*zIndex: 9999,*/}
                            {/*backgroundColor: 'white'*/}
                            {/*}}>*/}
                            {/*<PersianCalendarPicker*/}
                            {/*onDateChange={(date) => this.onDateChange(date)}*/}
                            {/*/>*/}
                            {/*</View>*/}
                            {/*: null*/}
                            {/*}*/}
                            <AlertModal
                                closeModal={(title) => this.closeModal2(title)}
                                modalVisible={this.state.showPicker}
                                onDateChange={(date) => this.onDateChange(date)}
                                onChange={()=> this.onChange()}
                            />
                            <AlertView
                                closeModal={(title) => this.closeModal(title)}
                                modalVisible={this.state.modalVisible}
                                onChange={() => this.setState({modalVisible: false})}
                                title={this.state.add ? 'یادآوری با موفقیت افزوده شد' :(this.state.fillAll ? 'لطفا تمام موارد را پر نمایید' : (this.state.fillFname ? 'نام نمی تواند مقدار عددی داشته باشد' : (this.state.fillLname ? 'نام خانوادگی نمیتواند مقدار عددی داشته باشد' : 'مشکلی در برقراری ارتباط با سرور به وجود آمده لطفا دوباره سعی کنید' )) )}
                            />
                        </View>
                    </ScrollView>
                </KeyboardAvoidingView>
            );
    }
}
function mapStateToProps(state) {
    return {
        user: state.auth.user,
    }
}
export default connect(mapStateToProps)(AddReminder);

