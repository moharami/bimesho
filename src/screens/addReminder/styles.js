/**
 * Created by n.moharami on 10/17/2018.
 */

import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        position: 'relative',
        zIndex: 1,
        backgroundColor: 'rgb(246, 246, 246)'
    },
    header: {
        flex: 1,
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        // backgroundColor: 'rgb(61, 105, 223)',
        height: 60,
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 9990
    },
    linearcontainer: {
        flex: 1,
        height: 60,
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 9999,
        borderBottomColor: 'lightgray',
        borderBottomWidth: 1,
        paddingRight: 15,
        paddingLeft: 15
    },
    iconContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    headerTitleContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    headerTitle: {
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 16,
    },
    bodyContainer: {
        paddingBottom: 170,
        borderWidth: 1,
        borderColor: 'rgb(237, 237, 237)',
        paddingRight: 10,
        paddingLeft: 10,
        paddingTop: 20,
        alignItems: 'flex-end',
        justifyContent: 'center'
    },
    scroll: {
        paddingTop: 60,
        paddingBottom: 120,
        paddingRight: 15,
        paddingLeft: 15,
    },
    text: {
        fontSize: 14,
        color: 'rgba(122, 130, 153, 1)',
        paddingTop: 15,
        paddingBottom: 15,
        fontFamily: 'IRANSansMobile(FaNum)',
    },
    picker: {
        height: 40,
        backgroundColor: 'white',
        width: '100%',
        color: 'gray',
        borderColor: 'lightgray',
        borderWidth: 1,
        // elevation: 4,
        borderRadius: 10,
        marginBottom: 20,
        // overflow: 'hidden'
    },
    telpicker: {
        height: 40,
        backgroundColor: 'white',
        width: '25%',
        color: 'gray',
        borderColor: 'lightgray',
        borderWidth: 1,
        // elevation: 4,
        borderRadius: 10,
    },
    questionTextContainer: {
        width: '65%'
    },
    questionText: {
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 12

    },
    questionSubText: {
        color: 'gray',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 13
    },
    numberOfMeals: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        // paddingBottom: 40
    },
    circle: {
        width: 45,
        height: 45,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 45,
        backgroundColor: 'white',
        elevation: 2
    },
    vacleContainer: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    vacle: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingRight: 15,
        paddingLeft: 15,
        paddingTop: 10,
        paddingBottom: 10,
        borderRadius: 10,

    },
    // Image: {
    //     tintColor: 'rgb(15, 76, 183)'
    // },
    vacleText: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 13,
        paddingRight: 10
    },
    label: {
        paddingLeft: 15,
        paddingRight: 10,
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 15,
    },
    topLabel: {
        paddingLeft: 15,
        paddingRight: 10,
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 18,
    },
    labelContainer: {
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'flex-end',
        paddingBottom: 20
    },
    advertise: {
        width: '65%',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 30,
        backgroundColor: 'rgba(255, 193, 39, 1)',
        padding: 7,
    },
    buttonTitle: {
        fontSize: 15,
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)',
    },

});
