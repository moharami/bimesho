import React, {Component} from 'react';
import {View, TouchableOpacity, Text, TextInput, Image,BackHandler, AsyncStorage, KeyboardAvoidingView, NetInfo} from 'react-native';
import styles from './styles'
import Icon from 'react-native-vector-icons/FontAwesome';
import {Actions} from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient';
import Axios from 'axios'
export const url = 'https://bimehsho.com/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import AlertView from '../../components/modalMassage'
import moment_jalaali from 'moment-jalaali'
import {store} from '../../config/store';

class Login extends Component {
    constructor(props) {
        super(props);
        this.backCount = 0;
        this.state = {
            text: '',
            login: true,
            loading: false,
            mobileCorrect: false,
            modalVisible: false,
            correct: false,
            isConnected: false,
            wifi: false,
            username: '',
            password: ''
        };
        // this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        // console.log('some thing unmount')
        BackHandler.removeEventListener("hardwareBackPress",  this.onBackPress);
        // BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        // console.log('some thing loged')
        // BackHandler.exitApp();
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    // componentWillUpdate(){
    //     BackHandler.addEventListener("hardwareBackPress",  this.onBackPress.bind(this));
    // }
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress",  this.onBackPress.bind(this));
        // console.log('herer is login')
        // NetInfo.getConnectionInfo().then((connectionInfo) => {
        //     this.setState({isConnected: connectionInfo.type !== 'none'}, ()=> {
        //         if(this.state.isConnected === false){
        //             this.setState({modalVisible: true, loading: false, wifi: true})
        //         }
        //     })
        // });
        // AsyncStorage.removeItem('token')
        // try {
        //     AsyncStorage.getItem('token').then((info) => {
        //         if(info !== null) {
        //             const newInfo = JSON.parse(info);
        //             // Axios.defaults.headers.common['Authorization'] = 'Bearer ' + newInfo.token;
        //             console.log('comment token', newInfo.token);
        //             const expiresTime = newInfo.expires_at;
        //             const currentTime = new Date().getTime()/1000;
        //             if(expiresTime> currentTime ){
        //                 // Actions.home({openDrawer: this.props.openDrawer, loged: true})
        //                 Actions.insuranceBuy({openDrawer: this.props.openDrawer})
        //                 this.setState({loading: false});
        //             }
        //             else {
        //                 Actions.login({openDrawer: this.props.openDrawer})
        //                 this.setState({loading: false});
        //                 // this.setState({info: true});
        //             }
        //         }
        //         else{
        //             this.setState({loading: false});
        //         }
        //     });
        // }
        // catch (error) {
        //     console.log(error)
        // }
    }
    closeModal() {
        this.setState({modalVisible: false});
    }
    onLogin(){
        console.log(this.state.text)
        // console.log(/‎09[123]\d{8}/g.test('09125555555'))
        // if (reg.test("09124180623") === true){
        //     alert('true')
        //     this.setState({mobileCorrect: true})
        // }
        if(this.state.text === '' && this.state.username === '' && this.state.password === '' || (this.state.username !== '' && this.state.password === '') || ((this.state.username === '' && this.state.password !== ''))) {
            this.setState({ correct: true, modalVisible: true});
        }
        else if(this.state.text !== '') {
            this.setState({loading: true});
            Axios.post('/send_login', {
                mobile: this.state.text
            }).then(response=> {
                this.setState({loading: false});
                console.log('send mobile', response.data);
                if(response.data.msg === 'CodeSendSuccess'){
                    this.setState({
                        loading: false
                    }, () => {
                        console.log('pppppprofile true in login', this.props.profile)
                        if (this.props.profile ===  true) {
                            Actions.confirmation({mobile: this.state.text, profile: true});
                        }
                        else {
                            Actions.confirmation({mobile: this.state.text, insBuy: true, pageTitle:'مشخصات خریدار', factor:this.props.factor, user_details:this.props.user_details, insurType:this.props.insurType, instalment: this.props.instalment});
                        }
                    })
                }
                else {
                    Actions.login({openDrawer: this.props.openDrawer, profile: true})
                }
            })
                .catch((response) => {
                    console.log('response error', response.response)
                    if(response.response.data.msg === 'NotRegister'){
                        Axios.post('/send_register', {
                            mobile: this.state.text
                        }).then(response=> {
                            if(response.data.msg === 'Register'){
                                console.log('register success', response.data);
                                this.setState({
                                    loading: false
                                }, () => {
                                    if (this.props.profile ===  true) {
                                        Actions.confirmation({mobile: this.state.text, profile: true});
                                    }
                                    else {
                                        Actions.confirmation({mobile: this.state.text, insBuy: true, pageTitle:'مشخصات خریدار', factor:this.props.factor, user_details:this.props.user_details, insurType:this.props.insurType, instalment: this.props.instalment});
                                    }

                                    // Actions.confirmation({mobile: this.state.text, insBuy: true, pageTitle:'مشخصات خریدار', factor:this.props.factor, user_details:this.props.user_details, insurType:this.props.insurType, instalment: this.props.instalment});
                                })
                            }
                            if(response.data.msg === 'BeforeRegister'){
                                this.setState({loading: false})
                            }
                        })
                            .catch((error) => {
                                // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                                // this.setState({loading: false});
                                this.setState({modalVisible: true, loading: false});

                            });
                    }
                    else if(response.response.data.msg === 'ErorrInput' || response.response.data.msg === 'CodeSendError'){
                        this.setState({
                            loading: false
                        }, () => {
                            // Alert.alert('','لطفا شماره موبایل خود را صحیح وارد کنید');
                            // this.setState({correct: true, loading: false});
                            this.setState({correct: true, modalVisible: true, loading: false});

                            // Actions.login({mobile: this.state.text});
                        })
                    }
                    else {
                        // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                        // this.setState({loading: false});
                        this.setState({modalVisible: true, loading: false});
                    }
                });
        }
        else if(this.state.username !== '' && this.state.password !== ''){
            this.setState({loading: true});
            Axios.post('/user_login', {
                username: this.state.username,
                pass: this.state.password
            }).then(response=> {
                this.setState({loading: false});
                console.log('send mobile', response.data);
                // if(response.data.msg === 'IncurrentPasswordOrUsername'){
                if(response.data.msg === 'LoginSuccesss'){
                    this.setState({
                        loading: false
                    }, () => {
                        console.log('pppppprofile true in login', this.props.profile)

                        store.dispatch({type: 'USER_INFO_FETCHED', payload: response.data.data});
                        store.dispatch({type: 'USER_LOGED', payload: true});
                        console.log('confirm data', response);
                        const info = {'token': response.data.access_token, 'mobile': response.data.data.mobile, 'expires_at': response.data.expires_at, 'user_id': response.data.data.id};
                        const newwwwAsync = AsyncStorage.setItem('token', JSON.stringify(info));
                        const info2 = {'loged': true};
                        const newwwwAsync2 = AsyncStorage.setItem('loged', JSON.stringify(info2));
                        console.log('newwwwAsync2', newwwwAsync2);
                        AsyncStorage.getItem('loged').then((info) => {
                            if(info !== null) {
                                const newInfo = JSON.parse(info);
                                console.log('neeeew iiiiinfologed ', newInfo)
                            }
                        })

                        if (this.props.profile ===  true) {
                            // Actions.confirmation({mobile: this.state.text, profile: true});
                            Actions.profile({openDrawer: this.props.openDrawer, mobile: response.data.data.mobile})
                        }
                        else if(this.props.insBuy) {

                            let newFactor = this.props.factor;
                            let newUserdetails = this.props.user_details;
                            newUserdetails.user_id = response.data.data.id;
                            newUserdetails._token = response.data.access_token;
                            const id = response.data.data.id;

                            if(this.props.insurType === 'body') {
                                newFactor.user_id = id;
                            }
                            if(this.props.insurType === 'Third') {
                                newFactor.user_id = id;
                            }
                            if(this.props.insurType === 'motor') {
                                newFactor.user_id = id;
                            }
                            else if(this.props.insurType === 'fire') {
                                newFactor.user_id = id;
                            }
                            else if(this.props.insurType === 'complete') {
                                newFactor.user_id = id;
                            }
                            else if(this.props.insurType === 'travel') {
                                newFactor.user_id = id;
                                newFactor.insurance.user_id = id;
                                newFactor.dataSelect.insurance.user_id = id;
                            }
                            else if(this.props.insurType === 'responsible') {
                                newFactor.user_id = id;
                            }
                            else if(this.props.insurType === 'life') {
                                newFactor.user_id = id;
                                newFactor.insurance.user_id = id;
                            }

                            // Actions.confirmation({mobile: this.state.text, insBuy: true, pageTitle:'مشخصات خریدار', factor:this.props.factor, user_details:this.props.user_details, insurType:this.props.insurType, instalment: this.props.instalment});
                            Actions.prices({openDrawer: this.props.openDrawer, insBuy: true, pageTitle:'مشخصات خریدار', factor:newFactor, user_details:newUserdetails, insurType:this.props.insurType, instalment: this.props.instalment})
                        }
                    })
                }
                else {
                    this.setState({loading: false})
                }
            })
                .catch((response) => {
                    console.log('response error', response.response)
                    this.setState({modalVisible: true, loading: false});
                });
        }
    }
    render() {
        if(this.state.loading){
            return (<Loader />)
        }
        else return (
            <KeyboardAvoidingView style={styles.container}  behavior="padding" enabled>
                {this.state.loading ? <Loader send={false}/> :
                    <View style={styles.send}>
                        <Image style={{alignSelf: 'center', marginRight: 20, width: '50%', resizeMode: 'contain'}} resizeMode={'contain'}
                               source={require('../../assets/logo.png')}/>
                        <View style={styles.body}>
                            <Text style={styles.header}>خوش آمدید</Text>
                            <Text style={styles.label}>برای بهره مندی از خدمات بیمه شو بیمه به یکی از روش های زیر وارد شوید</Text>
                            <TextInput
                                maxLength={11}
                                placeholder="شماره موبایل"
                                keyboardType='numeric'
                                placeholderTextColor={'#C8C8C8'}
                                underlineColorAndroid='transparent'
                                value={this.state.text}
                                style={{
                                    width: '85%',
                                    textAlign: 'right',
                                    borderWidth: 1,
                                    borderColor: 'gray',
                                    height: 40,
                                    backgroundColor: 'white',
                                    paddingRight: 15,
                                    borderRadius: 10,
                                    fontSize: 14,
                                    color: '#7A8299',
                                    fontFamily: 'IRANSansMobile(FaNum)'
                                }}
                                onChangeText={(text) => this.setState({text})}/>
                            <View style={styles.line}/>
                            <TextInput
                                maxLength={11}
                                placeholder="نام کاربری"
                                placeholderTextColor={'#C8C8C8'}
                                underlineColorAndroid='transparent'
                                value={this.state.username}
                                style={{
                                    width: '85%',
                                    textAlign: 'right',
                                    borderWidth: 1,
                                    borderColor: 'gray',
                                    height: 40,
                                    backgroundColor: 'white',
                                    paddingRight: 15,
                                    borderRadius: 10,
                                    fontSize: 14,
                                    color: '#7A8299',
                                    fontFamily: 'IRANSansMobile(FaNum)',
                                    marginBottom: 15
                                }}
                                onChangeText={(text) => this.setState({username: text})}/>

                            <TextInput
                                maxLength={11}
                                placeholder="کلمه عبور"
                                keyboardType='numeric'
                                placeholderTextColor={'#C8C8C8'}
                                underlineColorAndroid='transparent'
                                secureTextEntry
                                value={this.state.password}
                                style={{
                                    width: '85%',
                                    textAlign: 'right',
                                    borderWidth: 1,
                                    borderColor: 'gray',
                                    height: 40,
                                    backgroundColor: 'white',
                                    paddingRight: 15,
                                    borderRadius: 10,
                                    fontSize: 14,
                                    color: '#7A8299',
                                    fontFamily: 'IRANSansMobile(FaNum)'
                                }}
                                onChangeText={(text) => this.setState({password: text})}/>

                            <TouchableOpacity onPress={() => this.onLogin() } style={styles.advertise}>
                                <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={['#21b34f', '#95cc3a']}
                                                style={styles.advertise}>
                                    <View>
                                        <Text style={styles.buttonTitle}>ورود</Text>
                                    </View>
                                </LinearGradient>
                            </TouchableOpacity>
                        </View>
                    </View>
                }
                <Image style={{height:'20%',zIndex:-1,width:'100%',position:'absolute',bottom:0}} resizeMode={'cover'} source={require('../../assets/company-hero-3.png')}/>
                <AlertView
                    closeModal={(title) => this.closeModal(title)}
                    modalVisible={this.state.modalVisible}
                    onChange={(visible) => this.setState({modalVisible: false})}

                    title={this.state.correct ? 'شماره موبایل یا نام کاربری و پسورد نمی تواند خالی باشد':(this.state.wifi ? 'لطفا وضعیت وصل بودن به اینترنت را بررسی کنید': 'مشکلی در برقراری ارتباط با سرور به وجود آمده لطفا دوباره سعی کنید')}
                />
            </KeyboardAvoidingView>
        );
    }
}

export default Login;