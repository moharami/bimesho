import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-start',
        backgroundColor:'white',
        // paddingTop: 100

    },
    imageContainer: {
        position: 'relative',
        alignItems: 'center',
        justifyContent: 'center',
        top: 60,
        right: 0,
        left: 0
    },
    image: {
        // width: '100%',
        // position: 'absolute',
        // // top: 60,
        // height: '100%',
        // alignItems: 'center',
        // justifyContent: 'center',
        // top: 80,
        // zIndex: 9999,

    },
    body: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        // paddingTop: 50
    },
    header:{
        paddingBottom: 10,
        color: '#333640',
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'right',
        paddingRight: 20,
        alignSelf:'flex-end',
        paddingLeft:10,
        // paddingTop: '23%'
    },
    label: {
        paddingBottom: 10,
        color: '#7A8299',
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'right',
        paddingRight: 20,
        alignSelf:'flex-end',
        paddingLeft:20,
    },
    advertise: {
        borderRadius:6,
        width: '95%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
        padding: 8,
        marginTop: 2,
        marginBottom: 50


    },
    buttonTitle: {
        fontSize: 15,
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    send: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    footer: {
        paddingTop: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    footerText: {
        fontSize: 12,
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    signup: {
        fontSize: 12,
        color: '#21b34f',
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    line: {
        width: '90%',
        borderTopColor: 'gray',
        borderTopWidth: 1,
        margin: 20
    }
});
