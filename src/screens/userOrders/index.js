
import React, {Component} from 'react';
import { View, TouchableOpacity, Text, AsyncStorage,ImageBackground, Dimensions, ScrollView, BackHandler, Alert} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux';
import background from '../../assets/bg.png'
import Axios from 'axios';
export const url = 'https://bimehsho.com/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import Icon from 'react-native-vector-icons/dist/Feather';
import FooterMenu from "../../components/footerMenu/index";
import Order from "../../components/order";
import AlertView from '../../components/modalMassage'
import LinearGradient from 'react-native-linear-gradient'

class UserOrders extends Component {
    constructor(props){
        super(props);
        this.state = {
            text: '',
            login: true,
            loading: true,
            orders: [],
            modalVisible: false,
            showMore: true,
            commentModal: this.props.modalOpen,
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress(){
        if(this.props.profileBack) {
            Actions.pop({refresh: {refresh:Math.random()}});
        }
        else {
            Actions.insuranceBuy({openDrawer: this.props.openDrawer})
        }
        return true;
    };
    componentWillMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
        AsyncStorage.getItem('token').then((info) => {
            if(info !== null) {
                const newInfo = JSON.parse(info);
                Axios.post('/request/orders/list', {
                    user_id: newInfo.user_id
                }).then(response=> {
                    this.setState({loading: false, orders: response.data.data, userId: newInfo.user_id})
                    console.log('oooorders', this.state.orders)
                })
                    .catch((error) => {
                        console.log(error)
                        // Alert.alert('','خطا');
                        // this.setState({loading: false});
                        this.setState({modalVisible: true, loading: false});
                    });
            }
            else {
                this.setState({loading: false});
            }
        });
    }
    // closeModal() {
    //     this.setState({modalVisible: false});
    // }
    // closeModalComment() {
    //     this.setState({commentModal: false});
    // }
    render() {
        if(this.state.loading){
            return (<Loader />)
        }
        else
        return (
            <View style={styles.container}>
                <LinearGradient
                    start={{x: 0, y: 1}} end={{x: 1, y: 1}} colors={['#21b34f', '#95cc3a']}>
                    <View style={styles.image}>
                        <View style={styles.imageRow}>
                            <TouchableOpacity onPress={() => this.onBackPress()}>
                                <Icon name="arrow-left" size={20} color="white" style={{paddingRight: 20}} />
                            </TouchableOpacity>
                            <Text style={styles.label}>سفارش های من</Text>
                            <TouchableOpacity onPress={() => Actions.drawerOpen()}>
                                <Icon name="menu" size={26} color="white" />
                            </TouchableOpacity>
                        </View>
                    </View>
                </LinearGradient>
                <ScrollView style={styles.scroll}>
                    <View style={styles.body}>
                        {
                            this.state.orders.length !== 0 ? this.state.orders.map((item, index) => <Order key={index} item={item} />) :
                                <Text style={{paddingTop: '50%'}}>سفارشی برای نمایش وجود ندارد</Text>
                        }
                        <AlertView
                            closeModal={(title) => this.closeModal(title)}
                            modalVisible={this.state.modalVisible}
                            title='مشکلی در برقراری ارتباط با سرور به وجود آمده لطفا دوباره سعی کنید'
                        />
                        {/*<CommentModal*/}
                            {/*closeModal={() => this.closeModalComment()}*/}
                            {/*modalVisible={this.state.commentModal}*/}
                            {/*onChange={(visible) => this.setState({modalVisible: false})}*/}
                            {/*insId={this.props.insId}*/}
                            {/*insTitle={this.props.insTitle}*/}
                            {/*userId={this.state.userId}*/}
                        {/*/>*/}
                    </View>
                </ScrollView>
                <FooterMenu active="profile" openDrawer={this.props.openDrawer}/>
                {/*<LinearGradient*/}
                    {/*start={{x: 0, y: 1}} end={{x: 1, y: 1}} colors={['#21b34f', '#95cc3a']}>*/}
                    {/*<View style={[styles.TrapezoidStyle, {borderRightWidth: Dimensions.get('window').width}]} />*/}
                {/*</LinearGradient>*/}
            </View>

        );
    }
}
export default UserOrders;