
import React, {Component} from 'react';
import { View, TouchableOpacity, Text, TextInput,ImageBackground, Dimensions, ScrollView, BackHandler, Alert, AsyncStorage} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux';
import background from '../../assets/bg.png'
import Axios from 'axios'
export const url = 'https://bimehsho.com/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import Icon from 'react-native-vector-icons/dist/Feather';
import FooterMenu from "../../components/footerMenu/index";
import WalletAcount from "../../components/walletAcount";
import WalletPeople from "../../components/walletPeople";
import AlertView from '../../components/modalMassage'
import LinearGradient from 'react-native-linear-gradient'

class Marketing extends Component {
    constructor(props){
        super(props);
        this.state = {
            text: '',
            login: true,
            loading: false,
            peoples: [],
            modalVisible: false

        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress(){
        Actions.insuranceBuy({openDrawer: this.props.openDrawer})
        return true;
    };
    componentWillMount() {
        this.setState({loading: true});
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
        AsyncStorage.getItem('token').then((info) => {
            const newInfo = JSON.parse(info);
            Axios.post('/user/request/referrer', {user_id: newInfo.user_id})
                .then(response => {
                    this.setState({loading: false, peoples: response.data.data});
                })
            .catch((error) =>{
                console.log(error)
                this.setState({modalVisible: true, loading: false});
            });
        })
    }
    closeModal() {
        this.setState({modalVisible: false});
    }
    render() {
        if(this.state.loading){
            return (<Loader />)
        }
        else return (
            <View style={styles.container}>
                <LinearGradient start={{x: 0, y: 1}} end={{x: 1, y: 1}} colors={['#21b34f', '#95cc3a']} >
                    <View style={styles.image} source={background}>
                        <View style={styles.imageRow}>
                            <TouchableOpacity onPress={() => this.onBackPress()}>
                                <Icon name="arrow-left" size={20} color="white" style={{paddingRight: 20}} />
                            </TouchableOpacity>
                            <Text style={styles.label}>بازاریابی</Text>
                            <TouchableOpacity onPress={() => Actions.drawerOpen()}>
                                <Icon name="menu" size={26} color="white" />
                            </TouchableOpacity>
                        </View>
                    </View>
                </LinearGradient>
                <ScrollView style={styles.scroll}>
                    <View style={styles.body}>
                        <WalletAcount />
                        <AlertView
                            closeModal={(title) => this.closeModal(title)}
                            modalVisible={this.state.modalVisible}
                            title='مشکلی در برقراری ارتباط با سرور به وجود آمده لطفا دوباره سعی کنید'
                        />
                    </View>
                </ScrollView>
                <FooterMenu active="profile" openDrawer={this.props.openDrawer}/>
                {/*<LinearGradient start={{x: 0, y: 1}} end={{x: 1, y: 1}} colors={['#21b34f', '#95cc3a']}>*/}
                    {/*<View style={[styles.TrapezoidStyle, {borderRightWidth: Dimensions.get('window').width}]} />*/}
                {/*</LinearGradient>*/}
            </View>

        );
    }
}
export default Marketing;