import React, {Component} from 'react';
import {Actions} from 'react-native-router-flux';
import Logo from '../../assets/logo.png'
import {
    Image,
    Dimensions,
    Text,
    View
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import LinearGradient from 'react-native-linear-gradient';

class Splash extends Component {
    constructor(props) {
        super(props);

    }
    componentDidMount() {
        setTimeout( () => {
            this.redirect();
        },3000);
    }
    redirect () {
        Actions.insuranceBuy({openDrawer: this.props.openDrawer});
    }
    render() {
        return (
            <Animatable.View
                animation="fadeOut"
                duration={6000}
                style={{width: '100%' ,flex: 1 }}
                >
            <LinearGradient
                style={{ alignItems: 'center', justifyContent: 'space-between',  height: Dimensions.get('window').height}}
                start={{x: 0, y: 0}} end={{x: 0, y: 1}} colors={['white', 'white', '#1bb150', '#1bb150']}>
                <View style={{alignItems: 'center', justifyContent: 'space-between', paddingTop: '10%'}}>
                    <Image source={Logo} style={{resizeMode: 'contain'}} />
                    <Text style={{fontFamily: 'IRANSansMobile(FaNum)',
                        fontSize: 13, textAlign: 'center',
                        color: '#126976'}}>خرید و تمدید آنلاین بیمه</Text>
                </View>
                <View style={{alignItems: 'center', justifyContent: 'space-between'}}>
                    <Text style={{  fontFamily: 'IRANSansMobile(FaNum)',
                        fontSize: 14, textAlign: 'center', paddingRight: '5%',
                        color: 'white', paddingBottom: '15%'}}>طراحی و توسعه توسط آذرین وب</Text>
                </View>
                {/*<View style={{alignItems: 'center', justifyContent: 'space-between'}}>*/}

                {/*</View>*/}

            </LinearGradient>
            </Animatable.View>


        )
    }
}

export default Splash;