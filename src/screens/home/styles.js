
import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        position: 'relative',
        zIndex: 1,
        backgroundColor: 'rgba(245, 246, 250, 1)'
    },
    header: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        height: 150,
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 9990,
    },
    top: {
        // flex: 1,
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'space-between',
        height: 70,
        paddingRight: 15,
        paddingLeft: 15,
        paddingTop: 20,
        position: 'relative',
        zIndex: 0,
    },
    bodyContainer: {
        paddingBottom: 150,
        paddingTop: 10,
        alignItems: 'flex-end',
        justifyContent: 'center',
    },
    scroll: {
        paddingTop: 150,
        paddingRight: 15,
        paddingLeft: 15,
        marginBottom: 90
    },
    footer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        // height: 120,
        position: 'absolute',
        bottom: 0,
        right: 0,
        left: 0,
        zIndex: 9999,
        padding: 15
    },
    iconRightContainer: {
        backgroundColor: 'white',
        borderRadius: 30,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 15,
        paddingRight: 10,
        paddingLeft: 10,
        elevation: 4,
        width: 40,
        height: 40,
    },
    iconLeftContainer: {
        backgroundColor: 'rgba(200, 200, 200, 1)',
        borderRadius: 30,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        padding: 5,

    },
    label: {
        paddingLeft: 15,
        paddingRight: 10,
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 15,
    },
    catContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'space-between',
        transform: [
            {rotateY: '180deg'},
        ],
        paddingTop: 30
    },
    navContainer: {
        alignItems: "center",
        justifyContent: 'flex-end',
        width: 110,
        paddingBottom: 40,

    },
    text: {
        fontFamily: 'IRANSansMobile(FaNum)',
        position: 'absolute',
        bottom: 15
    },
    slideContainers: {
        flexDirection: 'row',
        transform: [
            {rotateY: '180deg'},
        ],
    },
    topLabel: {
        paddingRight: 10,
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 18,
    },
    seeMoreText: {
        paddingLeft: 10,
        color: '#21b34f',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 14,
    },
    labelContainer: {
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'space-between',
        width: '100%',
        paddingBottom: 20,
        paddingTop: 20
    },
    body: {
        flexDirection: 'row-reverse',
        flexWrap: 'wrap',
        // width: '100%'
        // marginBottom: 20,
    },
});
