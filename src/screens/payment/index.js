import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, StatusBar, Picker, AsyncStorage, BackHandler, Alert, Linking, Image, TextInput} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import FIcon from 'react-native-vector-icons/dist/Feather';
import EIcon from 'react-native-vector-icons/dist/Entypo';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import {store} from '../../config/store';
import {connect} from 'react-redux';
import Axios from 'axios'
// ;
export const url = 'https://bimehsho.com/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import HomeHeader from "../../components/homeHeader/index";
import SwitchButton from '../../components/switchButton'
import PaymentOptions from '../../components/paymentOptions'
import AlertView from '../../components/modalMassage'
import bime1 from "../../assets/newInsurance/body.png";
import bime2 from "../../assets/newInsurance/third.png";
import bime3 from "../../assets/newInsurance/travel.png";
import bime4 from "../../assets/newInsurance/medical.png";
import bime5 from "../../assets/newInsurance/fire.png";
import bime6 from "../../assets/newInsurance/life.png";
import bime7 from "../../assets/newInsurance/indi.png";
import { CheckBox } from 'react-native-elements'

class Payment extends Component {
    constructor(props){
        super(props);
        this.state = {
            loading: false,
            status: 1,
            activeButton: 1,
            activeVacle: 1,
            showPicker: false,
            selectedStartDate: null,
            switchStatus: false,
            discountCode: '',
            priceData: {},
            correct:false,
            modalVisible: false,
            closeIcon: false,
            checked: false,
            text: '',
            describe: '',
            new_invoice: ''
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        // Actions.pop({refresh: {refresh:Math.random()}});
        Actions.insuranceBuy({openDrawer: this.props.openDrawer});
        return true;
    };
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    paymentWay(type) {
        if(type === 'online') {
            const inv = this.state.new_invoice !== '' ? this.state.new_invoice : this.props.invoice_id;
            Linking.openURL('http://www.banibime.com/pay_invoice?invoice='+inv+'&gateway=1')
            // Linking.openURL('http://www.banibime.com/pay_invoice_bank?invoice='+inv+'&gateway=1')
        }

        // if(type === 'wallet') {
        //     Axios.post('/request/', {}
        //     ).then(response=> {
        //         Alert.alert('', 'پرداخت با موفقیت انجام گرفت')
        //         this.setState({loading: false});
        //
        //         // if(response.data.message === 'ok'){
        //         //     this.setState({loading: false});
        //         //     console.log('edited item', response.data);
        //         // }
        //     })
        //     .catch((error) => {
        //         console.log('edited item', error);
        //         Alert.alert('','خطایی رخ داده لطفا دوباره سعی کنید');
        //         this.setState({loading: false});
        //         console.log(error)
        //     });
        // }
    }
    useOff() {
        console.log('this.state.discountCode.length', this.state.discountCode.length)
        if(this.state.discountCode.length === 0) {
            this.setState({correct: true, modalVisible: true, loading: false});
        }
        else {
            console.log('this.props.invoice_id', this.props.invoice_id)
            this.setState({loading: true});
            Axios.post('/request/financial/request/get_discount_order', {
                    discountCode: this.state.discountCode,
                    invoice_id: this.props.invoice_id
                }
            ).then(response=> {
                console.log('price data', response.data)
                console.log('pric', response.data.length !== 0)
                if(response.data.data !== '') {
                    this.setState({loading: false, priceData: response.data, closeIcon: true, new_invoice: response.data.invoice.id});
                }
                else {
                    // Alert.alert('','لطفا کد صحیح را وارد کنید');
                    // this.setState({loading: false});
                    this.setState({correct: true, modalVisible: true, loading: false});
                }
            })
                .catch((response) => {
                    if(response.data.message === 'not_code') {
                        // Alert.alert('','لطفا کد صحیح را وارد کنید');
                        // this.setState({loading: false});
                        this.setState({correct: true, modalVisible: true, loading: false});
                    }
                    else {
                        // Alert.alert('','خطایی رخ داده لطفا دوباره سعی کنید');
                        // this.setState({loading: false});
                        this.setState({modalVisible: true, loading: false});
                    }
                });
        }
    }
    closeModal() {
        this.setState({modalVisible: false});
    }
    switchCheck(status) {
        this.setState({switchStatus: status})
    }
    render() {
        let overalPrice = null, offedValue = null , paymentValue = null;
        if(this.props.invoice && this.props.invoice.off_amount !== null){
            overalPrice = Math.floor(this.props.factor.dataSelect.fianl_price).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") ;
            offedValue = Math.floor(this.props.invoice.off_amount).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") ;
            paymentValue = Math.floor(this.props.invoice.amount).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
        else {
            overalPrice =  Object.keys(this.state.priceData).length !== 0 ? Math.floor(JSON.parse(this.state.priceData.invoice.data).amount).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : Math.floor(this.props.factor.dataSelect.fianl_price).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") ;
            offedValue =  Object.keys(this.state.priceData).length !== 0 ? Math.floor(this.state.priceData.invoice.off_amount).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : 0;
            paymentValue = Object.keys(this.state.priceData).length !== 0 ? Math.floor(this.state.priceData.invoice.amount).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : Math.floor(this.props.factor.dataSelect.fianl_price).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

        }
        if(this.state.loading){
            return (<Loader />)
        }
        else return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <HomeHeader active={7} openDrawer={this.props.openDrawer} pageTitle={this.props.pageTitle} />
                </View>
                <ScrollView style={styles.scroll}>
                    <View style={styles.bodyContainer}>
                        <View style={styles.reductionContainer}>
                            <View style={styles.reduction}>
                                <SwitchButton check={(status) => this.switchCheck(status)} switchStatus={this.state.switchStatus} />
                                <Text style={styles.redlabel}>کد تخفیف دارید؟</Text>
                            </View>
                            {
                                this.state.switchStatus ?
                                    <View style={{width: '100%'}}>
                                        <TextInput
                                            maxLength={10}
                                            placeholder="کد تخفیف را وارد کنید ..."
                                            placeholderTextColor={'gray'}
                                            underlineColorAndroid='transparent'
                                            value={this.state.discountCode}
                                            style={{
                                                height: 45,
                                                paddingRight: 15,
                                                width: '100%',
                                                borderRadius: 10,
                                                borderWidth: 1,
                                                borderColor: 'lightgray',
                                                direction: 'rtl',
                                                fontFamily: 'IRANSansMobile(FaNum)'
                                            }}
                                            onChangeText={(discountCode) => this.setState({discountCode})} />
                                        <TouchableOpacity onPress={() => this.useOff()} style={styles.redButton}>
                                            <View>
                                                <Text style={styles.buttonText}>تایید کد تخفیف</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                    : null
                            }
                        </View>
                        <View style={styles.body}>
                            <View style={[styles.left, {alignItems: 'flex-start', justifyContent: 'flex-start', paddingLeft: 20}]}>
                                {/*<View style={styles.bodyLeft}>*/}
                                {/*<Text style={styles.bodyLabel}>تومان</Text>*/}
                                {/*<Text style={styles.bodyValue}> 77655259 </Text>*/}
                                {/*</View>*/}
                                {/*<View style={styles.bodyLeft}>*/}
                                {/*<Text style={styles.bodyLabel}>تومان</Text>*/}
                                {/*<Text style={styles.bodyValue}> 77655259 </Text>*/}
                                {/*</View>*/}
                                <View style={styles.bodyLeft}>
                                    <Text style={styles.bodyLabel}>تومان</Text>
                                    <Text style={styles.bodyValue}> {overalPrice} </Text>
                                </View>
                                {/*<View style={styles.bodyLeft}>*/}
                                {/*<Text style={styles.bodyLabel}>تومان</Text>*/}
                                {/*<Text style={styles.bodyValue}> 77655259 </Text>*/}
                                {/*</View>*/}
                                <View style={styles.bodyLeft}>
                                    <Text style={styles.bodyLabel}>تومان</Text>
                                    <Text style={[styles.bodyValue, {color: 'red', paddingLeft: 5}]}>{offedValue}</Text>
                                    {
                                        this.state.closeIcon ?
                                            <TouchableOpacity onPress={() => this.setState({priceData: {}, closeIcon: false})}>
                                                <Icon name="close" size={18} color="red" style={{paddingLeft: 15}} />
                                            </TouchableOpacity> : null
                                    }

                                </View>
                                <View style={styles.bodyLeft}>
                                    <Text style={styles.bodyLabel}>تومان</Text>
                                    <Text style={[styles.bodyLabel, {color: '#21b34f'}]}> {paymentValue} </Text>
                                </View>
                            </View>
                            <View style={styles.right}>
                                {/*<Text style={styles.bodyLabel}>حق بیمه خالص</Text>*/}
                                {/*<Text style={styles.bodyLabel}>مالیات و عوارض</Text>*/}
                                <Text style={styles.bodyLabel}>جمع کل حق بیمه</Text>
                                {/*<Text style={styles.bodyLabel}>هزینه ارسال</Text>*/}
                                <Text style={styles.bodyLabel}>تخفیف</Text>
                                <Text style={styles.bodyLabel}>مبلغ قابل پرداخت</Text>
                            </View>
                        </View>
                        <View style={[styles.roulsContainer, {flexDirection: 'column', alignItems: 'center', justifyContent: 'center', marginTop: 15, paddingBottom: 30}]}>
                            <Text style={[styles.roulsText, {paddingBottom: 15}]}>
                                کد بازاریاب
                            </Text>
                            <TextInput
                                keyboardType="numeric"
                                placeholder="کد بازاریاب"
                                placeholderTextColor={'gray'}
                                underlineColorAndroid='transparent'
                                value={this.state.text}
                                maxLength={20}
                                style={{
                                    height: 36,
                                    paddingRight: 15,
                                    width: '95%',
                                    textAlign: 'center',
                                    backgroundColor: 'rgb(252, 252, 252)',
                                    borderWidth: 1,
                                    borderRadius: 5,
                                    borderColor: 'gray',
                                    // marginBottom: 10,
                                    // marginTop: 15,
                                    fontFamily: 'IRANSansMobile(FaNum)',
                                    fontSize: 12,
                                    color: 'gray',
                                }}
                                onChangeText={(text) => this.setState({text})}
                            />
                        </View>
                        <View style={[styles.roulsContainer, {flexDirection: 'column', alignItems: 'center', justifyContent: 'center', paddingBottom: 30}]}>
                            <Text style={[styles.roulsText, {paddingBottom: 8, color: 'black', fontSize: 13}]}>
                                توضیحات لازم
                            </Text>
                            <Text style={[styles.roulsText, {paddingBottom: 15}]}>
                                اگر در مورد نحوه صدور بیمه نامه و یا سایر موارد توضیحاتی دارید لطفا در این قسمت وارد کنید.
                            </Text>
                            <TextInput
                                multiline = {true}
                                keyboardType="numeric"
                                numberOfLines={20}
                                placeholder="توضیحات"
                                placeholderTextColor={'gray'}
                                underlineColorAndroid='transparent'
                                maxLength={150}
                                value={this.state.describe}
                                style={{
                                    height: 70 ,
                                    paddingRight: 15,
                                    width: '95%',
                                    textAlign: 'right',
                                    backgroundColor: 'rgb(252, 252, 252)',
                                    borderWidth: 1,
                                    borderColor: 'gray',
                                    // marginBottom: 10,
                                    // marginTop: 15,
                                    borderRadius: 5,
                                    fontFamily: 'IRANSansMobile(FaNum)',
                                    fontSize: 12,
                                    color: 'gray',
                                }}
                                onChangeText={(text) => this.setState({describe: text})}
                            />
                        </View>
                        <View style={styles.offeredContainer}>
                            <Text style={[styles.roulsText, {color: 'black', fontSize: 13, paddingBottom: 15}]}>
                                بیمه های پیشنهادی
                            </Text>
                            {
                                this.props.insurType === 'Third' ?
                                    <View style={styles.insListContainer}>
                                        <View style={styles.item}>
                                            <TouchableOpacity onPress={() => Actions.otherVacleInfo({
                                                openDrawer: this.props.openDrawer,
                                                fire: true,
                                                pageTitle: 'بیمه آتش سوزی'
                                            })} style={styles.imageContainer}>
                                                <Image source={bime5} style={styles.bodyImage}/>
                                            </TouchableOpacity>
                                            <Text style={styles.label}>بیمه آتش سوزی</Text>
                                        </View>
                                        <View style={styles.item}>
                                            <TouchableOpacity style={styles.imageContainer}
                                                              onPress={() => Actions.vacleInfo({
                                                                  openDrawer: this.props.openDrawer,
                                                                  bodyBime: true,
                                                                  pageTitle: 'بیمه بدنه خودرو'
                                                              })}>
                                                <Image source={bime1} style={styles.bodyImage}/>
                                            </TouchableOpacity>
                                            <Text style={styles.label}>بیمه بدنه خودرو</Text>
                                        </View>
                                        <View style={styles.item}>
                                            <TouchableOpacity onPress={() => Actions.otherVacleInfo({
                                                openDrawer: this.props.openDrawer,
                                                travel: true,
                                                pageTitle: 'بیمه مسافرتی'
                                            })} style={styles.imageContainer}>
                                                <Image source={bime3} style={styles.bodyImage}/>
                                            </TouchableOpacity>
                                            <Text style={styles.label}>بیمه مسافرتی</Text>
                                        </View>
                                        {/*<View style={styles.item}>*/}
                                        {/*<TouchableOpacity onPress={() => Actions.otherVacleInfo({*/}
                                        {/*openDrawer: this.props.openDrawer,*/}
                                        {/*individual: true,*/}
                                        {/*pageTitle: 'بیمه درمان انفرادی'*/}
                                        {/*})} style={styles.imageContainer}>*/}
                                        {/*<Image source={bime7} style={styles.bodyImage}/>*/}
                                        {/*</TouchableOpacity>*/}
                                        {/*<Text style={styles.label}>بیمه درمان انفرادی</Text>*/}
                                        {/*</View>*/}

                                    </View>
                                    :
                                    this.props.insurType === 'body' ?
                                        <View style={styles.insListContainer}>
                                            <View style={styles.item}>
                                                <TouchableOpacity onPress={() => Actions.otherVacleInfo({
                                                    openDrawer: this.props.openDrawer,
                                                    fire: true,
                                                    pageTitle: 'بیمه آتش سوزی'
                                                })} style={styles.imageContainer}>
                                                    <Image source={bime5} style={styles.bodyImage}/>
                                                </TouchableOpacity>
                                                <Text style={styles.label}>بیمه آتش سوزی</Text>
                                            </View>
                                            <View style={styles.item}>
                                                <TouchableOpacity style={styles.imageContainer}
                                                                  onPress={() => Actions.vacleInfo({
                                                                      openDrawer: this.props.openDrawer,
                                                                      thirdBime: true,
                                                                      motor: false,
                                                                      pageTitle: 'بیمه شخص ثالث'
                                                                  })}>
                                                    <Image source={bime2} style={styles.bodyImage}/>
                                                </TouchableOpacity>
                                                <Text style={styles.label}>بیمه شخص ثالث</Text>
                                            </View>
                                            {/*<View style={styles.item}>*/}
                                            {/*<TouchableOpacity onPress={() => Actions.otherVacleInfo({*/}
                                            {/*openDrawer: this.props.openDrawer,*/}
                                            {/*travel: true,*/}
                                            {/*pageTitle: 'بیمه مسافرتی'*/}
                                            {/*})} style={styles.imageContainer}>*/}
                                            {/*<Image source={bime3} style={styles.bodyImage}/>*/}
                                            {/*</TouchableOpacity>*/}
                                            {/*<Text style={styles.label}>بیمه مسافرتی</Text>*/}
                                            {/*</View>*/}
                                            <View style={styles.item}>
                                                <TouchableOpacity onPress={() => Actions.otherVacleInfo({
                                                    openDrawer: this.props.openDrawer,
                                                    individual: true,
                                                    pageTitle: 'بیمه درمان انفرادی'
                                                })} style={styles.imageContainer}>
                                                    <Image source={bime7} style={styles.bodyImage}/>
                                                </TouchableOpacity>
                                                <Text style={styles.label}>بیمه درمان انفرادی</Text>
                                            </View>
                                        </View>
                                        :
                                        this.props.insurType === 'fire' || this.props.insurType === 'earth'?
                                            <View style={styles.insListContainer}>
                                                <View style={styles.item}>
                                                    <TouchableOpacity style={styles.imageContainer}
                                                                      onPress={() => Actions.vacleInfo({
                                                                          openDrawer: this.props.openDrawer,
                                                                          thirdBime: true,
                                                                          motor: false,
                                                                          pageTitle: 'بیمه شخص ثالث'
                                                                      })}>
                                                        <Image source={bime2} style={styles.bodyImage}/>
                                                    </TouchableOpacity>
                                                    <Text style={styles.label}>بیمه شخص ثالث</Text>
                                                </View>
                                                <View style={styles.item}>
                                                    <TouchableOpacity style={styles.imageContainer}
                                                                      onPress={() => Actions.vacleInfo({
                                                                          openDrawer: this.props.openDrawer,
                                                                          bodyBime: true,
                                                                          pageTitle: 'بیمه بدنه خودرو'
                                                                      })}>
                                                        <Image source={bime1} style={styles.bodyImage}/>
                                                    </TouchableOpacity>
                                                    <Text style={styles.label}>بیمه بدنه خودرو</Text>
                                                </View>
                                                <View style={styles.item}>
                                                    <TouchableOpacity onPress={() => Actions.otherVacleInfo({
                                                        openDrawer: this.props.openDrawer,
                                                        travel: true,
                                                        pageTitle: 'بیمه مسافرتی'
                                                    })} style={styles.imageContainer}>
                                                        <Image source={bime3} style={styles.bodyImage}/>
                                                    </TouchableOpacity>
                                                    <Text style={styles.label}>بیمه مسافرتی</Text>
                                                </View>
                                                <View style={styles.item}>
                                                    <TouchableOpacity onPress={() => Actions.otherVacleInfo({
                                                        openDrawer: this.props.openDrawer,
                                                        individual: true,
                                                        pageTitle: 'بیمه درمان انفرادی'
                                                    })} style={styles.imageContainer}>
                                                        <Image source={bime7} style={styles.bodyImage}/>
                                                    </TouchableOpacity>
                                                    <Text style={styles.label}>بیمه درمان انفرادی</Text>
                                                </View>
                                                <View style={styles.item}>
                                                    <TouchableOpacity onPress={() => Actions.otherVacleInfo({
                                                        openDrawer: this.props.openDrawer,
                                                        age: true,
                                                        pageTitle: 'بیمه عمر'
                                                    })} style={styles.imageContainer}>
                                                        <Image source={bime6} style={styles.bodyImage}/>
                                                    </TouchableOpacity>
                                                    <Text style={styles.label}>بیمه عمر</Text>
                                                </View>
                                            </View>
                                            :
                                            this.props.insurType === 'travel' ?
                                                <View style={styles.insListContainer}>
                                                    <View style={styles.item}>
                                                        <TouchableOpacity onPress={() => Actions.otherVacleInfo({
                                                            openDrawer: this.props.openDrawer,
                                                            fire: true,
                                                            pageTitle: 'بیمه آتش سوزی'
                                                        })} style={styles.imageContainer}>
                                                            <Image source={bime5} style={styles.bodyImage}/>
                                                        </TouchableOpacity>
                                                        <Text style={styles.label}>بیمه آتش سوزی</Text>
                                                    </View>

                                                    {/*<View style={styles.item}>*/}
                                                    {/*<TouchableOpacity onPress={() => Actions.otherVacleInfo({*/}
                                                    {/*openDrawer: this.props.openDrawer,*/}
                                                    {/*age: true,*/}
                                                    {/*pageTitle: 'بیمه عمر'*/}
                                                    {/*})} style={styles.imageContainer}>*/}
                                                    {/*<Image source={bime6} style={styles.bodyImage}/>*/}
                                                    {/*</TouchableOpacity>*/}
                                                    {/*<Text style={styles.label}>بیمه عمر</Text>*/}
                                                    {/*</View>*/}
                                                    <View style={styles.item}>
                                                        <TouchableOpacity onPress={() => Actions.otherVacleInfo({
                                                            openDrawer: this.props.openDrawer,
                                                            medical: true,
                                                            pageTitle: 'بیمه مسئولیت پزشکی'
                                                        })} style={styles.imageContainer}>
                                                            <Image source={bime4} style={styles.bodyImage}/>
                                                        </TouchableOpacity>
                                                        <Text style={styles.label}>بیمه مسئولیت پزشکان و پیرا
                                                            پزشکان</Text>
                                                    </View>
                                                    <View style={styles.item}>
                                                        <TouchableOpacity onPress={() => Actions.otherVacleInfo({
                                                            openDrawer: this.props.openDrawer,
                                                            individual: true,
                                                            pageTitle: 'بیمه درمان انفرادی'
                                                        })} style={styles.imageContainer}>
                                                            <Image source={bime7} style={styles.bodyImage}/>
                                                        </TouchableOpacity>
                                                        <Text style={styles.label}>بیمه درمان انفرادی</Text>
                                                    </View>
                                                </View>
                                                :
                                                this.props.insurType === 'responsible' ?
                                                    <View style={styles.insListContainer}>
                                                        <View style={styles.item}>
                                                            <TouchableOpacity style={styles.imageContainer}
                                                                              onPress={() => Actions.vacleInfo({
                                                                                  openDrawer: this.props.openDrawer,
                                                                                  thirdBime: true,
                                                                                  motor: false,
                                                                                  pageTitle: 'بیمه شخص ثالث'
                                                                              })}>
                                                                <Image source={bime2} style={styles.bodyImage}/>
                                                            </TouchableOpacity>
                                                            <Text style={styles.label}>بیمه شخص ثالث</Text>
                                                        </View>
                                                        {/*<View style={styles.item}>*/}
                                                        {/*<TouchableOpacity style={styles.imageContainer}*/}
                                                        {/*onPress={() => Actions.vacleInfo({*/}
                                                        {/*openDrawer: this.props.openDrawer,*/}
                                                        {/*bodyBime: true,*/}
                                                        {/*pageTitle: 'بیمه بدنه خودرو'*/}
                                                        {/*})}>*/}
                                                        {/*<Image source={bime1} style={styles.bodyImage}/>*/}
                                                        {/*</TouchableOpacity>*/}
                                                        {/*<Text style={styles.label}>بیمه بدنه خودرو</Text>*/}
                                                        {/*</View>*/}
                                                        <View style={styles.item}>
                                                            <TouchableOpacity onPress={() => Actions.otherVacleInfo({
                                                                openDrawer: this.props.openDrawer,
                                                                travel: true,
                                                                pageTitle: 'بیمه مسافرتی'
                                                            })} style={styles.imageContainer}>
                                                                <Image source={bime3} style={styles.bodyImage}/>
                                                            </TouchableOpacity>
                                                            <Text style={styles.label}>بیمه مسافرتی</Text>
                                                        </View>
                                                        <View style={styles.item}>
                                                            <TouchableOpacity onPress={() => Actions.otherVacleInfo({
                                                                openDrawer: this.props.openDrawer,
                                                                fire: true,
                                                                pageTitle: 'بیمه آتش سوزی'
                                                            })} style={styles.imageContainer}>
                                                                <Image source={bime5} style={styles.bodyImage}/>
                                                            </TouchableOpacity>
                                                            <Text style={styles.label}>بیمه آتش سوزی</Text>
                                                        </View>
                                                    </View>
                                                    :
                                                    this.props.insurType === 'complete' ?
                                                        <View style={styles.insListContainer}>
                                                            <View style={styles.item}>
                                                                <TouchableOpacity
                                                                    onPress={() => Actions.otherVacleInfo({
                                                                        openDrawer: this.props.openDrawer,
                                                                        fire: true,
                                                                        pageTitle: 'بیمه آتش سوزی'
                                                                    })} style={styles.imageContainer}>
                                                                    <Image source={bime5} style={styles.bodyImage}/>
                                                                </TouchableOpacity>
                                                                <Text style={styles.label}>بیمه آتش سوزی</Text>
                                                            </View>
                                                            <View style={styles.item}>
                                                                <TouchableOpacity
                                                                    onPress={() => Actions.otherVacleInfo({
                                                                        openDrawer: this.props.openDrawer,
                                                                        travel: true,
                                                                        pageTitle: 'بیمه مسافرتی'
                                                                    })} style={styles.imageContainer}>
                                                                    <Image source={bime3} style={styles.bodyImage}/>
                                                                </TouchableOpacity>
                                                                <Text style={styles.label}>بیمه مسافرتی</Text>
                                                            </View>
                                                            <View style={styles.item}>
                                                                <TouchableOpacity
                                                                    onPress={() => Actions.otherVacleInfo({
                                                                        openDrawer: this.props.openDrawer,
                                                                        individual: true,
                                                                        pageTitle: 'بیمه درمان انفرادی'
                                                                    })} style={styles.imageContainer}>
                                                                    <Image source={bime7} style={styles.bodyImage}/>
                                                                </TouchableOpacity>
                                                                <Text style={styles.label}>بیمه درمان انفرادی</Text>
                                                            </View>
                                                            <View style={styles.item}>
                                                                <TouchableOpacity
                                                                    onPress={() => Actions.otherVacleInfo({
                                                                        openDrawer: this.props.openDrawer,
                                                                        medical: true,
                                                                        pageTitle: 'بیمه مسئولیت پزشکی'
                                                                    })} style={styles.imageContainer}>
                                                                    <Image source={bime4} style={styles.bodyImage}/>
                                                                </TouchableOpacity>
                                                                <Text style={styles.label}>بیمه مسئولیت پزشکان و پیرا
                                                                    پزشکان</Text>
                                                            </View>
                                                            <View style={styles.item}>
                                                                <TouchableOpacity
                                                                    onPress={() => Actions.otherVacleInfo({
                                                                        openDrawer: this.props.openDrawer,
                                                                        age: true,
                                                                        pageTitle: 'بیمه عمر'
                                                                    })} style={styles.imageContainer}>
                                                                    <Image source={bime6} style={styles.bodyImage}/>
                                                                </TouchableOpacity>
                                                                <Text style={styles.label}>بیمه عمر</Text>
                                                            </View>
                                                        </View>
                                                        :
                                                        this.props.insurType === 'life' ?
                                                            <View style={styles.insListContainer}>
                                                                <View style={styles.item}>
                                                                    <TouchableOpacity
                                                                        onPress={() => Actions.otherVacleInfo({
                                                                            openDrawer: this.props.openDrawer,
                                                                            fire: true,
                                                                            pageTitle: 'بیمه آتش سوزی'
                                                                        })} style={styles.imageContainer}>
                                                                        <Image source={bime5} style={styles.bodyImage}/>
                                                                    </TouchableOpacity>
                                                                    <Text style={styles.label}>بیمه آتش سوزی</Text>
                                                                </View>
                                                                <View style={styles.item}>
                                                                    <TouchableOpacity
                                                                        onPress={() => Actions.otherVacleInfo({
                                                                            openDrawer: this.props.openDrawer,
                                                                            travel: true,
                                                                            pageTitle: 'بیمه مسافرتی'
                                                                        })} style={styles.imageContainer}>
                                                                        <Image source={bime3} style={styles.bodyImage}/>
                                                                    </TouchableOpacity>
                                                                    <Text style={styles.label}>بیمه مسافرتی</Text>
                                                                </View>
                                                                <View style={styles.item}>
                                                                    <TouchableOpacity
                                                                        onPress={() => Actions.otherVacleInfo({
                                                                            openDrawer: this.props.openDrawer,
                                                                            individual: true,
                                                                            pageTitle: 'بیمه درمان انفرادی'
                                                                        })} style={styles.imageContainer}>
                                                                        <Image source={bime7} style={styles.bodyImage}/>
                                                                    </TouchableOpacity>
                                                                    <Text style={styles.label}>بیمه درمان انفرادی</Text>
                                                                </View>
                                                                <View style={styles.item}>
                                                                    <TouchableOpacity style={styles.imageContainer}
                                                                                      onPress={() => Actions.vacleInfo({
                                                                                          openDrawer: this.props.openDrawer,
                                                                                          bodyBime: true,
                                                                                          pageTitle: 'بیمه بدنه خودرو'
                                                                                      })}>
                                                                        <Image source={bime1} style={styles.bodyImage}/>
                                                                    </TouchableOpacity>
                                                                    <Text style={styles.label}>بیمه بدنه خودرو</Text>
                                                                </View>
                                                                <View style={styles.item}>
                                                                    <TouchableOpacity style={styles.imageContainer}
                                                                                      onPress={() => Actions.vacleInfo({
                                                                                          openDrawer: this.props.openDrawer,
                                                                                          thirdBime: true,
                                                                                          motor: false,
                                                                                          pageTitle: 'بیمه شخص ثالث'
                                                                                      })}>
                                                                        <Image source={bime2} style={styles.bodyImage}/>
                                                                    </TouchableOpacity>
                                                                    <Text style={styles.label}>بیمه شخص ثالث</Text>
                                                                </View>
                                                            </View>
                                                            :
                                                            null
                            }
                        </View>
                        <View style={styles.labelContainer}>
                            <Text style={styles.topLabel}>نحوه پرداخت</Text>
                            <EIcon name="browser" size={20} color="gray" />
                        </View>
                        <PaymentOptions paymentWay={(type)=> null} paymentValue={paymentValue} />
                        <View style={styles.roulsContainer}>
                            <Text style={styles.roulsText}>
                                بدینوسیله اطلاعات وارد شده در بالا را تایید میکنم و صحت اطلاعات فوق تماما بر عهده اینجانب  است و سایت بیمه شو در برابر صحیح نبودن اطلاعات فوق هیچ مسئولیتی ندارد.
                            </Text>
                            <CheckBox
                                containerStyle={{backgroundColor: 'transparent', borderWidth: 0, padding: 0}}
                                // center
                                // title='Click Here'
                                checkedIcon='dot-circle-o'
                                uncheckedIcon='circle-o'
                                checked={  this.state.checked}
                                onPress={() =>  {this.setState({ checked: !this.state.checked})}} />
                        </View>
                        <AlertView
                            closeModal={(title) => this.closeModal(title)}
                            modalVisible={this.state.modalVisible}
                            onChange={(visible) => this.setState({modalVisible: false})}

                            title={this.state.correct ? 'لطفا کد صحیح را وارد کنید': 'مشکلی در برقراری ارتباط با سرور به وجود آمده لطفا دوباره سعی کنید'}
                        />
                    </View>
                </ScrollView>
                {
                    this.state.checked ?
                        <View style={styles.footer}>
                            <TouchableOpacity onPress={() => this.paymentWay('online')} style={styles.iconLeftContainer}>
                                <FIcon name="arrow-left" size={18} color="white" style={{borderColor: 'white', borderWidth: 1, borderRadius: 20, padding: 3, marginRight: 5}} />
                                <Text style={styles.label2}>تایید و پرداخت</Text>
                            </TouchableOpacity>
                            {/*<TouchableOpacity style={styles.iconRightContainer} onPress={() => this.onBackPress()}>*/}
                            {/*<FIcon name="arrow-right" size={18} color="#21b34f" />*/}
                            {/*</TouchableOpacity>*/}
                        </View>
                        : null
                }
            </View>
        );
    }
}
export default Payment;

